#pragma once


typedef struct _COMPORT_INFO
{
	int num;
	CString name;
	CString desc;
} COMPORT_INFO;

class CComPortEnum
{
public:
	CComPortEnum(void);
	virtual ~CComPortEnum(void);

private:
	int m_nComPortCount;
	CList<COMPORT_INFO, COMPORT_INFO> m_lstComPort;

public:
	int GetComPortNumber(void) { return m_nComPortCount; }
	int EnumComPort(void);                // 枚举所有的串口，返回总串口数
	BOOL EnumComPort2(void);
	CString GetComPortDesc(int listnum);  // 从列表索引号得到串口描述
	CString GetComPortName(int listnum);  // 从列表索引号得到串口名
	int GetPortNum(int listnum);          // 从列表索引号得到串口设备号

private:
	CString GetName(CString csstr);
	int GetNumber(CString csstr);
};
