#include "StdAfx.h"
#include "LedMode.h"
#include "LedModeMgr.h"

CLedModeMgr::CLedModeMgr(void)
{
}

CLedModeMgr::~CLedModeMgr(void)
{
	RemoveAll();
}
void CLedModeMgr::RemoveAll()
{
	int i;
	int nSize = (int)m_aryLedModes.GetSize();
	if (nSize > 0) {
		for (i=0; i<nSize; i++) {
			CLedMode* p = m_aryLedModes.GetAt(i);
			delete p;
		}
		m_aryLedModes.RemoveAll();
	}
}
CLedModeMgr* CLedModeMgr::Copy(CLedModeMgr* pOrig)
{
	CLedModeMgr* pNewMgr = new CLedModeMgr();
	int i;
	int nSize = (int)pOrig->m_aryLedModes.GetSize();
	for (i=0; i<nSize; i++) {
		CLedMode* p = pOrig->m_aryLedModes.GetAt(i);
		CLedMode* pNewMode = CLedMode::Copy(p);
		pNewMgr->Add(pNewMode);
	}
	return pNewMgr;
}
void CLedModeMgr::Add(CLedMode* pMode)
{
	m_aryLedModes.Add(pMode);
}
void CLedModeMgr::Del(int index)
{
	CLedMode* p = m_aryLedModes.GetAt(index);
	m_aryLedModes.RemoveAt(index);
	delete p;
}
CLedMode* CLedModeMgr::Get(int index)
{
	return m_aryLedModes.GetAt(index);
}
void CLedModeMgr::Insert(int index, CLedMode* pMode)
{
}
int CLedModeMgr::GetCount()
{
	return (int)m_aryLedModes.GetSize();
}
void CLedModeMgr::NewData()
{
	RemoveAll();
}
BOOL CLedModeMgr::Save(CString& strOutput)
{
	int i;
	int nSize = (int)m_aryLedModes.GetSize();
	if (nSize == 0) {
		strOutput += _T("-");
		return TRUE;
	}
	for (i=0; i<nSize; i++) {
		CLedMode* p = m_aryLedModes.GetAt(i);
		CString strMode;
		p->Save(strMode);
		if (i != (nSize-1)) {
			strMode += _T(";");
		}
		strOutput += strMode;
	}
	return TRUE;
}
BOOL CLedModeMgr::Load(CString strInput)
{
	NewData();
	if (strInput.Left(1) == _T("-")) {
		return TRUE;
	}
	CStringArray sar;
	int count = Split(sar, strInput, _T(";"));
	int i;
	for (i=0; i<count; i++) {
		CLedMode* pNewMode = new CLedMode(0);
		if (pNewMode->Load(sar[i])) {
			m_aryLedModes.Add(pNewMode);
		} else {
			delete pNewMode;
			return FALSE;
		}
	}
	return TRUE;
}
BOOL CLedModeMgr::MoveUp(int index)
{
	int nCount = (int)m_aryLedModes.GetSize();
	if (index <= 0 || index >= nCount) {
		return FALSE;
	}
	CLedMode* pMode = m_aryLedModes.GetAt(index);
	m_aryLedModes.RemoveAt(index);
	m_aryLedModes.InsertAt(index - 1, pMode);
	return TRUE;
}
BOOL CLedModeMgr::MoveDown(int index)
{
	int nCount = (int)m_aryLedModes.GetSize();
	if (index < 0 || index >= (nCount-1)) {
		return FALSE;
	}
	CLedMode* pMode = m_aryLedModes.GetAt(index);
	m_aryLedModes.RemoveAt(index);
	m_aryLedModes.InsertAt(index + 1, pMode);
	return TRUE;
}
BOOL CLedModeMgr::RemoveAt(int index)
{
	int nCount = (int)m_aryLedModes.GetSize();
	if (index < 0 || index >= nCount) {
		return FALSE;
	}
	CLedMode* pMode = m_aryLedModes.GetAt(index);
	m_aryLedModes.RemoveAt(index);
	delete pMode;
	return TRUE;
}
void CLedModeMgr::SimResetAll(int offset)
{
	int i;
	int nSize = (int)m_aryLedModes.GetSize();
	for (i=0; i<nSize; i++) {
		CLedMode* p = m_aryLedModes.GetAt(i);
		p->SimReset(offset);
	}
}