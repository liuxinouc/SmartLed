#pragma once


// CSetupLedBarDlg 对话框

#define  MSG_MODI_LED_COUNT        (WM_USER+2000)

class CImageButton;
class CTextProgressCtrl;
class CLedEditorDoc;
class CLedEditorView;
class CLedBar;
class CLedModeMgr;
class CSetupLedBarDlg : public CDialog
{
	DECLARE_DYNAMIC(CSetupLedBarDlg)

private:
	CLedEditorDoc* m_pDoc;
	CLedEditorView* m_pView;
	CLedBar* m_pLedBar;
	int m_nOldCount;
	BOOL m_bOldIndependent;
	CLedModeMgr* m_pOldModeMgr;

	CButton* m_pBtnSub10;
	CButton* m_pBtnSub1;
	CButton* m_pBtnAdd10;
	CButton* m_pBtnAdd1;
	CEdit* m_pEdtLedDots;
	CEdit* m_pEdtLedLength;
	CEdit* m_pEdtTotalTime;
	CTextProgressCtrl* m_pPrgMemory;
	CButton* m_pChkIndependent;
	CListCtrl* m_pLstModes;
	CButton* m_pBtnAddMode;
	CButton* m_pBtnEditMode;
	CButton* m_pBtnMoveUp;
	CButton* m_pBtnMoveDown;
	CButton* m_pBtnDeleteMode;
	CImageButton* m_pBtnStart;
	CImageButton* m_pBtnStop;

public:
	CSetupLedBarDlg(CWnd* pParent, CLedBar* pBar);
	virtual ~CSetupLedBarDlg();

// 对话框数据
	enum { IDD = IDD_SETUP_LEDBAR };

	int GetRemainedCodeSize();

private:
	void RenewDotsAndLength();
	void InitSetupOptions();
	void SetIndependent(BOOL bIndependent);
	void RenewModeList();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedSub10();
	afx_msg void OnBnClickedSub1();
	afx_msg void OnBnClickedAdd1();
	afx_msg void OnBnClickedAdd10();
	afx_msg void OnEnChangeLedDots();
	afx_msg LRESULT OnMsgModiLedCount(WPARAM wp, LPARAM lp);
	afx_msg void OnBnClickedIndependent();
	afx_msg void OnBnClickedAddMode();
	afx_msg void OnBnClickedEditMode();
	afx_msg void OnBnClickedMoveUp();
	afx_msg void OnBnClickedMoveDown();
	afx_msg void OnBnClickedDeleteMode();
	afx_msg void OnBnClickedBtnStart();
	afx_msg void OnBnClickedBtnStop();
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
public:
	afx_msg void OnHdnDblclickParamList(NMHDR *pNMHDR, LRESULT *pResult);
};
