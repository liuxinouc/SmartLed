#pragma once

class CLed
{
public:
	CLed(void);
	virtual ~CLed(void);

public:
	// 颜色
	DWORD m_dwColor;

public:
	static CLed* Copy(CLed* pOrig);

	// 根据颜色推算出消耗电流，单位安培
	float GetAmpere();
};
