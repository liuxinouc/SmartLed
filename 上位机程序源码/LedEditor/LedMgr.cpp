#include "StdAfx.h"
#include "resource.h"
#include "LedMgr.h"
#include "LedEditorDoc.h"
#include "LedEditorView.h"

CLedMgr::CLedMgr(CLedEditorDoc* pDoc)
{
	m_pDoc = pDoc;
	m_ptDrawPos.SetPoint(HUB_DRAW_X, HUB_DRAW_Y);
	m_nCurIndex = 0;
}

CLedMgr::~CLedMgr(void)
{
	RemoveAll();
}

void CLedMgr::RemoveAll(void)
{
	int i;
	int nSize = (int)m_aryLedBars.GetSize();
	if (nSize > 0) {
		for (i=0; i<nSize; i++) {
			CLedBar* p = m_aryLedBars.GetAt(i);
			delete p;
		}
		m_aryLedBars.RemoveAll();
	}
}

// 初始化
// nLedCount是支持的最大灯带数量
void CLedMgr::Init(int nLedCount)
{
	int i;
	for (i=0; i<nLedCount; i++) {
		CLedBar* p = new CLedBar();
		p->SetLedCount(LED_BAR_SIZE_DEFAULT);
		p->Enable(FALSE);
		CPoint pt = GetPortDrawPos(i);
		p->SetDrawPos(pt.x, pt.y);
		m_aryLedBars.Add(p);
	}
}

// 使能/禁止灯带
// nIndex：第几条灯带，0-based
// bEnable：是否使能
void CLedMgr::EnableBar(int nIndex, BOOL bEnable)
{
	GetLedBar(nIndex)->Enable(bEnable);
	// 如果关闭一条灯带，那么紧接其后的非独立灯带将变成独立灯带
	if (!bEnable) {
		CLedBar* p = GetValidNextBar(nIndex);
		if (p != NULL) {
			if (!p->IsIndependent()) {
				p->Independent(TRUE);
			}
		}
	}
}
void CLedMgr::EnableCurBar(BOOL bEnable)
{
	EnableBar(m_nCurIndex, bEnable);
}

// 设置编辑按钮的状态和位置
void CLedMgr::SetCurBarBtn(CLedEditorView* pView)
{
	BOOL bEnable = GetLedBar(m_nCurIndex)->IsEnable();
	int l = m_ptDrawPos.x + HUB_INFO_W;
	int t = m_ptDrawPos.y + m_nCurIndex * HUB_UNIT_H;
	int r = l + HUB_UNIT_W;
	int b = t + HUB_UNIT_H;
	CRect rc(l, t, r, b);
	pView->SetBtnPos(rc, bEnable);
}
void CLedMgr::ResetBtns(CLedEditorView* pView)
{
	BOOL bEnable = GetLedBar(0)->IsEnable();
	int l = m_ptDrawPos.x + HUB_INFO_W;
	int t = m_ptDrawPos.y;
	int r = l + HUB_UNIT_W;
	int b = t + HUB_UNIT_H;
	CRect rc(l, t, r, b);
	pView->SetBtnPos(rc, bEnable);
}

// 获得灯带
// nIndex：灯带索引
CLedBar* CLedMgr::GetLedBar(int nIndex)
{
	return m_aryLedBars.GetAt(nIndex);
}
CLedBar* CLedMgr::GetCurLedBar()
{
	return m_aryLedBars.GetAt(m_nCurIndex);
}
int CLedMgr::GetCurIndex()
{
	return m_nCurIndex;
}
// 获得前一条使能的工作灯带
CLedBar* CLedMgr::GetValidPreBar(int index)
{
	if (index > 0) {
		CLedBar* p = m_aryLedBars.GetAt(index - 1);
		if (p->IsEnable()) {
			return p;
		}
	}
	return NULL;
}
// 获得后一条使能的工作灯带
CLedBar* CLedMgr::GetValidNextBar(int index)
{
	int nSize = (int)m_aryLedBars.GetSize();
	if (index < (nSize - 1)) {
		CLedBar* p = m_aryLedBars.GetAt(index + 1);
		if (p->IsEnable()) {
			return p;
		}
	}
	return NULL;
}
// 复制灯带
// 将nIndexOrig灯带复制给nIndexDest灯带
void CLedMgr::CopyLedBar(int nIndexOrig, int nIndexDest)
{
	CLedBar* pOrig = m_aryLedBars.GetAt(nIndexOrig);
	CLedBar* pNew = CLedBar::Copy(pOrig);
	CPoint pt = GetPortDrawPos(nIndexDest);
	pNew->SetDrawPos(pt.x, pt.y);
	delete pOrig;
	m_aryLedBars.SetAt(nIndexDest, pNew);
}

// 绘图
void CLedMgr::DrawAll(CDC* pdc, float scale)
{
	this->DrawRules(pdc);
	this->DrawHub(pdc);
	this->DrawLedBars(pdc, scale);
}

// 绘制背景及标尺
void CLedMgr::DrawRules(CDC* pdc)
{
	int i;
	int l = m_ptDrawPos.x + HUB_INFO_W + HUB_UNIT_W;
	int t = m_ptDrawPos.y - RULER_H;
	int r = l + RULER_W;
	int b = m_ptDrawPos.y - 1;
	// 绘制基线
	pdc->MoveTo(l, b);
	pdc->LineTo(r, b);
	// 绘制刻度
	for (i=0; i<RULER_W; i++) {
		if (i % LED_DRAW_W == 0) {
			// 绘制灯格
			pdc->MoveTo(l+i, b-RULER_UNIT_SCALE_H);
			pdc->LineTo(l+i, b);
			int real_pos = (int)(LED_REAL_W * (i / LED_DRAW_W));
			if ((real_pos % 100) == 0) {
				// 绘制分米刻度
				pdc->MoveTo(l+i, b-RULER_DM_SCALE_H);
				pdc->LineTo(l+i, b);
			}
			if ((real_pos % 500) == 0) {
				// 绘制半米刻度
				pdc->MoveTo(l+i, b-RULER_HM_SCALE_H);
				pdc->LineTo(l+i, b);
			}
			if ((real_pos % 1000) == 0) {
				// 绘制米刻度
				pdc->MoveTo(l+i, b-RULER_M_SCALE_H);
				pdc->LineTo(l+i, b);
				// 绘制米数
				CString num;
				num.Format(_T("%dM"), real_pos / 1000);
				pdc->TextOut(l+i+5, b-RULER_M_SCALE_H, num);
			}
		}
	}
	// 绘制辅助线
	if (GetLedBar(m_nCurIndex)->IsEnable()) {
		CPen bluePen;
		bluePen.CreatePen(PS_SOLID, 1, RGB(99, 66, 255));
		CObject* pOldPen = pdc->SelectObject(&bluePen);
		int nCurLedDots = GetLedBar(m_nCurIndex)->GetLedCount();
		int x = l + LED_DRAW_W * nCurLedDots;
		pdc->MoveTo(x, 0);
		pdc->LineTo(x, b + HUB_UNIT_H * CONTROL_BAR_COUNT);
		pdc->SelectObject(pOldPen);
		DeleteObject(bluePen);
	}
}

// 绘制控制器
void CLedMgr::DrawHub(CDC* pdc)
{
	int i;
	CDC* pPortUnitDC = NULL;
	int nSize = (int)m_aryLedBars.GetSize();
	for (i=0; i<nSize; i++) {
		int l = m_ptDrawPos.x;
		int t = m_ptDrawPos.y + i * HUB_UNIT_H;
		int t2 = t + HUB_UNIT_H / 2 - 2;
		// 绘制控制器信息
		CString info;
		CString info2;
		CLedBar* pBar = this->GetLedBar(i);
		if (pBar->IsEnable()) {
			info.Format(_T("LED dots:%d"), pBar->GetLedCount());
			info2.Format(_T("Length:%.0fCM"), pBar->GetBarLength());
			pdc->TextOut(l, t+1, info);
			pdc->TextOutW(l, t2, info2);
		}
		// 绘制控制器单元
		l += HUB_INFO_W;
		if (m_nCurIndex == i) {
			pPortUnitDC = m_pDoc->m_pImgMgr->GetImgDC(IDB_PORTFOCUS);
		} else {
			if (pBar->IsEnable()) {
				pPortUnitDC = m_pDoc->m_pImgMgr->GetImgDC(IDB_PORTENABLE);
			} else {
				pPortUnitDC = m_pDoc->m_pImgMgr->GetImgDC(IDB_PORTUNIT);
			}
		}
		pdc->BitBlt(l, t, HUB_UNIT_W, HUB_UNIT_H, pPortUnitDC, 0, 0, SRCCOPY);
		if (m_nCurIndex != i) {
			CString num;
			num.Format(_T("<%d>"), i+1);
			int bkmode = pdc->SetBkMode(TRANSPARENT);
			pdc->TextOut(l+5, t+2, num);
			pdc->SetBkMode(bkmode);
		}
	}
}

// 绘制灯带
void CLedMgr::DrawLedBars(CDC* pdc, float scale)
{
	int i;
	int nSize = (int)m_aryLedBars.GetSize();
	for (i=0; i<nSize; i++) {
		CLedBar* pBar = m_aryLedBars.GetAt(i);
		pBar->DrawBar(pdc, scale);
	}
}

CPoint CLedMgr::GetPortDrawPos(int index)
{
	int l = m_ptDrawPos.x + HUB_INFO_W + HUB_UNIT_W;
	int t = m_ptDrawPos.y + index * HUB_UNIT_H;
	CPoint pt(l, t);
	return pt;
}

// 响应鼠标点击
void CLedMgr::MouseClick(CLedEditorView* pView, CPoint pt)
{
	int i;
	int nSize = (int)m_aryLedBars.GetSize();
	for (i=0; i<nSize; i++) {
		int l = m_ptDrawPos.x + HUB_INFO_W;
		int t = m_ptDrawPos.y + i * HUB_UNIT_H;
		int r = l + HUB_UNIT_W;
		int b = t + HUB_UNIT_H;
		CRect rc(l, t, r, b);
		if (rc.PtInRect(pt)) {
			this->m_nCurIndex = i;
			CLedBar* pBar = GetLedBar(i);
			pView->SetBtnPos(rc, pBar->IsEnable());
			return;
		}
	}
}

// 获得某条灯带的前导偏移量（独立灯带偏移量为0）
int CLedMgr::GetOffset(int nBarIndex)
{
	CLedBar* pBar = m_aryLedBars.GetAt(nBarIndex);
	if (pBar->IsIndependent()) {
		return 0;
	} else {
		int offset = 0;
		while (nBarIndex > 0) {
			nBarIndex --;
			pBar = m_aryLedBars.GetAt(nBarIndex);
			if (pBar->IsEnable()) {
				offset += pBar->GetLedCount();
				if (pBar->IsIndependent()) {
					return offset;
				}
			}
		}
	}
	// 出错了，正常不会走到这里
	return -1;
}

// 新建
void CLedMgr::NewData() {
	RemoveAll();
	Init(CONTROL_BAR_COUNT);
}

// 保存
BOOL CLedMgr::Save(CString& strFile)
{
	int i;
	CString strInfo = _T("");
	int nSize = (int)m_aryLedBars.GetSize();
	for (i=0; i<nSize; i++) {
		CLedBar* p = m_aryLedBars.GetAt(i);
		CString strSubInfo;
		p->Save(strSubInfo);
		strInfo += strSubInfo;
		strInfo += _T("\r\n");
	}
	strInfo.TrimRight(_T("\r\n"));
	strFile += strInfo;
	return TRUE;
}

// 导入
BOOL CLedMgr::Load(CString strFile)
{
	int i;
	CStringArray sar;
	int count = Split(sar, strFile, _T("\r\n"));

	int nSize = (int)m_aryLedBars.GetSize();
	for (i=0; i<nSize; i++) {
		CLedBar* p = m_aryLedBars.GetAt(i);
		if (i < count) {
			if (!p->Load(sar[i])) {
				return FALSE;
			}
		} else {
			p->NewData();
		}
	}
	return TRUE;
}
