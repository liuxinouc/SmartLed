// LedEditorDoc.h : CLedEditorDoc 类的接口
//


#pragma once

class CLedModeOption;
class CLedEditorView;
class CImgMgr;
class CLedMgr;
class CLedEditorDoc : public CDocument
{
protected: // 仅从序列化创建
	CLedEditorDoc();
	DECLARE_DYNCREATE(CLedEditorDoc)

private:
	CLedEditorView* m_pView;

// 属性
public:
	CLedModeOption* m_pModeOpt;
	CImgMgr* m_pImgMgr;
	CLedMgr* m_pLedMgr;

// 操作
public:
	void InitImgMgr(CDC* pdc);
	void SetView(CLedEditorView* pView);

// 重写
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// 实现
public:
	virtual ~CLedEditorDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
public:
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
};


