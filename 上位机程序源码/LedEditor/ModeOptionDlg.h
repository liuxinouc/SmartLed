#pragma once
#include "afxwin.h"
#include "ImageButton.h"
#include "ColorSelectComboBox.h"

// CModeOptionDlg 对话框
class CLedBar;
class CLedMode;
class CLedModeOption;
class CSetupLedBarDlg;
class CModeOptionDlg : public CDialog
{
	DECLARE_DYNAMIC(CModeOptionDlg)

public:
	CModeOptionDlg(CWnd* pParent, CLedEditorView* pView, CLedBar* pLedBar, CLedMode* pMode, BOOL bEdit);
	virtual ~CModeOptionDlg();

private:
	CLedModeOption* m_pOptions;
	CMap<int, int, int, int> m_mapIndexDict;

	CSetupLedBarDlg* m_pSetupDlg;
	CLedEditorView* m_pView;
	CLedBar* m_pLedBar;
	CLedMode* m_pMode;
	BOOL m_bEditOp;

// 对话框数据
	enum { IDD = IDD_MODE_OPTION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

private:
	void SetControlStatus(int nType, CColorSelectComboBox& cb, CEdit& ed);
	void AddModeParam(CStringArray& saParams, int nType, CColorSelectComboBox& cb, CEdit& ed);
	void GetModeParam(CStringArray& saParams, int nIndex, int nType, CColorSelectComboBox& cb, CEdit& ed);
	int ModeTypeToComboIndex(int nTypeIndex);
	void UpdateParameters(BOOL bUp);

public:
	CImageButton m_btnStart;
	CImageButton m_btnStop;

	CComboBox m_cbModeSel;
	CEdit m_edModeDesc;
	CColorSelectComboBox m_cbP1;
	CColorSelectComboBox m_cbP2;
	CColorSelectComboBox m_cbP3;
	CColorSelectComboBox m_cbP4;
	CColorSelectComboBox m_cbP5;
	CColorSelectComboBox m_cbP6;
	CColorSelectComboBox m_cbP7;
	CColorSelectComboBox m_cbP8;
	CColorSelectComboBox m_cbP9;
	CColorSelectComboBox m_cbP10;
	CColorSelectComboBox m_cbP11;
	CColorSelectComboBox m_cbP12;
	CColorSelectComboBox m_cbP13;
	CColorSelectComboBox m_cbP14;
	CColorSelectComboBox m_cbP15;
	CColorSelectComboBox m_cbP16;
	CColorSelectComboBox m_cbP17;
	CColorSelectComboBox m_cbP18;
	CEdit m_edP1;
	CEdit m_edP2;
	CEdit m_edP3;
	CEdit m_edP4;
	CEdit m_edP5;
	CEdit m_edP6;
	CEdit m_edP7;
	CEdit m_edP8;
	CEdit m_edP9;
	CEdit m_edP10;
	CEdit m_edP11;
	CEdit m_edP12;
	CEdit m_edP13;
	CEdit m_edP14;
	CEdit m_edP15;
	CEdit m_edP16;
	CEdit m_edP17;
	CEdit m_edP18;
	CEdit m_descP1;
	CEdit m_descP2;
	CEdit m_descP3;
	CEdit m_descP4;
	CEdit m_descP5;
	CEdit m_descP6;
	CEdit m_descP7;
	CEdit m_descP8;
	CEdit m_descP9;
	CEdit m_descP10;
	CEdit m_descP11;
	CEdit m_descP12;
	CEdit m_descP13;
	CEdit m_descP14;
	CEdit m_descP15;
	CEdit m_descP16;
	CEdit m_descP17;
	CEdit m_descP18;

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedBtnStart2();
	afx_msg void OnBnClickedBtnStop2();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnCbnSelchangeModeSel();
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
