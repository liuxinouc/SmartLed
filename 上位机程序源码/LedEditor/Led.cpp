#include "StdAfx.h"
#include "Led.h"

CLed::CLed(void)
{
	m_dwColor = LED_COLOR_DEFAULT;
}

CLed::~CLed(void)
{
}

CLed* CLed::Copy(CLed* pOrig)
{
	CLed* pNewLed = new CLed;
	pNewLed->m_dwColor = pOrig->m_dwColor;
	return pNewLed;
}

// 根据颜色推算出消耗电流，单位安培
float CLed::GetAmpere()
{
	return 0.01f;
}