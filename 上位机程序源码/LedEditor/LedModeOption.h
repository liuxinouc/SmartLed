#pragma once

typedef enum _EnParamType
{
	INVAL = 0,
	COLOR = 1,  // 灯管颜色
	COUNT = 2,  // 灯管数量
	DELAY = 3,  // 延迟时间（以秒为单位）
	SPEED = 4   // 流动速度
} EnParamType;

typedef struct _StLedModeOption
{
	CString sName;
	CString sDesc;
	int nCodeSize;
	EnParamType eParam1Type;
	CString sParam1Desc;
	EnParamType eParam2Type;
	CString sParam2Desc;
	EnParamType eParam3Type;
	CString sParam3Desc;
	EnParamType eParam4Type;
	CString sParam4Desc;
	EnParamType eParam5Type;
	CString sParam5Desc;
	EnParamType eParam6Type;
	CString sParam6Desc;
	EnParamType eParam7Type;
	CString sParam7Desc;
	EnParamType eParam8Type;
	CString sParam8Desc;
	EnParamType eParam9Type;
	CString sParam9Desc;
	EnParamType eParam10Type;
	CString sParam10Desc;
	EnParamType eParam11Type;
	CString sParam11Desc;
	EnParamType eParam12Type;
	CString sParam12Desc;
	EnParamType eParam13Type;
	CString sParam13Desc;
	EnParamType eParam14Type;
	CString sParam14Desc;
	EnParamType eParam15Type;
	CString sParam15Desc;
	EnParamType eParam16Type;
	CString sParam16Desc;
	EnParamType eParam17Type;
	CString sParam17Desc;
	EnParamType eParam18Type;
	CString sParam18Desc;
} StLedModeOption;

class CLedModeOption
{
private:
	static CLedModeOption* m_pInstance;
	static int m_nCodeSizeMin;

public:
	CArray<StLedModeOption*, StLedModeOption*> m_aryOptions;

public:
	CLedModeOption(void);
	~CLedModeOption(void);

	static CLedModeOption* GetOptions();
	static int GetCodeSizeMin();
};
