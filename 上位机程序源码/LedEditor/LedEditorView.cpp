// LedEditorView.cpp : CLedEditorView 类的实现
//

#include "stdafx.h"
#include "LedEditor.h"
#include "LedEditorDoc.h"
#include "LedEditorView.h"
#include "SetupLedBarDlg.h"
#include "ProgramDlg.h"
#include "LedMode.h"
#include "LedModeMgr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CLedEditorView
IMPLEMENT_DYNCREATE(CLedEditorView, CScrollView)

BEGIN_MESSAGE_MAP(CLedEditorView, CScrollView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CScrollView::OnFilePrintPreview)
	ON_WM_LBUTTONUP()
	ON_BN_CLICKED(IDC_CHECK, &CLedEditorView::OnBnClickedCheck)
	ON_BN_CLICKED(IDC_SETUP, &CLedEditorView::OnBnClickedSetup)
	ON_COMMAND(ID_PROGRAM, &CLedEditorView::OnProgram)
	ON_COMMAND(ID_SCALE_1, &CLedEditorView::OnScale1)
	ON_COMMAND(ID_SCALE_2, &CLedEditorView::OnScale2)
	ON_COMMAND(ID_SCALE_3, &CLedEditorView::OnScale3)
	ON_COMMAND(ID_SPEED_1, &CLedEditorView::OnSpeed1)
	ON_COMMAND(ID_SPEED_2, &CLedEditorView::OnSpeed2)
	ON_COMMAND(ID_SPEED_3, &CLedEditorView::OnSpeed3)
	ON_COMMAND(ID_SPEED_4, &CLedEditorView::OnSpeed4)
	ON_COMMAND(ID_SPEED_5, &CLedEditorView::OnSpeed5)
	ON_COMMAND(ID_SPEED_6, &CLedEditorView::OnSpeed6)
	ON_COMMAND(ID_SIM_PLAY, &CLedEditorView::OnSimPlay)
	ON_COMMAND(ID_SIM_STOP, &CLedEditorView::OnSimStop)
	ON_UPDATE_COMMAND_UI(ID_SCALE_1, &CLedEditorView::OnUpdateScale1)
	ON_UPDATE_COMMAND_UI(ID_SCALE_2, &CLedEditorView::OnUpdateScale2)
	ON_UPDATE_COMMAND_UI(ID_SCALE_3, &CLedEditorView::OnUpdateScale3)
	ON_UPDATE_COMMAND_UI(ID_SPEED_1, &CLedEditorView::OnUpdateSpeed1)
	ON_UPDATE_COMMAND_UI(ID_SPEED_2, &CLedEditorView::OnUpdateSpeed2)
	ON_UPDATE_COMMAND_UI(ID_SPEED_3, &CLedEditorView::OnUpdateSpeed3)
	ON_UPDATE_COMMAND_UI(ID_SPEED_4, &CLedEditorView::OnUpdateSpeed4)
	ON_UPDATE_COMMAND_UI(ID_SPEED_5, &CLedEditorView::OnUpdateSpeed5)
	ON_UPDATE_COMMAND_UI(ID_SPEED_6, &CLedEditorView::OnUpdateSpeed6)
	ON_UPDATE_COMMAND_UI(ID_SIM_PLAY, &CLedEditorView::OnUpdateSimPlay)
	ON_UPDATE_COMMAND_UI(ID_SIM_STOP, &CLedEditorView::OnUpdateSimStop)
	ON_WM_TIMER()
END_MESSAGE_MAP()

// CLedEditorView 构造/析构
CLedEditorView::CLedEditorView()
{
	m_fDrawScale = 1.0;
	m_bInitialed = FALSE;
	m_bSimRunning = FALSE;
}

CLedEditorView::~CLedEditorView()
{
}

BOOL CLedEditorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CScrollView::PreCreateWindow(cs);
}

// CLedEditorView 绘制
static BOOL bFirst = TRUE;
void CLedEditorView::OnDraw(CDC* pdc)
{
	CLedEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	if (bFirst) {
		pDoc->InitImgMgr(pdc);
		pDoc->m_pLedMgr->SetCurBarBtn(this);
		bFirst = FALSE;
	}

	// TODO: 在此处为本机数据添加绘制代码
	pDoc->m_pLedMgr->DrawAll(pdc, m_fDrawScale);
}

// CLedEditorView 打印
BOOL CLedEditorView::OnPreparePrinting(CPrintInfo* pInfo)
{
	OnSimStop();
	return DoPreparePrinting(pInfo);
}

void CLedEditorView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CLedEditorView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清除过程
}


// CLedEditorView 诊断
#ifdef _DEBUG
void CLedEditorView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CLedEditorView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CLedEditorDoc* CLedEditorView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CLedEditorDoc)));
	return (CLedEditorDoc*)m_pDocument;
}
#endif //_DEBUG


// CLedEditorView 消息处理程序
void CLedEditorView::OnLButtonUp(UINT nFlags, CPoint point)
{
	OnSimStop();
	// ScrollView需要进行坐标转换
	CClientDC dc(this);
	this->OnPrepareDC(&dc);
	dc.DPtoLP(&point);

	CLedEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	pDoc->m_pLedMgr->MouseClick(this, point);
	this->Invalidate(FALSE);
	CScrollView::OnLButtonUp(nFlags, point);
}

void CLedEditorView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
	if (!m_bInitialed) {
		m_btnCheck.Create(_T(""), WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX, CRect(10,10,30,30), this, IDC_CHECK);
		m_btnSetup.Create(_T("Setup"), WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON, CRect(10,70,30,90), this, IDC_SETUP);
		m_bInitialed = TRUE;
		CLedEditorDoc* pDoc = GetDocument();
		ASSERT_VALID(pDoc);
		pDoc->SetView(this);
	}
	SetScrollSizes(MM_TEXT,CSize(CLIENT_W, CLIENT_H));
}

BOOL CLedEditorView::DestroyWindow()
{
	OnSimStop();
	m_btnCheck.DestroyWindow();
	m_btnSetup.DestroyWindow();
	return CScrollView::DestroyWindow();
}

void CLedEditorView::SetBtnPos(CRect rc, BOOL bEnable)
{
	// ScrollView需要进行坐标转换
	CClientDC dc(this);
	this->OnPrepareDC(&dc);
	dc.LPtoDP(&rc);

	CRect rcCheck;
	CRect rcSetup;
	rcCheck.SetRect(rc.left + 6, rc.top + 12, rc.left + 22, rc.top + 28);
	m_btnCheck.MoveWindow(&rcCheck, TRUE);
	m_btnCheck.ShowWindow(SW_SHOW);
	m_btnCheck.SetCheck(bEnable);
	rcSetup.SetRect(rc.left + 28, rc.top + 6, rc.left + 106, rc.top + 34);
	m_btnSetup.MoveWindow(&rcSetup, TRUE);
	m_btnSetup.ShowWindow(bEnable ? SW_SHOW : SW_HIDE);
	this->InvalidateRect(rc);
}

void CLedEditorView::OnBnClickedCheck()
{
	OnSimStop();
	BOOL bCheck = m_btnCheck.GetCheck();
	CLedEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	pDoc->m_pLedMgr->EnableCurBar(bCheck);
	pDoc->m_pLedMgr->SetCurBarBtn(this);
	pDoc->SetModifiedFlag();
	this->Invalidate();
}

void CLedEditorView::OnBnClickedSetup()
{
	OnSimStop();
	CLedEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	CSetupLedBarDlg dlg(this, pDoc->m_pLedMgr->GetCurLedBar());
	dlg.DoModal();
}

void CLedEditorView::OnProgram()
{
	OnSimStop();
	CProgramDlg dlg(this);
	dlg.DoModal();
}

void CLedEditorView::OnScale1()
{
	// TODO: 在此添加命令处理程序代码
}

void CLedEditorView::OnScale2()
{
	// TODO: 在此添加命令处理程序代码
}

void CLedEditorView::OnScale3()
{
	// TODO: 在此添加命令处理程序代码
}

void CLedEditorView::OnSpeed1()
{
	// TODO: 在此添加命令处理程序代码
}

void CLedEditorView::OnSpeed2()
{
	// TODO: 在此添加命令处理程序代码
}

void CLedEditorView::OnSpeed3()
{
	// TODO: 在此添加命令处理程序代码
}

void CLedEditorView::OnSpeed4()
{
	// TODO: 在此添加命令处理程序代码
}

void CLedEditorView::OnSpeed5()
{
	// TODO: 在此添加命令处理程序代码
}

void CLedEditorView::OnSpeed6()
{
	// TODO: 在此添加命令处理程序代码
}

void CLedEditorView::OnUpdateScale1(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
}

void CLedEditorView::OnUpdateScale2(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
}

void CLedEditorView::OnUpdateScale3(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
}

void CLedEditorView::OnUpdateSpeed1(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
}

void CLedEditorView::OnUpdateSpeed2(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
}

void CLedEditorView::OnUpdateSpeed3(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
}

void CLedEditorView::OnUpdateSpeed4(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
}

void CLedEditorView::OnUpdateSpeed5(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
}

void CLedEditorView::OnUpdateSpeed6(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
}

void CLedEditorView::OnUpdateSimPlay(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
}

void CLedEditorView::OnUpdateSimStop(CCmdUI *pCmdUI)
{
	// TODO: 在此添加命令更新用户界面处理程序代码
}

void CLedEditorView::OnSimPlay()
{
	// 灯带开始模拟显示
	m_bSimRunning = TRUE;

	CLedEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	CLedMgr* pMgr = pDoc->m_pLedMgr;
	int i;
	for (i=0; i<CONTROL_BAR_COUNT; i++) {
		CLedBar* pBar = pMgr->GetLedBar(i);
		pBar->m_nSimCurModeIndex = 0;
		int offset = pMgr->GetOffset(i);
		pBar->GetModeMgr()->SimResetAll(offset);
	}

	SetTimer(ID_SIM_TIMER, SIM_TIMER_PERIOD, NULL);
}

void CLedEditorView::OnSimStop()
{
	if (m_bSimRunning) {
		KillTimer(ID_SIM_TIMER);
		m_bSimRunning = FALSE;

		CLedEditorDoc* pDoc = GetDocument();
		ASSERT_VALID(pDoc);
		int i;
		for (i=0; i<CONTROL_BAR_COUNT; i++) {
			CLedBar* pBar = pDoc->m_pLedMgr->GetLedBar(i);
			pBar->SetDefaultLedColor();
		}
	}

	// 刷新显示
	CRect rc(RULER_LEFT, HUB_DRAW_Y, RULER_LEFT+RULER_W, HUB_DRAW_Y+HUB_UNIT_H * CONTROL_BAR_COUNT+2);
	this->InvalidateRect(&rc, 0);
}

void CLedEditorView::OnTimer(UINT_PTR nIDEvent)
{
	CLedEditorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	BOOL bHasEnabled = FALSE;
	int i;
	for (i=0; i<CONTROL_BAR_COUNT; i++) {
		CLedBar* pBar = pDoc->m_pLedMgr->GetLedBar(i);
		if (pBar->IsEnable()) {
			CLedModeMgr* pMgr = pBar->GetModeMgr();
			if (pMgr->GetCount() > 0) {
				// 设置当前灯带的显示方式
				if (pMgr->Get(pBar->m_nSimCurModeIndex)->SimRun(pBar)) {
					pBar->m_nSimCurModeIndex ++;
					if (pBar->m_nSimCurModeIndex >= pMgr->GetCount()) {
						pBar->m_nSimCurModeIndex = 0;
					}
					int offset = pDoc->m_pLedMgr->GetOffset(i);
					pMgr->Get(pBar->m_nSimCurModeIndex)->SimReset(offset);
				}
				bHasEnabled = TRUE;
			}
		}
	}

	if (!bHasEnabled) {
		OnSimStop();
	}

	// 刷新显示
	CRect rc(RULER_LEFT, HUB_DRAW_Y, RULER_LEFT+RULER_W, HUB_DRAW_Y+HUB_UNIT_H * CONTROL_BAR_COUNT+2);
	this->RenewRect(&rc, 0);
	CScrollView::OnTimer(nIDEvent);
}

void CLedEditorView::RenewRect(CRect* prc, BOOL bErase)
{
	// ScrollView需要进行坐标转换
	CClientDC dc(this);
	this->OnPrepareDC(&dc);
	dc.LPtoDP(prc);
	this->InvalidateRect(prc, bErase);
}

