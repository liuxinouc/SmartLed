#include "StdAfx.h"
#include "LedBar.h"
#include "LedModeMgr.h"
#include "SmartDC.h"

CLedBar::CLedBar(void)
{
	m_nUnitDrawWidth = LED_DRAW_W;
	m_nUnitDrawHeight = HUB_UNIT_H;
	m_fUnitWidth = LED_REAL_W;
	m_fUnitHeight = LED_REAL_H;
	m_bIndependent = TRUE;
	m_pModeMgr = new CLedModeMgr();
	m_nSimCurModeIndex = 0;
	NewData();
}

CLedBar::~CLedBar(void)
{
	int i;
	int nSize = (int)m_aryLeds.GetSize();
	if (nSize > 0) {
		for (i=0; i<nSize; i++) {
			CLed* p = m_aryLeds.GetAt(i);
			delete p;
		}
		m_aryLeds.RemoveAll();
	}

	if (m_pModeMgr != NULL) {
		delete m_pModeMgr;
		m_pModeMgr = NULL;
	}
}

CLedBar* CLedBar::Copy(CLedBar* pOrig)
{
	int i;
	CLedBar* pNewBar = new CLedBar();
	pNewBar->m_bEnable = pOrig->m_bEnable;
	pNewBar->m_nUnitDrawWidth = pOrig->m_nUnitDrawWidth;
	pNewBar->m_nUnitDrawHeight = pOrig->m_nUnitDrawHeight;
	pNewBar->m_fUnitWidth = pOrig->m_fUnitWidth;
	pNewBar->m_fUnitHeight = pOrig->m_fUnitHeight;
	pNewBar->m_bIndependent = pOrig->m_bIndependent;

	int nSize = pOrig->GetLedCount();
	for (i=0; i<nSize; i++) {
		CLed* pLed = pOrig->m_aryLeds.GetAt(i);
		CLed* pNewLed = CLed::Copy(pLed);
		pNewBar->m_aryLeds.Add(pNewLed);
	}

	pNewBar->m_pModeMgr = CLedModeMgr::Copy(pOrig->m_pModeMgr);
	return pNewBar;
}

// 设置使用状态
void CLedBar::Enable(BOOL enable)
{
	m_bEnable = enable;
}

BOOL CLedBar::IsEnable()
{
	return m_bEnable;
}

// 设置独立状态
void CLedBar::Independent(BOOL independent)
{
	m_bIndependent = independent;
}

BOOL CLedBar::IsIndependent()
{
	return m_bIndependent;
}

// 设置灯带的绘制位置
void CLedBar::SetDrawPos(int x, int y)
{
	m_ptDrawPos.SetPoint(x, y);
}

// 设置灯的显示颜色
void CLedBar::SetLedColor(int index, DWORD color)
{
	CLed* pled = m_aryLeds.GetAt(index);
	pled->m_dwColor = color;
}

// 向前推移灯带显示
void CLedBar::Shift()
{
	int i;
	int nSize = (int)m_aryLeds.GetSize();
	for (i=nSize-1; i>0; i--) {
		m_aryLeds.GetAt(i)->m_dwColor = m_aryLeds.GetAt(i-1)->m_dwColor;
	}
}

// 设置整条灯带为默认颜色
void CLedBar::SetDefaultLedColor()
{
	int i;
	int nSize = (int)m_aryLeds.GetSize();
	for (i=0; i<nSize; i++) {
		m_aryLeds.GetAt(i)->m_dwColor = LED_COLOR_DEFAULT;
	}
}

// 绘制灯带
void CLedBar::DrawBar(CDC* pdc, float scale)
{
	if (!m_bEnable) {
		return;
	}

	// 使用缓冲绘制，避免闪烁
	CRect rc;
	GetRect(rc, scale);
	CSmartDC dc(pdc, &rc);

	float w = m_nUnitDrawWidth * scale;
	float h = m_nUnitDrawHeight * scale;
	int i;
	int nSize = (int)m_aryLeds.GetSize();

	// 独立灯带与非独立灯带的边框颜色不同
	if (m_bIndependent) {
		dc.BitBlt(m_ptDrawPos.x, m_ptDrawPos.y, (int)w*nSize+1, (int)h, pdc,0,0,BLACKNESS);
	} else {
		dc.BitBlt(m_ptDrawPos.x, m_ptDrawPos.y, (int)w*nSize+1, (int)h, pdc,0,0,WHITENESS);
	}

	// 灯体
	for (i=0; i<nSize; i++) {
		CLed* p = m_aryLeds.GetAt(i);
		CBrush br(p->m_dwColor);
		int l = (int)(m_ptDrawPos.x + w * i + 1);
		int t = (int)(m_ptDrawPos.y + 1);
		int r = (int)(l + w - 1);
		int b = (int)(t + h - 2);
		CRect rc(l, t, r, b);
		dc.FillRect(&rc, &br);
	}
}

// 获得灯带的绘制矩形
void CLedBar::GetRect(CRect& rc, float scale)
{
	float w = m_nUnitDrawWidth * scale;
	float h = m_nUnitDrawHeight * scale;
	int nSize = (int)m_aryLeds.GetSize();
	CPoint ptBottomRight(m_ptDrawPos.x + (int)w*nSize+1, m_ptDrawPos.y + (int)h);
	rc.SetRect(m_ptDrawPos, ptBottomRight);
}

// 设置灯带上灯的数量
int CLedBar::SetLedCount(int count)
{
	int i;
	int nSize = (int)m_aryLeds.GetSize();
	if (nSize == count) {
		return count;
	}
	if (nSize < count) {
		// 增加新灯泡
		int nAddCount = count - nSize;
		for (i=0; i<nAddCount; i++) {
			CLed* p = new CLed();
			m_aryLeds.Add(p);
		}
	} else {
		// 剪掉多余的灯泡
		int nSubCount = nSize - count;
		int nPos = (count > 1) ? (count-1) : 0;
		for (i=0; i<nSubCount; i++) {
			CLed* p = m_aryLeds.GetAt(nPos);
			delete p;
			m_aryLeds.RemoveAt(nPos);
		}
	}
	return GetLedCount();
}

// 获得灯带上灯的数量
int CLedBar::GetLedCount()
{
	return (int)m_aryLeds.GetSize();
}

// 获得灯带的总长度
// 单位毫米
float CLedBar::GetBarLength()
{
	return m_fUnitWidth * this->GetLedCount();
}

// 计算灯带总电流
// 单位安培
float CLedBar::GetAmpere()
{
	int nSize = (int)m_aryLeds.GetSize();
	int i;
	float ampere = 0.0;
	for (i=0; i<nSize; i++) {
		CLed* pled = m_aryLeds.GetAt(i);
		ampere += pled->GetAmpere();
	}
	return ampere;
}

// 新建
void CLedBar::NewData()
{
	if (m_pModeMgr != NULL) {
		delete m_pModeMgr;
	}
	m_pModeMgr = new CLedModeMgr();
}

// 获得工作模式管理器
CLedModeMgr* CLedBar::GetModeMgr()
{
	return m_pModeMgr;
}
void CLedBar::SetModeMgr(CLedModeMgr* pMode)
{
	if (m_pModeMgr != NULL) {
		delete m_pModeMgr;
	}
	m_pModeMgr = pMode;
}

// 保存
BOOL CLedBar::Save(CString& strOutput)
{
	if (m_bEnable) {
		strOutput += _T("[Enable]#");
	} else {
		strOutput += _T("[Disable]#");
	}

	CString strLedCount;
	int nLedCount = this->GetLedCount();
	strLedCount.Format(_T("[%d]#"), nLedCount);
	strOutput += strLedCount;

	if (m_bIndependent) {
		strOutput += _T("[Independ]#");
	} else {
		strOutput += _T("[Depend]#");
	}

	CString strInfo = _T("");
	m_pModeMgr->Save(strInfo);
	strOutput += strInfo;
	return TRUE;
}

// 导入
BOOL CLedBar::Load(CString strInput)
{
	CStringArray sar;
	int count = Split(sar, strInput, _T("#"));
	if (count < 4) {
		return FALSE;
	}

	CString strEnable = sar[0];
	m_bEnable = (strEnable == _T("[Enable]")) ? TRUE : FALSE;
	CString strLedCount = sar[1];
	strLedCount.TrimLeft(_T("["));
	strLedCount.TrimRight(_T("]"));
	int nLedCount = _ttoi(strLedCount);
	this->SetLedCount(nLedCount);
	CString strIndependent = sar[2];
	m_bIndependent = (strIndependent == _T("[Independ]")) ? TRUE : FALSE;
	return m_pModeMgr->Load(sar[3]);
}

// 根据上一条灯带的配置，设置本灯带的参数
void CLedBar::CopyMode(CLedBar* pPreBar)
{
	if (m_pModeMgr != NULL) {
		delete m_pModeMgr;
	}
	m_pModeMgr = CLedModeMgr::Copy(pPreBar->m_pModeMgr);
}

