#pragma once

class CLed;
class CLedModeMgr;
class CLedBar
{
public:
	CLedBar(void);
	virtual ~CLedBar(void);

private:
	BOOL m_bEnable;

	// 绘图参数
	CPoint m_ptDrawPos;
	int m_nUnitDrawWidth;
	int m_nUnitDrawHeight;

	// 实际参数
	float m_fUnitWidth;      // 单元宽度，单位毫米
	float m_fUnitHeight;     // 单元高度，单位毫米

	// 管理灯带上的每个灯
	CArray<CLed*, CLed*> m_aryLeds;

	// 是否独立工作
	BOOL m_bIndependent;

	// 管理灯带的工作模式
	CLedModeMgr* m_pModeMgr;

public:
	// 与模拟显示有关的变量
	int m_nSimCurModeIndex;

public:
	// 拷贝
	static CLedBar* Copy(CLedBar* pOrig);

	// 设置使用状态
	void Enable(BOOL enable);
	BOOL IsEnable();

	// 设置独立状态
	void Independent(BOOL independent);
	BOOL IsIndependent();

	// 设置灯带的绘制位置
	void SetDrawPos(int x, int y);

	// 设置灯的显示颜色
	void SetLedColor(int index, DWORD color);

	// 向前推移灯带显示
	void Shift();

	// 设置整条灯带为默认颜色
	void SetDefaultLedColor();

	// 绘制灯带
	void DrawBar(CDC* pdc, float scale);

	// 获得灯带的绘制矩形
	void GetRect(CRect& rc, float scale);

	// 设置灯带上灯的数量
	int SetLedCount(int count);

	// 获得灯带上灯的数量
	int GetLedCount();

	// 获得灯带的总长度
	// 单位毫米
	float GetBarLength();

	// 计算灯带总电流
	// 单位安培
	float GetAmpere();

	// 获得工作模式管理器
	CLedModeMgr* GetModeMgr();
	void SetModeMgr(CLedModeMgr* pMode);

	// 存储/导入
	void NewData();
	BOOL Save(CString& strOutput);
	BOOL Load(CString strInput);

	// 根据上一条灯带的配置，设置本灯带的参数
	void CopyMode(CLedBar* pPreBar);
};
