#pragma once

// Led灯条管理类
class CLedModeMgr;
class CLedEditorDoc;
class CLedEditorView;
class CLedMgr
{
public:
	CLedMgr(CLedEditorDoc* pDoc);
	virtual ~CLedMgr(void);

private:
	CLedEditorDoc* m_pDoc;

	// 灯带管理数组
	CArray<CLedBar*, CLedBar*> m_aryLedBars;

	// 灯带显示位置
	CPoint m_ptDrawPos;

	// 当前正在编辑的灯带
	int m_nCurIndex;

private:
	// 销毁
	void RemoveAll(void);

public:
	// 初始化
	// nLedCount是支持的最大灯带数量
	void Init(int nLedCount);

	// 使能/禁止灯带
	// nIndex：第几条灯带，0-based
	// bEnable：是否使能
	void EnableBar(int nIndex, BOOL bEnable);
	void EnableCurBar(BOOL bEnable);

	// 设置编辑按钮的状态和位置
	void SetCurBarBtn(CLedEditorView* pView);
	void ResetBtns(CLedEditorView* pView);

	// 获得灯带
	// nIndex：灯带索引
	CLedBar* GetLedBar(int nIndex);
	CLedBar* GetCurLedBar();
	int GetCurIndex();
	CLedBar* GetValidPreBar(int index);
	CLedBar* GetValidNextBar(int index);

	// 复制灯带
	// 将nIndexOrig灯带复制给nIndexDest灯带
	void CopyLedBar(int nIndexOrig, int nIndexDest);

	// 绘图
	void DrawAll(CDC* pdc, float scale);

	// 绘制背景及标尺
	void DrawRules(CDC* pdc);

	// 绘制控制器
	void DrawHub(CDC* pdc);

	// 绘制灯带
	void DrawLedBars(CDC* pdc, float scale);

	// 响应鼠标点击
	void MouseClick(CLedEditorView* pView, CPoint pt);

	// 存储/导入
	void NewData();
	BOOL Save(CString& strFile);
	BOOL Load(CString strFile);

	// 获得某条灯带的前导偏移量（独立灯带偏移量为0）
	int GetOffset(int nBarIndex);

private:
	// 灯带的绘制位置
	CPoint GetPortDrawPos(int index);
};
