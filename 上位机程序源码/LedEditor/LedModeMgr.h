#pragma once

class CLedMode;
class CLedModeMgr
{
public:
	CArray<CLedMode*, CLedMode*> m_aryLedModes;

private:
	void RemoveAll();

public:
	static CLedModeMgr* Copy(CLedModeMgr* pOrig);

public:
	CLedModeMgr(void);
	~CLedModeMgr(void);

	void Add(CLedMode* pMode);
	void Del(int index);
	CLedMode* Get(int index);
	void Insert(int index, CLedMode* pMode);
	int GetCount();

	void NewData();
	BOOL Save(CString& strOutput);
	BOOL Load(CString strInput);

	BOOL MoveUp(int index);
	BOOL MoveDown(int index);
	BOOL RemoveAt(int index);

	void SimResetAll(int offset);
};
