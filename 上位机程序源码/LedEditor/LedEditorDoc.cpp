// LedEditorDoc.cpp : CLedEditorDoc 类的实现
//

#include "stdafx.h"
#include "LedModeOption.h"
#include "LedEditor.h"
#include "LedEditorDoc.h"
#include "LedEditorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CLedEditorDoc
IMPLEMENT_DYNCREATE(CLedEditorDoc, CDocument)

BEGIN_MESSAGE_MAP(CLedEditorDoc, CDocument)
END_MESSAGE_MAP()


// CLedEditorDoc 构造/析构
CLedEditorDoc::CLedEditorDoc()
{
	m_pView = NULL;
	m_pModeOpt = new CLedModeOption;
	m_pImgMgr = new CImgMgr;
	m_pLedMgr = new CLedMgr(this);
	m_pLedMgr->NewData();
}

CLedEditorDoc::~CLedEditorDoc()
{
	if (m_pModeOpt != NULL) {
		delete m_pModeOpt;
		m_pModeOpt = NULL;
	}
	if (m_pImgMgr != NULL) {
		delete m_pImgMgr;
		m_pImgMgr = NULL;
	}
	if (m_pLedMgr != NULL) {
		delete m_pLedMgr;
		m_pLedMgr = NULL;
	}
}

void CLedEditorDoc::InitImgMgr(CDC* pdc)
{
	m_pImgMgr->Init(pdc);
}

void CLedEditorDoc::SetView(CLedEditorView* pView)
{
	m_pView = pView;
}
// CLedEditorDoc 序列化

void CLedEditorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring()) {
	} else {
	}
}

// CLedEditorDoc 诊断

#ifdef _DEBUG
void CLedEditorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLedEditorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

// CLedEditorDoc 命令
BOOL CLedEditorDoc::OnNewDocument()
{
	if (m_pView != NULL) {
		m_pView->OnSimStop();
	}
	if (!CDocument::OnNewDocument())
		return FALSE;

	// (SDI 文档将重用该文档)
	m_pLedMgr->NewData();
	return TRUE;
}

BOOL CLedEditorDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	if (m_pView != NULL) {
		m_pView->OnSimStop();
	}
	//if (!CDocument::OnOpenDocument(lpszPathName))
	//	return FALSE;

	// 读出磁盘文件
	HANDLE hFile;
	hFile = CreateFile(lpszPathName,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,NULL);
	if(hFile == INVALID_HANDLE_VALUE) {
		return FALSE;
	}

	DWORD dwRead = 0;
	BOOL bReadSt;
	DWORD dwFileSize = GetFileSize(hFile, NULL);
	if (dwFileSize <= 0) {
		return FALSE;
	}

	BYTE fileHeader[2];
	bReadSt = ReadFile (hFile, fileHeader, 2, &dwRead, NULL);
	if (!bReadSt || dwRead != 2) {
		CloseHandle (hFile);
		return FALSE;
	}
	if (fileHeader[0] != 0xFF || fileHeader[1] != 0xFE) {
		return FALSE;
	}

	dwFileSize -= 2;
	wchar_t* fileBuffer = (wchar_t*)new BYTE[dwFileSize];
	memset(fileBuffer, 0x0, dwFileSize);
	bReadSt = ReadFile (hFile, fileBuffer, dwFileSize, &dwRead, NULL);
	CloseHandle (hFile);

	if (!bReadSt || dwRead != dwFileSize) {
		delete[] fileBuffer;
		return FALSE;
	}

	CString strFile(fileBuffer);
	delete[] fileBuffer;
	if (!m_pLedMgr->Load(strFile)) {
		if (m_pView != NULL) {
			m_pView->MessageBox(_T("文件内容损坏！"), _T("出错信息"), MB_OK|MB_ICONHAND);
		}
		return FALSE;
	}

	if (m_pView != NULL) {
		m_pLedMgr->ResetBtns(m_pView);
	}

	// 清除修改标志
    SetModifiedFlag(FALSE);
	return TRUE;
}

BOOL CLedEditorDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
	if (m_pView != NULL) {
		m_pView->OnSimStop();
	}
	CString strFile;
	m_pLedMgr->Save(strFile);

	// 写入磁盘文件
	HANDLE hFile;
	hFile = CreateFile (lpszPathName,GENERIC_READ|GENERIC_WRITE,FILE_SHARE_READ,0,CREATE_ALWAYS,0,NULL);
	if(hFile == INVALID_HANDLE_VALUE) {
		return FALSE;
	}

	DWORD dwWritten = 0;
	BOOL bWriteSt;
	BYTE fileHeader[2] = {0xFF, 0xFE};
	bWriteSt = WriteFile (hFile, fileHeader, 2, &dwWritten, NULL);
	if (!bWriteSt || dwWritten != 2) {
		CloseHandle (hFile);
		return FALSE;
	}

	bWriteSt = WriteFile (hFile, strFile, sizeof(wchar_t) * strFile.GetLength(), &dwWritten, NULL);
	CloseHandle (hFile);
	if (!bWriteSt || dwWritten != (DWORD)(sizeof(wchar_t) * strFile.GetLength())) {
		return FALSE;
	}

	// 清除修改标志
    SetModifiedFlag(FALSE);
	return TRUE;
	//return CDocument::OnSaveDocument(lpszPathName);
}
