#pragma once


#define PROG_STATUS_READY      (1)
#define PROG_STATUS_RIGHT      (2)
#define PROG_STATUS_ERROR      (3)

// CProgramDlg 对话框
class CMSComm;
class CComPortEnum;
class CLedEditorDoc;
class CLedEditorView;
class CProgramDlg : public CDialog
{
	DECLARE_DYNAMIC(CProgramDlg)

private:
	int m_nProgStatus;
	BOOL m_bProgRunning;
	int m_nProgIndex;
	BYTE m_pbDataGroup[CODE_SIZE_TOTAL];
	CMap<int, int, int, int> m_mapPortDict;

	CLedEditorDoc* m_pDoc;
	CLedEditorView* m_pView;
	CComPortEnum* m_pComEnum;
	CComboBox* m_pcbComList;
	CListBox* m_plstInfo;
	CStatic* m_picResult;
	CStatic* m_ptxtResult;

	CMSComm* m_pMsComm;

private:
	void TestProg();
	void CreateByteCode();
	void SendDataToCom();

public:
	CProgramDlg(CWnd* pParent);
	virtual ~CProgramDlg();

// 对话框数据
	enum { IDD = IDD_PROGRAM_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
	DECLARE_EVENTSINK_MAP()

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnPaint();
	afx_msg void OnComm();
	afx_msg void OnBnClickedWriteToFile();
	afx_msg void OnBnClickedAnalysisFile();
};
