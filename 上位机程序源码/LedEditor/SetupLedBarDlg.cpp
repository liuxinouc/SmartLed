// SetupLedBarDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "LedMode.h"
#include "LedModeMgr.h"
#include "LedModeOption.h"
#include "LedEditor.h"
#include "LedEditorDoc.h"
#include "LedEditorView.h"
#include "ModeOptionDlg.h"
#include "SetupLedBarDlg.h"
#include "ImageButton.h"
#include "TextProgressCtrl.h"

// CSetupLedBarDlg 对话框
//column上面显示的文字
_TCHAR *_gszColumnLabel[11] =
{
	_T("工作模式名称"),
	_T("参数说明")
};
//column上文字的显示方式（靠左）
int _gnColumnFmt[11] = 
{
	LVCFMT_LEFT,
	LVCFMT_LEFT
};
//column的宽度
int _gnColumnWidth[11] = 
{
	130,
	380
};

IMPLEMENT_DYNAMIC(CSetupLedBarDlg, CDialog)

CSetupLedBarDlg::CSetupLedBarDlg(CWnd* pParent, CLedBar* pBar)
	: CDialog(CSetupLedBarDlg::IDD, pParent)
{
	m_pView = (CLedEditorView*)pParent;
	m_pDoc = m_pView->GetDocument();
	m_pLedBar = pBar;

	m_nOldCount = m_pLedBar->GetLedCount();
	m_bOldIndependent = m_pLedBar->IsIndependent();
	m_pOldModeMgr = CLedModeMgr::Copy(m_pLedBar->GetModeMgr());

	m_pPrgMemory = new CTextProgressCtrl();
	m_pBtnStart = new CImageButton(IDB_START, IDB_START, IDB_START_DISABLED);
	m_pBtnStop = new CImageButton(IDB_STOP, IDB_STOP, IDB_STOP_DISABLED);
}

CSetupLedBarDlg::~CSetupLedBarDlg()
{
	if (m_pOldModeMgr) {
		delete m_pOldModeMgr;
		m_pOldModeMgr = NULL;
	}
	if (m_pPrgMemory) {
		delete m_pPrgMemory;
		m_pPrgMemory = NULL;
	}
	if (m_pBtnStart) {
		delete m_pBtnStart;
		m_pBtnStart = NULL;
	}
	if (m_pBtnStop) {
		delete m_pBtnStop;
		m_pBtnStop = NULL;
	}
}

void CSetupLedBarDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSetupLedBarDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSetupLedBarDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CSetupLedBarDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_SUB_10, &CSetupLedBarDlg::OnBnClickedSub10)
	ON_BN_CLICKED(IDC_SUB_1, &CSetupLedBarDlg::OnBnClickedSub1)
	ON_BN_CLICKED(IDC_ADD_1, &CSetupLedBarDlg::OnBnClickedAdd1)
	ON_BN_CLICKED(IDC_ADD_10, &CSetupLedBarDlg::OnBnClickedAdd10)
	ON_EN_CHANGE(IDC_LED_DOTS, &CSetupLedBarDlg::OnEnChangeLedDots)
	ON_MESSAGE(MSG_MODI_LED_COUNT, &CSetupLedBarDlg::OnMsgModiLedCount)
	ON_BN_CLICKED(IDC_ADD_MODE, &CSetupLedBarDlg::OnBnClickedAddMode)
	ON_BN_CLICKED(IDC_EDIT_MODE, &CSetupLedBarDlg::OnBnClickedEditMode)
	ON_BN_CLICKED(IDC_MOVE_UP, &CSetupLedBarDlg::OnBnClickedMoveUp)
	ON_BN_CLICKED(IDC_MOVE_DOWN, &CSetupLedBarDlg::OnBnClickedMoveDown)
	ON_BN_CLICKED(IDC_DELETE_MODE, &CSetupLedBarDlg::OnBnClickedDeleteMode)
	ON_BN_CLICKED(IDC_INDEPENDENT, &CSetupLedBarDlg::OnBnClickedIndependent)
	ON_BN_CLICKED(IDC_BTN_START, &CSetupLedBarDlg::OnBnClickedBtnStart)
	ON_BN_CLICKED(IDC_BTN_STOP, &CSetupLedBarDlg::OnBnClickedBtnStop)
	ON_WM_TIMER()
	ON_NOTIFY(NM_DBLCLK, IDC_PARAM_LIST, &CSetupLedBarDlg::OnHdnDblclickParamList)
END_MESSAGE_MAP()


// CSetupLedBarDlg 消息处理程序

void CSetupLedBarDlg::OnBnClickedOk()
{
	// 检查后续的灯带，如果都不是独立工作模式，则都需要重新设置
	CLedMgr* pMgr = m_pDoc->m_pLedMgr;
	int nCurIndex = pMgr->GetCurIndex();
	CLedBar* pPreBar = pMgr->GetCurLedBar();
	while (1) {
		CLedBar* pNextBar = pMgr->GetValidNextBar(nCurIndex);
		if (pNextBar == NULL) {
			break;
		}
		if (pNextBar->IsIndependent()) {
			break;
		}
		pNextBar->CopyMode(pPreBar);
		pPreBar = pNextBar;
		nCurIndex ++;
	}
	m_pDoc->SetModifiedFlag();

	if (m_pView->m_bSimRunning) {
		KillTimer(ID_SIM_TIMER);
		m_pView->m_bSimRunning = FALSE;
		m_pLedBar->SetDefaultLedColor();
	}
	OnOK();
}

void CSetupLedBarDlg::OnBnClickedCancel()
{
	// 取消操作，恢复Led的数量
	m_pLedBar->SetLedCount(m_nOldCount);

	// 恢复独立工作标志
	m_pLedBar->Independent(m_bOldIndependent);

	// 恢复工作模式
	m_pLedBar->SetModeMgr(CLedModeMgr::Copy(m_pOldModeMgr));

	m_pView->Invalidate();

	if (m_pView->m_bSimRunning) {
		KillTimer(ID_SIM_TIMER);
		m_pView->m_bSimRunning = FALSE;
		m_pLedBar->SetDefaultLedColor();
	}
	OnCancel();
}

void CSetupLedBarDlg::RenewDotsAndLength()
{
	int count = m_pLedBar->GetLedCount();
	float len = m_pLedBar->GetBarLength();
	CString s;
	s.Format(_T("%d"), count);
	m_pEdtLedDots->SetWindowText(s);
	s.Format(_T("%.0f CM"), len);
	m_pEdtLedLength->SetWindowText(s);
	m_pView->Invalidate();
}

BOOL CSetupLedBarDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_pBtnSub10 = (CButton*)GetDlgItem(IDC_SUB_10);
	m_pBtnSub1 = (CButton*)GetDlgItem(IDC_SUB_1);
	m_pBtnAdd10 = (CButton*)GetDlgItem(IDC_ADD_10);
	m_pBtnAdd1 = (CButton*)GetDlgItem(IDC_ADD_1);
	m_pEdtLedDots = (CEdit*)GetDlgItem(IDC_LED_DOTS);
	m_pEdtLedLength = (CEdit*)GetDlgItem(IDC_REAL_LENGTH);
	m_pEdtTotalTime = (CEdit*)GetDlgItem(IDC_TOTAL_TIME);
	m_pChkIndependent = (CButton*)GetDlgItem(IDC_INDEPENDENT);
	m_pLstModes = (CListCtrl*)GetDlgItem(IDC_PARAM_LIST);
	m_pBtnAddMode = (CButton*)GetDlgItem(IDC_ADD_MODE);
	m_pBtnEditMode = (CButton*)GetDlgItem(IDC_EDIT_MODE);
	m_pBtnMoveUp = (CButton*)GetDlgItem(IDC_MOVE_UP);
	m_pBtnMoveDown = (CButton*)GetDlgItem(IDC_MOVE_DOWN);
	m_pBtnDeleteMode = (CButton*)GetDlgItem(IDC_DELETE_MODE);
	m_pPrgMemory->Attach(IDC_MEMORY, this);
	m_pBtnStart->Attach(IDC_BTN_START, this);
	m_pBtnStop->Attach(IDC_BTN_STOP, this);

	m_pPrgMemory->SetRange(0, CODE_SIZE_CHANNEL);

	// 显示LED灯数和物理长度
	RenewDotsAndLength();

	// 初始化配置选项
	m_pChkIndependent->SetCheck(m_pLedBar->IsIndependent());
	InitSetupOptions();
	SetIndependent(m_pLedBar->IsIndependent());

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CSetupLedBarDlg::InitSetupOptions()
{
	m_pLstModes->ModifyStyle(0,LVS_REPORT|LVS_SHOWSELALWAYS,0);
	//设定一个用于存取column的结构lvc
	LVCOLUMN lvc;
	//设定存取模式
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	//用InSertColumn函数向窗口中插入柱
	int nCount = (sizeof(_gszColumnLabel) / sizeof(_gszColumnLabel[0]));
	for( int i=0 ; i<nCount; i++ ) {
		lvc.iSubItem = i;
		lvc.pszText = _gszColumnLabel[i];
		lvc.cx = _gnColumnWidth[i];
		lvc.fmt = _gnColumnFmt[i];
        m_pLstModes->InsertColumn(i,&lvc);
	}
	//设定列表一行全部选中的特性和显示网格的属性
	m_pLstModes->SetExtendedStyle(LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
    // 设置字体
	/*
	CFont font;
    font.CreateFont(12,0,0,0,0,FALSE,FALSE,0,
		HANGUL_CHARSET,
        OUT_DEFAULT_PRECIS,
        CLIP_DEFAULT_PRECIS,
        DEFAULT_QUALITY,
        DEFAULT_PITCH|FF_SWISS, _T("宋体"));
    m_pLstModes->SetFont(&font, TRUE);
	*/
	// 从文件中读出配置选项，并显示在列表中
	RenewModeList();
}

void CSetupLedBarDlg::OnBnClickedSub10()
{
	// 减少10个灯
	int count = m_pLedBar->GetLedCount();
	int newCount = count - 10;
	if (newCount < 0) {
		newCount = 0;
	}
	m_pLedBar->SetLedCount(newCount);
	RenewDotsAndLength();
}

void CSetupLedBarDlg::OnBnClickedSub1()
{
	int count = m_pLedBar->GetLedCount();
	if (count >= 2) {
		count -= 2;
	}
	m_pLedBar->SetLedCount(count);
	RenewDotsAndLength();
}

void CSetupLedBarDlg::OnBnClickedAdd1()
{
	int count = m_pLedBar->GetLedCount();
	if (count <= (LED_BAR_SIZE_MAX - 2)) {
		count += 2;
	}
	m_pLedBar->SetLedCount(count);
	RenewDotsAndLength();
}

void CSetupLedBarDlg::OnBnClickedAdd10()
{
	// 增加10个灯
	int count = m_pLedBar->GetLedCount();
	int newCount = count + 10;
	if (newCount > LED_BAR_SIZE_MAX) {
		newCount = LED_BAR_SIZE_MAX;
	}
	m_pLedBar->SetLedCount(newCount);
	RenewDotsAndLength();
}

void CSetupLedBarDlg::OnEnChangeLedDots()
{
	BOOL bOverflow = FALSE;
	if (this->GetDlgItem(IDC_LED_DOTS) != this->GetFocus()) {
		// 焦点不在本控件上，不做处理
		return;
	}
	// 注意，这里不能再修改本控件值，否则会造成递归嵌套
	CString sCount;
	this->GetDlgItem(IDC_LED_DOTS)->GetWindowText(sCount);
	int nCount = _ttoi(sCount);
	if (nCount < 0) {
		bOverflow = TRUE;
		nCount = 0;
	}
	if (nCount > LED_BAR_SIZE_MAX) {
		nCount = LED_BAR_SIZE_MAX;
		bOverflow = TRUE;
	}
	// 不能支持奇数个灯泡，必须对齐成偶数
	if (nCount % 2 == 1) {
		nCount = (nCount / 2) * 2;
		bOverflow = TRUE;
	}
	m_pLedBar->SetLedCount(nCount);
	float len = m_pLedBar->GetBarLength();
	CString s;
	s.Format(_T("%.0f CM"), len);
	m_pEdtLedLength->SetWindowText(s);
	m_pView->Invalidate();
	if (bOverflow) {
		PostMessage(MSG_MODI_LED_COUNT, nCount);
	}
}

LRESULT CSetupLedBarDlg::OnMsgModiLedCount(WPARAM wp, LPARAM lp)
{
	RenewDotsAndLength();
	return 0;
}

void CSetupLedBarDlg::OnBnClickedIndependent()
{
	// 是否独立工作。不独立工作则无需显示模式列表
	BOOL bIndependent = m_pChkIndependent->GetCheck();
	if (!bIndependent) {
		// 首先检查该灯带能否关闭独立工作模式
		CLedMgr* pMgr = m_pDoc->m_pLedMgr;
		int nCurIndex = pMgr->GetCurIndex();
		CLedBar* pPreBar = pMgr->GetValidPreBar(nCurIndex);
		if (pPreBar == NULL) {
			MessageBox(_T("无前序灯带，无法独立工作"), _T("提示"), MB_OK|MB_ICONEXCLAMATION);
			m_pChkIndependent->SetCheck(TRUE);
			return;
		} else {
			// 自动生成本灯带的工作模式列表
			m_pLedBar->CopyMode(pPreBar);
		}
	}
	SetIndependent(bIndependent);
}

void CSetupLedBarDlg::SetIndependent(BOOL bIndependent)
{
	m_pLedBar->Independent(bIndependent);
	if (bIndependent) {
		m_pLstModes->ShowWindow(SW_SHOW);
		m_pBtnAddMode->ShowWindow(SW_SHOW);
		m_pBtnEditMode->ShowWindow(SW_SHOW);
		m_pBtnMoveUp->ShowWindow(SW_SHOW);
		m_pBtnMoveDown->ShowWindow(SW_SHOW);
		m_pBtnDeleteMode->ShowWindow(SW_SHOW);
		m_pBtnStart->EnableWindow(TRUE);
		m_pBtnStop->EnableWindow(TRUE);
	} else {
		m_pLstModes->ShowWindow(SW_HIDE);
		m_pBtnAddMode->ShowWindow(SW_HIDE);
		m_pBtnEditMode->ShowWindow(SW_HIDE);
		m_pBtnMoveUp->ShowWindow(SW_HIDE);
		m_pBtnMoveDown->ShowWindow(SW_HIDE);
		m_pBtnDeleteMode->ShowWindow(SW_HIDE);
		m_pBtnStart->EnableWindow(FALSE);
		m_pBtnStop->EnableWindow(FALSE);
	}
	m_pView->Invalidate();
}

int CSetupLedBarDlg::GetRemainedCodeSize()
{
	int nCodeSize = 0;
	CLedModeMgr* pMgr = m_pLedBar->GetModeMgr();
	int i;
	int nSize = pMgr->GetCount();
	for (i=0; i<nSize; i++) {
		nCodeSize += pMgr->Get(i)->GetCodeSize();
	}
	return (CODE_SIZE_CHANNEL - nCodeSize);
}

void CSetupLedBarDlg::OnBnClickedAddMode()
{
	int nRemainedCodeSize = this->GetRemainedCodeSize();
	if (nRemainedCodeSize < CLedModeOption::GetCodeSizeMin()) {
		//MessageBox(_T("本灯带代码存储空间已满，无法再添加新的工作模式！"), _T("提示"), MB_OK|MB_ICONEXCLAMATION);
		MessageBox(_T("本型号的控制器不能支持更多的工作模式了！"), _T("提示"), MB_OK|MB_ICONEXCLAMATION);
		return;
	}
	CLedMode mode(0);
	CModeOptionDlg dlg(this, m_pView, m_pLedBar, &mode, FALSE);
	if (IDOK == dlg.DoModal()) {
		CLedMode* pNewMode = CLedMode::Copy(&mode);
		CLedModeMgr* pMgr = m_pLedBar->GetModeMgr();
		pMgr->Add(pNewMode);
		RenewModeList();
	}
}

void CSetupLedBarDlg::OnBnClickedEditMode()
{
	int nCurSel = m_pLstModes->GetSelectionMark();
	if (nCurSel < 0) {
		MessageBox(_T("未选择模式！"), _T("提示"), MB_OK|MB_ICONEXCLAMATION);
		return;
	}
	CLedModeMgr* pMgr = m_pLedBar->GetModeMgr();
	CLedMode* pMode = pMgr->Get(nCurSel);
	CModeOptionDlg dlg(this, m_pView, m_pLedBar, pMode, TRUE);
	dlg.DoModal();
	RenewModeList();
}

void CSetupLedBarDlg::OnBnClickedMoveUp()
{
	int nCurSel = m_pLstModes->GetSelectionMark();
	CLedModeMgr* pMgr = m_pLedBar->GetModeMgr();
	if (pMgr->MoveUp(nCurSel)) {
		RenewModeList();
		m_pLstModes->SetSelectionMark(nCurSel - 1);
		m_pLstModes->SetItemState(nCurSel - 1, LVIS_SELECTED, LVIS_SELECTED);
	}
}

void CSetupLedBarDlg::OnBnClickedMoveDown()
{
	int nCurSel = m_pLstModes->GetSelectionMark();
	CLedModeMgr* pMgr = m_pLedBar->GetModeMgr();
	if (pMgr->MoveDown(nCurSel)) {
		RenewModeList();
		m_pLstModes->SetSelectionMark(nCurSel + 1);
		m_pLstModes->SetItemState(nCurSel + 1, LVIS_SELECTED, LVIS_SELECTED);
	}
}

void CSetupLedBarDlg::OnBnClickedDeleteMode()
{
	int nCurSel = m_pLstModes->GetSelectionMark();
	if (nCurSel < 0) {
		MessageBox(_T("未选择模式！"), _T("提示"), MB_OK|MB_ICONEXCLAMATION);
		return;
	}
	CLedModeMgr* pMgr = m_pLedBar->GetModeMgr();
	if (pMgr->RemoveAt(nCurSel)) {
		RenewModeList();
	}
}

void CSetupLedBarDlg::OnBnClickedBtnStart()
{
	CLedModeMgr* pMgr = m_pLedBar->GetModeMgr();
	int nSize = pMgr->GetCount();
	if (nSize == 0) {
		return;
	}

	// 灯带开始模拟显示
	m_pView->m_bSimRunning = TRUE;
	m_pLedBar->m_nSimCurModeIndex = 0;
	SetTimer(ID_SIM_TIMER, SIM_TIMER_PERIOD, NULL);
	pMgr->Get(0)->SimReset(0);

	int i;
	int count = m_pLstModes->GetItemCount();
	for (i=1; i<count; i++) {
		m_pLstModes->SetItemState(i, 0, -1);
	}
	m_pLstModes->SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);

	// 禁止其他控件
	m_pBtnSub10->EnableWindow(FALSE);
	m_pBtnSub1->EnableWindow(FALSE);
	m_pBtnAdd10->EnableWindow(FALSE);
	m_pBtnAdd1->EnableWindow(FALSE);
	m_pEdtLedDots->EnableWindow(FALSE);
	m_pChkIndependent->EnableWindow(FALSE);
	m_pLstModes->EnableWindow(FALSE);
	m_pBtnAddMode->EnableWindow(FALSE);
	m_pBtnEditMode->EnableWindow(FALSE);
	m_pBtnMoveUp->EnableWindow(FALSE);
	m_pBtnMoveDown->EnableWindow(FALSE);
	m_pBtnDeleteMode->EnableWindow(FALSE);
	m_pBtnStart->EnableWindow(FALSE);
}

void CSetupLedBarDlg::OnBnClickedBtnStop()
{
	if (!m_pView->m_bSimRunning) {
		return;
	}
	KillTimer(ID_SIM_TIMER);
	m_pView->m_bSimRunning = FALSE;
	m_pLedBar->SetDefaultLedColor();

	// 所有按钮使能
	m_pBtnSub10->EnableWindow(FALSE);
	m_pBtnSub1->EnableWindow(FALSE);
	m_pBtnAdd10->EnableWindow(FALSE);
	m_pBtnAdd1->EnableWindow(FALSE);
	m_pEdtLedDots->EnableWindow(TRUE);
	m_pChkIndependent->EnableWindow(TRUE);
	m_pLstModes->EnableWindow(TRUE);
	m_pBtnAddMode->EnableWindow(TRUE);
	m_pBtnEditMode->EnableWindow(TRUE);
	m_pBtnMoveUp->EnableWindow(TRUE);
	m_pBtnMoveDown->EnableWindow(TRUE);
	m_pBtnDeleteMode->EnableWindow(TRUE);
	m_pBtnStart->EnableWindow(TRUE);
}

void CSetupLedBarDlg::RenewModeList()
{
	CLedModeMgr* pMgr = m_pLedBar->GetModeMgr();
	m_pLstModes->DeleteAllItems();

	int i;
	int nSize = pMgr->GetCount();
	int nDelayTime = 0;
	int nCodeSize = 0;
	for (i=0; i<nSize; i++) {
		CLedMode* pMode = pMgr->Get(i);

		CString sName = pMode->GetName();
		CString sSummary = pMode->GetSummary();

		// 更新列表显示
		int nCount = m_pLstModes->GetItemCount();
		m_pLstModes->InsertItem(nCount, sName);
		m_pLstModes->SetItemText(nCount, 1, sSummary);

		nCodeSize += pMode->GetCodeSize();
		if (nDelayTime != -1) {
			int nItemDelayTime = pMode->GetDelayTime();
			if (0 == nItemDelayTime) {
				nDelayTime = -1; // -1表示“无限长”
			} else {
				nDelayTime += nItemDelayTime;
			}
		}
	}

	// 更新模式总时间
	if (-1 == nDelayTime) {
		m_pEdtTotalTime->SetWindowText(_T("无限长"));
	} else {
		CString sDelayTime;
		float fDelayTime = (float)nDelayTime;
		fDelayTime /= 10;
		sDelayTime.Format(_T("%.1f秒"), fDelayTime);
		m_pEdtTotalTime->SetWindowText(sDelayTime);
	}

	// 更新代码剩余空间
	CString sCodeSize;
	sCodeSize.Format(_T("%dB; "), nCodeSize);
	m_pPrgMemory->SetWindowText(sCodeSize);
	m_pPrgMemory->SetPos(nCodeSize);
}

void CSetupLedBarDlg::OnTimer(UINT_PTR nIDEvent)
{
	// 设置当前灯带的显示方式
	CLedModeMgr* pMgr = m_pLedBar->GetModeMgr();

	// 设置当前灯带的显示方式
	if (pMgr->Get(m_pLedBar->m_nSimCurModeIndex)->SimRun(m_pLedBar)) {
		m_pLedBar->m_nSimCurModeIndex ++;
		if (m_pLedBar->m_nSimCurModeIndex >= pMgr->GetCount()) {
			OnBnClickedBtnStop();
		} else {
			pMgr->Get(m_pLedBar->m_nSimCurModeIndex)->SimReset(0);
			int i;
			int count = m_pLstModes->GetItemCount();
			for (i=0; i<count; i++) {
				m_pLstModes->SetItemState(i, 0, -1);
			}
			m_pLstModes->SetItemState(m_pLedBar->m_nSimCurModeIndex, LVIS_SELECTED, LVIS_SELECTED);
		}
	}

	// 刷新灯带显示
	CRect rc;
	m_pLedBar->GetRect(rc, m_pView->GetDrawScale());
	m_pView->RenewRect(&rc, 0);

	CDialog::OnTimer(nIDEvent);
}

void CSetupLedBarDlg::OnHdnDblclickParamList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMHEADER phdr = reinterpret_cast<LPNMHEADER>(pNMHDR);

	int nCurSel = m_pLstModes->GetSelectionMark();
	if (nCurSel < 0) {

		return;
	}
	CLedModeMgr* pMgr = m_pLedBar->GetModeMgr();
	CLedMode* pMode = pMgr->Get(nCurSel);
	CModeOptionDlg dlg(this, m_pView, m_pLedBar, pMode, TRUE);
	dlg.DoModal();
	RenewModeList();

	*pResult = 0;
}
