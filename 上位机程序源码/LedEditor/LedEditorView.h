// LedEditorView.h : CLedEditorView 类的接口
//


#pragma once

#define IDC_CHECK   1101
#define IDC_SETUP  1102

class CButton;
class CLedEditorView : public CScrollView
{
private:
	BOOL m_bInitialed;

protected: // 仅从序列化创建
	CLedEditorView();
	DECLARE_DYNCREATE(CLedEditorView)

// 属性
public:
	CLedEditorDoc* GetDocument() const;
	float m_fDrawScale;
	CButton m_btnCheck;
	CButton m_btnSetup;
	BOOL m_bBtnCheckState;
	BOOL m_bSimRunning;

// 操作
public:
	void SetBtnPos(CRect rc, BOOL bEnable);
	float GetDrawScale(){ return m_fDrawScale; }

// 重写
public:
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	void CLedEditorView::RenewRect(CRect* prc, BOOL bErase = TRUE);

protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CLedEditorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()

public:
	virtual void OnInitialUpdate();
	virtual BOOL DestroyWindow();

public:
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedCheck();
	afx_msg void OnBnClickedSetup();
	afx_msg void OnProgram();
	afx_msg void OnScale1();
	afx_msg void OnScale2();
	afx_msg void OnScale3();
	afx_msg void OnSpeed1();
	afx_msg void OnSpeed2();
	afx_msg void OnSpeed3();
	afx_msg void OnSpeed4();
	afx_msg void OnSpeed5();
	afx_msg void OnSpeed6();
	afx_msg void OnSimPlay();
	afx_msg void OnSimStop();
	afx_msg void OnUpdateScale1(CCmdUI *pCmdUI);
	afx_msg void OnUpdateScale2(CCmdUI *pCmdUI);
	afx_msg void OnUpdateScale3(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSpeed1(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSpeed2(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSpeed3(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSpeed4(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSpeed5(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSpeed6(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSimPlay(CCmdUI *pCmdUI);
	afx_msg void OnUpdateSimStop(CCmdUI *pCmdUI);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};

#ifndef _DEBUG  // LedEditorView.cpp 中的调试版本
inline CLedEditorDoc* CLedEditorView::GetDocument() const
   { return reinterpret_cast<CLedEditorDoc*>(m_pDocument); }
#endif

