// stdafx.cpp : 只包括标准包含文件的源文件
// LedEditor.pch 将作为预编译头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"

CString GetFileName(CString& strFullPath)
{
	CString s;
	int nPos = 0;
	int nLeft = 0;
	while(1){
		nLeft = strFullPath.Find('\\', nPos);
		if( nLeft < 0 )
			break;
		nPos = nLeft+1;
	}
	s = strFullPath.Mid(nPos);
	return s;
}
CString GetFilePath(CString& strFullPath)
{
	CString s;
	int nPos = 0;
	int nLeft = 0;
	while(1){
		nLeft = strFullPath.Find('\\', nPos);
		if( nLeft < 0 )
			break;
		nPos = nLeft+1;
	}
	s = strFullPath.Left(nPos);
	return s;
}
CString TrimAll(const CString& rs)
{
	CString s(rs);
	s.TrimLeft(); s.TrimRight();
	return s;
}
int Split(CStringArray& rAr, LPCTSTR pszSrc, LPCTSTR pszDelimiter)
{
	int n = (int)_tcslen(pszDelimiter);
	rAr.RemoveAll();
	if( n<=0 ){
		rAr.Add(TrimAll(CString(pszSrc)));
		return (int)rAr.GetSize();
	}
	CString s(pszSrc);
	while( s.GetLength() ){
		int iPos = s.Find(pszDelimiter);
		if( iPos<0 ){
			rAr.Add(TrimAll(s));
			break;
		}
		rAr.Add(TrimAll(s.Left(iPos)));
		s = s.Mid(iPos + n);
	}
	return (int)rAr.GetSize();
}