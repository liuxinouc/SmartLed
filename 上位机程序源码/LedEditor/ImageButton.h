#if !defined(AFX_IMAGEBUTTON_H__A87BCD7F_AB36_11D3_9FE1_006067718D04__INCLUDED_)
#define AFX_IMAGEBUTTON_H__A87BCD7F_AB36_11D3_9FE1_006067718D04__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImageButton.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CImageButton window

class CImageButton : public CButton 
{
	DECLARE_DYNAMIC(CImageButton)

private:
	UINT up;
	UINT down;
	UINT disabled;
	BOOL m_fill;

public:
	CImageButton(UINT up, UINT down = 0, UINT disabled = 0, BOOL fill = FALSE);
	virtual ~CImageButton();
	BOOL CImageButton::Attach(const UINT nID, CWnd* pParent);

protected:
	virtual void DrawItem(LPDRAWITEMSTRUCT dis);
};
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMAGEBUTTON_H__A87BCD7F_AB36_11D3_9FE1_006067718D04__INCLUDED_)
