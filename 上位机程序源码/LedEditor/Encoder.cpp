#include "StdAfx.h"
#include "Encoder.h"
#include "LedModeOption.h"

CString CEncoder::GetModeSummary(CStringArray& sa, int nIndex, int nType)
{
	CString sSummary = _T("");
	int nParamCount = (int)sa.GetCount();
	if (nIndex >= nParamCount) {
		return sSummary;
	}
	CString sParam = sa.GetAt(nIndex);
	int nParam;
	double dbDelay;
	switch (nType) {
	case INVAL:
		break;
	case COLOR:
		nParam = _ttoi(sParam);
		switch (nParam) {
			case 0:	sSummary = _T("颜色:红");	break;
			case 1:	sSummary = _T("颜色:黄");	break;
			case 2:	sSummary = _T("颜色:绿");	break;
			case 3:	sSummary = _T("颜色:青");	break;
			case 4:	sSummary = _T("颜色:蓝");	break;
			case 5:	sSummary = _T("颜色:紫");	break;
			case 6:	sSummary = _T("颜色:白");	break;
			case 7:	sSummary = _T("此段不亮");	break;
		}
		break;
	case COUNT:
		sSummary.Format(_T("灯数:%s"), sParam);
		break;
	case SPEED:
		sSummary.Format(_T("速度:%s"), sParam);
		break;
	case DELAY:
		nParam = _ttoi(sParam);
		dbDelay = nParam;
		dbDelay /= 10;
		sSummary.Format(_T("时长:%.1f秒"), dbDelay);
		break;
	}
	if (nIndex < (nParamCount-1)) {
		sSummary += _T(", ");
	}
	return sSummary;
}

DWORD CEncoder::iToColor(int index)
{
	DWORD dict[] = {
	CR_RED,
	CR_YELLOR,
	CR_GREEN,
	CR_CYAN,
	CR_BLUE,
	CR_MAGENTA,
	CR_WHITE,
	CR_BLACK
	};
	return dict[index];
}

int CEncoder::SpeedToDelay(int speed)
{
	int dict[] = {
		0,
		30,
		23,
		17,
		13,
		10,
		7,
		5,
		3,
		2,
		1
	};
	return dict[speed];
}

CString CEncoder::Summary(int nTypeIndex, CStringArray& sa)
{
	CLedModeOption* pOptions = CLedModeOption::GetOptions();
	StLedModeOption* pOpt = pOptions->m_aryOptions.GetAt(nTypeIndex);

	CString sSummary = _T("");
	sSummary += GetModeSummary(sa, 0, pOpt->eParam1Type);
	sSummary += GetModeSummary(sa, 1, pOpt->eParam2Type);
	sSummary += GetModeSummary(sa, 2, pOpt->eParam3Type);
	sSummary += GetModeSummary(sa, 3, pOpt->eParam4Type);
	sSummary += GetModeSummary(sa, 4, pOpt->eParam5Type);
	sSummary += GetModeSummary(sa, 5, pOpt->eParam6Type);
	sSummary += GetModeSummary(sa, 6, pOpt->eParam7Type);
	sSummary += GetModeSummary(sa, 7, pOpt->eParam8Type);
	sSummary += GetModeSummary(sa, 8, pOpt->eParam9Type);
	sSummary += GetModeSummary(sa, 9, pOpt->eParam10Type);
	sSummary += GetModeSummary(sa, 10, pOpt->eParam11Type);
	sSummary += GetModeSummary(sa, 11, pOpt->eParam12Type);
	sSummary += GetModeSummary(sa, 12, pOpt->eParam13Type);
	sSummary += GetModeSummary(sa, 13, pOpt->eParam14Type);
	sSummary += GetModeSummary(sa, 14, pOpt->eParam15Type);
	sSummary += GetModeSummary(sa, 15, pOpt->eParam16Type);
	sSummary += GetModeSummary(sa, 16, pOpt->eParam17Type);
	sSummary += GetModeSummary(sa, 17, pOpt->eParam18Type);
	return sSummary;
}

int CEncoder::ByteCode(BYTE* buffer, int i, CStringArray& sa, int nIndex, int nType)
{
	int nParamCount = (int)sa.GetCount();
	if (nIndex >= nParamCount) {
		return 0;
	}
	CString sParam = sa.GetAt(nIndex);
	int nParam;
	switch (nType) {
	case INVAL:
		return 0;
	case COLOR:
		nParam = _ttoi(sParam);
		switch (nParam) {
			case 0:	buffer[i] = 0x84;	break; // 红
			case 1:	buffer[i] = 0x94;	break; // 黄
			case 2:	buffer[i] = 0x90;	break; // 绿
			case 3:	buffer[i] = 0x91;	break; // 青
			case 4:	buffer[i] = 0x81;	break; // 蓝
			case 5:	buffer[i] = 0x85;	break; // 紫
			case 6:	buffer[i] = 0x95;	break; // 白
			case 7:	buffer[i] = 0x80;	break; // 灭
		}
		return 1;
	case COUNT:
	case SPEED:
		nParam = _ttoi(sParam);
		buffer[i] = (BYTE)nParam;
		return 1;
	case DELAY:
		nParam = _ttoi(sParam);
		buffer[i++] = (BYTE)(nParam >> 8);
		buffer[i++] = (BYTE)nParam;
		return 2;
	}
	return 0;
}

int CEncoder::GetByteCode(BYTE* buffer, int index, int offset, int nTypeIndex, CStringArray& sa)
{
	int i = 0;

	CLedModeOption* pOptions = CLedModeOption::GetOptions();
	StLedModeOption* pOpt = pOptions->m_aryOptions.GetAt(nTypeIndex);

    buffer[i++] = (BYTE)nTypeIndex;     // 灯带的工作模式。模式确定了，参数也就确定了。

	switch (nTypeIndex) {               // offset参数不是每种模式都有的
	case 0: // 单色常亮模式
	case 1: // 熄灭等待
	case 6: // 呼吸模式
		break;
	case 2: // 单色流动模式
	case 3: // 双色流动模式
	case 4: // 三色流动模式
	case 5: // 八色流动模式
	case 7: // 单色拖尾流动模式
		buffer[i++] = (BYTE)(offset >> 8);  // 16位的offset要做大小端处理
		buffer[i++] = (BYTE)offset;
		break;
	}

	i += ByteCode(buffer, i, sa, 0, pOpt->eParam1Type);
	i += ByteCode(buffer, i, sa, 1, pOpt->eParam2Type);
	i += ByteCode(buffer, i, sa, 2, pOpt->eParam3Type);
	i += ByteCode(buffer, i, sa, 3, pOpt->eParam4Type);
	i += ByteCode(buffer, i, sa, 4, pOpt->eParam5Type);
	i += ByteCode(buffer, i, sa, 5, pOpt->eParam6Type);
	i += ByteCode(buffer, i, sa, 6, pOpt->eParam7Type);
	i += ByteCode(buffer, i, sa, 7, pOpt->eParam8Type);
	i += ByteCode(buffer, i, sa, 8, pOpt->eParam9Type);
	i += ByteCode(buffer, i, sa, 9, pOpt->eParam10Type);
	i += ByteCode(buffer, i, sa, 10, pOpt->eParam11Type);
	i += ByteCode(buffer, i, sa, 11, pOpt->eParam12Type);
	i += ByteCode(buffer, i, sa, 12, pOpt->eParam13Type);
	i += ByteCode(buffer, i, sa, 13, pOpt->eParam14Type);
	i += ByteCode(buffer, i, sa, 14, pOpt->eParam15Type);
	i += ByteCode(buffer, i, sa, 15, pOpt->eParam16Type);
	i += ByteCode(buffer, i, sa, 16, pOpt->eParam17Type);
	i += ByteCode(buffer, i, sa, 17, pOpt->eParam18Type);
	return i; // 返回buffer数组中有效内容的长度
}

BOOL CEncoder::Analysis(BYTE* pbBuffer, CString sReportFileName)
{
	CFile file(sReportFileName, CFile::modeCreate | CFile::modeWrite);
	BYTE fileHeader[2] = {0xFF, 0xFE};
	file.Write(fileHeader, 2);

	int i;
	for (i=0; i<CONTROL_BAR_COUNT; i++) {
		BYTE* pData = pbBuffer + i * CODE_SIZE_CHANNEL;
		CString sLog;

		// 分析段标号
		if (pData[0] == 0xFF) {
			sLog.Format(_T("通道%2d 未启用\r\n"), i);
			file.Write(sLog.GetBuffer(0), sLog.GetLength() * sizeof(wchar_t));
			continue;
		} else {
			sLog.Format(_T("通道%2d:\r\n"), i);
			file.Write(sLog.GetBuffer(0), sLog.GetLength() * sizeof(wchar_t));
		}

		// 分析ByteCode
		int j = 0;
		BOOL bRun = TRUE;
		while (bRun) {
			BYTE nTypeIndex = pData[j++];
			switch (nTypeIndex) {
				case 0:	AnalysisMode0(pData, j, file);	break; // 单色常亮模式
				case 1:	AnalysisMode1(pData, j, file);	break; // 熄灭等待
				case 2:	AnalysisMode2(pData, j, file);	break; // 单色流动模式
				case 3:	AnalysisMode3(pData, j, file);	break; // 双色流动模式
				case 4:	AnalysisMode4(pData, j, file);	break; // 三色流动模式
				case 5:	AnalysisMode5(pData, j, file);	break; // 八色流动模式
				case 6:	AnalysisMode6(pData, j, file);	break; // 呼吸模式
				case 7:	AnalysisMode7(pData, j, file);	break; // 单色拖尾流动模式
				default:
					// 遇到不存在的TypeIndex，解析结束
					bRun = FALSE;
					break;
			}
			// 本灯带存储区域已满，解析结束
			if ((CODE_SIZE_CHANNEL - j) < 4) {
				bRun = FALSE;
			}
		}
		file.Write(_T("\r\n"), 4);
	}

	file.Close();
	return TRUE;
}

CString CEncoder::ColorToString(BYTE color)
{
	CString sMsg;
	switch (color) {
		case 0x84: sMsg.Format(_T("红"));	break;
		case 0x94: sMsg.Format(_T("黄"));	break;
		case 0x90: sMsg.Format(_T("绿"));	break;
		case 0x91: sMsg.Format(_T("青"));	break;
		case 0x81: sMsg.Format(_T("蓝"));	break;
		case 0x85: sMsg.Format(_T("紫"));	break;
		case 0x95: sMsg.Format(_T("白"));	break;
		case 0x80: sMsg.Format(_T("灭"));	break;
	}
	return sMsg;
}

void CEncoder::AnalysisMode0(BYTE* pData, int& j, CFile& file)
{
	CString sMsg = _T("\t单色常亮模式: ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE color = pData[j++];
	sMsg = ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	WORD delay = ((WORD)pData[j++]) << 8;
	delay += pData[j++];
	sMsg.Format(_T("延时%d单位"), delay);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	file.Write(_T("\r\n"), 4);
}

void CEncoder::AnalysisMode1(BYTE* pData, int& j, CFile& file)
{
	CString sMsg = _T("\t熄灭等待: ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	WORD delay = ((WORD)pData[j++]) << 8;
	delay += pData[j++];
	sMsg.Format(_T("延时%d单位"), delay);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	file.Write(_T("\r\n"), 4);
}

void CEncoder::AnalysisMode2(BYTE* pData, int& j, CFile& file)
{
	CString sMsg = _T("\t单色流动模式: ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	WORD offset = ((WORD)pData[j++]) << 8;
	offset += pData[j++];
	sMsg.Format(_T("偏移%d, "), offset);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE color = pData[j++];
	sMsg = ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE light = pData[j++];
	sMsg.Format(_T("明长%d, "), light);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	WORD dark = pData[j++];
	sMsg.Format(_T("暗长%d, "), dark);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE speed = pData[j++];
	sMsg.Format(_T("速度%d, "), speed);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	WORD delay = ((WORD)pData[j++]) << 8;
	delay += pData[j++];
	sMsg.Format(_T("延时%d单位"), delay);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	file.Write(_T("\r\n"), 4);
}

void CEncoder::AnalysisMode3(BYTE* pData, int& j, CFile& file)
{
	CString sMsg = _T("\t双色流动模式: ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	WORD offset = ((WORD)pData[j++]) << 8;
	offset += pData[j++];
	sMsg.Format(_T("偏移%d, "), offset);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE color = pData[j++];
	sMsg = _T("1") + ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE count = pData[j++];
	sMsg.Format(_T("长度%d, "), count);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	color = pData[j++];
	sMsg = _T("2") + ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	count = pData[j++];
	sMsg.Format(_T("长度%d, "), count);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE speed = pData[j++];
	sMsg.Format(_T("速度%d, "), speed);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	WORD delay = ((WORD)pData[j++]) << 8;
	delay += pData[j++];
	sMsg.Format(_T("延时%d单位"), delay);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	file.Write(_T("\r\n"), 4);
}

void CEncoder::AnalysisMode4(BYTE* pData, int& j, CFile& file)
{
	CString sMsg = _T("\t三色流动模式: ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	WORD offset = ((WORD)pData[j++]) << 8;
	offset += pData[j++];
	sMsg.Format(_T("偏移%d, "), offset);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE color = pData[j++];
	sMsg = _T("1") + ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE count = pData[j++];
	sMsg.Format(_T("长度%d, "), count);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	color = pData[j++];
	sMsg = _T("2") + ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	count = pData[j++];
	sMsg.Format(_T("长度%d, "), count);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	color = pData[j++];
	sMsg = _T("3") + ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	count = pData[j++];
	sMsg.Format(_T("长度%d, "), count);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE speed = pData[j++];
	sMsg.Format(_T("速度%d, "), speed);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	WORD delay = ((WORD)pData[j++]) << 8;
	delay += pData[j++];
	sMsg.Format(_T("延时%d单位"), delay);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	file.Write(_T("\r\n"), 4);
}

void CEncoder::AnalysisMode5(BYTE* pData, int& j, CFile& file)
{
	CString sMsg = _T("\t八色流动模式: ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	WORD offset = ((WORD)pData[j++]) << 8;
	offset += pData[j++];
	sMsg.Format(_T("偏移%d, "), offset);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE color = pData[j++];
	sMsg = _T("1") + ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE count = pData[j++];
	sMsg.Format(_T("长度%d, "), count);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	color = pData[j++];
	sMsg = _T("2") + ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	count = pData[j++];
	sMsg.Format(_T("长度%d, "), count);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	color = pData[j++];
	sMsg = _T("3") + ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	count = pData[j++];
	sMsg.Format(_T("长度%d, "), count);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	color = pData[j++];
	sMsg = _T("4") + ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	count = pData[j++];
	sMsg.Format(_T("长度%d, "), count);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	color = pData[j++];
	sMsg = _T("5") + ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	count = pData[j++];
	sMsg.Format(_T("长度%d, "), count);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	color = pData[j++];
	sMsg = _T("6") + ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	count = pData[j++];
	sMsg.Format(_T("长度%d, "), count);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	color = pData[j++];
	sMsg = _T("7") + ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	count = pData[j++];
	sMsg.Format(_T("长度%d, "), count);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	color = pData[j++];
	sMsg = _T("8") + ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	count = pData[j++];
	sMsg.Format(_T("长度%d, "), count);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE speed = pData[j++];
	sMsg.Format(_T("速度%d, "), speed);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	WORD delay = ((WORD)pData[j++]) << 8;
	delay += pData[j++];
	sMsg.Format(_T("延时%d单位"), delay);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	file.Write(_T("\r\n"), 4);
}

void CEncoder::AnalysisMode6(BYTE* pData, int& j, CFile& file)
{
	CString sMsg = _T("\t呼吸模式: ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE color = pData[j++];
	sMsg = ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE speed = pData[j++];
	sMsg.Format(_T("频率%d, "), speed);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	WORD delay = ((WORD)pData[j++]) << 8;
	delay += pData[j++];
	sMsg.Format(_T("延时%d单位"), delay);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	file.Write(_T("\r\n"), 4);
}

void CEncoder::AnalysisMode7(BYTE* pData, int& j, CFile& file)
{
	CString sMsg = _T("\t单色拖尾流动模式: ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	WORD offset = ((WORD)pData[j++]) << 8;
	offset += pData[j++];
	sMsg.Format(_T("偏移%d, "), offset);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE color = pData[j++];
	sMsg = ColorToString(color) + _T(", ");
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE count = pData[j++];
	sMsg.Format(_T("间距%d, "), count);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	BYTE speed = pData[j++];
	sMsg.Format(_T("速度%d, "), speed);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	WORD delay = ((WORD)pData[j++]) << 8;
	delay += pData[j++];
	sMsg.Format(_T("延时%d单位"), delay);
	file.Write(sMsg.GetBuffer(0), sMsg.GetLength() * sizeof(wchar_t));

	file.Write(_T("\r\n"), 4);
}

