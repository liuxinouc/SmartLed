#pragma once

class CImgMgr
{
public:
	CImgMgr(void);
	virtual ~CImgMgr(void);

private:
	CMap<int, int, CDC*, CDC*> m_mapImgDict;

public:
	void Init(CDC* pdc);
	CDC* GetImgDC(int imgId);

private:
	void AddImg(int imgId, CDC* pdc);
};
