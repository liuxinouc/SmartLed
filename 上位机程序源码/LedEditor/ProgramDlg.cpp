// ProgramDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "CMSComm.h"
#include "LedEditor.h"
#include "ProgramDlg.h"
#include "ComPortEnum.h"
#include "LedEditorDoc.h"
#include "LedEditorView.h"
#include "LedMode.h"
#include "LedModeMgr.h"

// CProgramDlg 对话框

IMPLEMENT_DYNAMIC(CProgramDlg, CDialog)

CProgramDlg::CProgramDlg(CWnd* pParent)
	: CDialog(CProgramDlg::IDD, pParent)
{
	m_nProgStatus = PROG_STATUS_READY;
	m_bProgRunning = FALSE;
	m_pView = (CLedEditorView*)pParent;
	m_pDoc = m_pView->GetDocument();
	m_pComEnum = new CComPortEnum;
	m_pComEnum->EnumComPort2();
	m_pMsComm = new CMSComm();
}

CProgramDlg::~CProgramDlg()
{
	if (m_pComEnum) {
		delete m_pComEnum;
		m_pComEnum = NULL;
	}
	if (m_pMsComm) {
		delete m_pMsComm;
		m_pMsComm = NULL;
	}
}

void CProgramDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CProgramDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CProgramDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CProgramDlg::OnBnClickedCancel)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_WRITE_TO_FILE, &CProgramDlg::OnBnClickedWriteToFile)
	ON_BN_CLICKED(IDC_ANALYSIS_FILE, &CProgramDlg::OnBnClickedAnalysisFile)
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CProgramDlg, CDialog)
	ON_EVENT(CProgramDlg, IDC_MSCOMM, 1, OnComm, VTS_NONE)
END_EVENTSINK_MAP()

// CProgramDlg 消息处理程序

void CProgramDlg::OnBnClickedOk()
{
	// 测试模拟写入
	//TestProg();

	this->GetDlgItem(IDOK)->EnableWindow(TRUE);
	// 与用户选择的串口建立通讯
	int nSel = m_pcbComList->GetCurSel();
	if (nSel == -1) {
		if (m_pcbComList->GetCount() > 0) {
			MessageBox(_T("请首先选择编程端口！"),_T("提示"),MB_OK|MB_ICONEXCLAMATION);
		} else {
			MessageBox(_T("未发现设备，请插入设备后重启本窗口！"),_T("提示"),MB_OK|MB_ICONEXCLAMATION);
		}
		return;
	}

	// 获得串口号
	int nPort;
	if (!m_mapPortDict.Lookup(nSel, nPort)) {
		MessageBox(_T("连接出错，请重新插入设备并重启本窗口！"),_T("提示"),MB_OK|MB_ICONEXCLAMATION);
		return;
	}

	// 打开串口
	if (m_pMsComm->get_PortOpen()) { //如果串口是打开的，则关闭串口
		m_pMsComm->put_PortOpen(FALSE);
	}
	m_pMsComm->put_CommPort(nPort); //选择COM
	m_pMsComm->put_InBufferSize(1024); //接收缓冲区
	m_pMsComm->put_OutBufferSize(1024);//发送缓冲区
	m_pMsComm->put_InputLen(100);//设置当前接收区数据长度为0,表示全部读取
	m_pMsComm->put_InputMode(1);//以二进制方式读写数据
	m_pMsComm->put_RThreshold(4);//接收缓冲区有n个及以上字符时，将引发接收数据的OnComm事件
	m_pMsComm->put_SThreshold(0);//发送不触发OnComm事件
	m_pMsComm->put_Settings(_T("19200,n,8,1"));//波特率9600无检验位，8个数据位，1个停止位
	if (!m_pMsComm->get_PortOpen()) {//如果串口没有打
		m_pMsComm->put_PortOpen(TRUE);//打开串口
	} else {
		m_pMsComm->put_OutBufferSize(0);
		MessageBox(_T("连接出错，请重新插入设备并重启本窗口！"),_T("提示"),MB_OK|MB_ICONEXCLAMATION);
		return;
	}

	// 生成写入信息
	CreateByteCode();
	m_plstInfo->ResetContent();
	m_bProgRunning = TRUE;
	m_nProgIndex = -1;

	// 发出“0xCC”（擦除）指令
	CByteArray bAry;
	bAry.RemoveAll();
	bAry.SetSize(1);
	bAry.SetAt(0, 0xCC);
	m_pMsComm->put_Output(COleVariant(bAry));
}

void CProgramDlg::CreateByteCode()
{
	CLedMgr* pMgr = m_pDoc->m_pLedMgr;
	memset(m_pbDataGroup, 0xFF, CODE_SIZE_TOTAL);

	int i;
	for (i=0; i<CONTROL_BAR_COUNT; i++) {
		int nBufferIndex = CODE_SIZE_CHANNEL * i;

		CLedBar* pBar = pMgr->GetLedBar(i);
		int offset = pMgr->GetOffset(i);
		CLedModeMgr* pModeMgr = pBar->GetModeMgr();

		int j;
		int nSize = pModeMgr->GetCount();
		for (j=0; j<nSize; j++) {
			CLedMode* pMode = pModeMgr->Get(j);
			int nCodeLen = CEncoder::GetByteCode(m_pbDataGroup + nBufferIndex,
				i, offset,
				pMode->m_nTypeIndex, pMode->m_saParams);
			nBufferIndex += nCodeLen;
		}
	}
}

void CProgramDlg::SendDataToCom()
{
	int i;
	int nSize;
	BYTE pbData[SEND_UNIT_SIZE + 2];
	pbData[0] = 0xAA;
	pbData[SEND_UNIT_SIZE + 1] = 0xBB;
	memcpy(pbData + 1, m_pbDataGroup + m_nProgIndex * SEND_UNIT_SIZE, SEND_UNIT_SIZE); 
	nSize = sizeof(pbData);
	CByteArray bAry;
	bAry.RemoveAll();
	bAry.SetSize(nSize);
	for (i=0; i<nSize; i++) {
		bAry.SetAt(i, pbData[i]);
	}
	m_pMsComm->put_Output(COleVariant(bAry));
}

void CProgramDlg::OnBnClickedCancel()
{
	if (m_pMsComm->get_PortOpen()) { //如果串口是打开的，则关闭串口
		m_pMsComm->put_PortOpen(FALSE);
	}
	if (m_bProgRunning) {
		m_bProgRunning = FALSE;
		m_ptxtResult->SetWindowText(_T("程序写入出错！"));
		m_nProgStatus = PROG_STATUS_ERROR;
		this->GetDlgItem(IDOK)->EnableWindow(TRUE);
	}
	OnCancel();
}

// 测试模拟写入（写入试验数据）
void CProgramDlg::TestProg()
{
	// 与用户选择的串口建立通讯
	int nSel = m_pcbComList->GetCurSel();
	if (nSel == -1) {
		if (m_pcbComList->GetCount() > 0) {
			MessageBox(_T("请首先选择编程端口！"),_T("提示"),MB_OK|MB_ICONEXCLAMATION);
		} else {
			MessageBox(_T("未发现设备，请插入设备后重启本窗口！"),_T("提示"),MB_OK|MB_ICONEXCLAMATION);
		}
		return;
	}

	// 获得串口号
	int nPort;
	if (!m_mapPortDict.Lookup(nSel, nPort)) {
		MessageBox(_T("连接出错，请重新插入设备并重启本窗口！"),_T("提示"),MB_OK|MB_ICONEXCLAMATION);
		return;
	}

	// 打开串口
	if (m_pMsComm->get_PortOpen()) { //如果串口是打开的，则关闭串口
		m_pMsComm->put_PortOpen(FALSE);
	}
	m_pMsComm->put_CommPort(nPort); //选择COM
	m_pMsComm->put_InBufferSize(1024); //接收缓冲区
	m_pMsComm->put_OutBufferSize(1024);//发送缓冲区
	m_pMsComm->put_InputLen(100);//设置当前接收区数据长度为0,表示全部读取
	m_pMsComm->put_InputMode(1);//以二进制方式读写数据
	m_pMsComm->put_RThreshold(4);//接收缓冲区有n个及以上字符时，将引发接收数据的OnComm事件
	m_pMsComm->put_SThreshold(0);//发送不触发OnComm事件
	m_pMsComm->put_Settings(_T("9600,n,8,1"));//波特率9600无检验位，8个数据位，1个停止位
	if (!m_pMsComm->get_PortOpen()) {//如果串口没有打
		m_pMsComm->put_PortOpen(TRUE);//打开串口
	} else {
		m_pMsComm->put_OutBufferSize(0);
		MessageBox(_T("连接出错，请重新插入设备并重启本窗口！"),_T("提示"),MB_OK|MB_ICONEXCLAMATION);
		return;
	}

	// 写入串口信息
	BOOL bSuccess = TRUE;

	int i;
	int nSize;
	BYTE pbData[] = { 0xAA, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0xBB };
	nSize = sizeof(pbData);
	CByteArray bAry;
	bAry.RemoveAll();
	bAry.SetSize(nSize);
	for (i=0; i<nSize; i++) {
		bAry.SetAt(i, pbData[i]);
	}
	m_pMsComm->put_Output(COleVariant(bAry));
	this->Invalidate();
}

void CProgramDlg::OnComm()
{
	switch(m_pMsComm->get_CommEvent())
	{
	case 2:
		// 不知何故收到了数据，不处理
		if (!m_bProgRunning) {
			return;
		}
		// 检测收到的数据是不是“_ _ OK _ _”（ _ 表示空格）
		long i;
		COleSafeArray sa = COleSafeArray(m_pMsComm->get_Input());
		long len = sa.GetOneDimSize();
		char* pchBuffer = new char[len + 1];
		for (i=0; i<len; i++) {
			sa.GetElement(&i, pchBuffer + i);
		}
		pchBuffer[len] = 0x0;
		//MessageBox(CString(pchBuffer), _T("测试"), MB_OK);
		if (CString(pchBuffer) == _T(" OK ")) {// 正确，继续发下一组
			// 检查是不是“擦除”命令的返回信息
			if (m_nProgIndex == -1) {
				CString msg = _T("擦除操作成功");
				m_plstInfo->AddString(msg);
			} else if (m_nProgIndex % CODE_CHIP_NUM == (CODE_CHIP_NUM - 1)) {
				CString msg;
				msg.Format(_T("第%d通道数据写入成功"), (m_nProgIndex / CODE_CHIP_NUM + 1));
				m_plstInfo->AddString(msg);
			}
			m_nProgIndex ++;
			if (m_nProgIndex == CODE_PROG_NUM) {
				// 通讯正常结束
				m_ptxtResult->SetWindowText(_T("程序写入成功！"));
				m_nProgStatus = PROG_STATUS_RIGHT;
				this->GetDlgItem(IDOK)->EnableWindow(TRUE);
			} else {
				SendDataToCom();
			}
		} else {// 出错，结束发送
			if (m_nProgIndex == -1) {
				CString msg = _T("擦除操作失败！");
				m_plstInfo->AddString(msg);
			} else if (m_nProgIndex % CODE_CHIP_NUM == (CODE_CHIP_NUM - 1)) {
				CString msg;
				msg.Format(_T("第%d通道数据写入失败 ！！！"), (m_nProgIndex / CODE_CHIP_NUM + 1));
				m_plstInfo->AddString(msg);
			}
			m_ptxtResult->SetWindowText(_T("程序写入出错！"));
			m_nProgStatus = PROG_STATUS_ERROR;
			this->GetDlgItem(IDOK)->EnableWindow(TRUE);
		}
		break;
	}
	this->Invalidate();
}

BOOL CProgramDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_pcbComList = (CComboBox*)this->GetDlgItem(IDC_COM_LIST);
	m_plstInfo = (CListBox*)this->GetDlgItem(IDC_PROG_INFO);
	m_picResult = (CStatic*)this->GetDlgItem(IDC_PROG_ICON);
	m_ptxtResult = (CStatic*)this->GetDlgItem(IDC_PROG_RESULT);

#ifndef DEBUG
	this->GetDlgItem(IDC_WRITE_TO_FILE)->ShowWindow(SW_HIDE);
	this->GetDlgItem(IDC_ANALYSIS_FILE)->ShowWindow(SW_HIDE);
#endif // DEBUG

	// 检查可以使用的串口设备
	int nSize = m_pComEnum->GetComPortNumber();
	int i;
	for (i=0; i<nSize; i++) {
		int iPort = m_pComEnum->GetPortNum(i);
		CString sDesc = m_pComEnum->GetComPortDesc(i);
		m_pcbComList->AddString(sDesc);
		m_mapPortDict.SetAt(i, iPort);
	}

	// 创建串口控件
	m_pMsComm->Create(NULL, WS_VISIBLE, CRect(0,0,0,0), this, IDC_MSCOMM);
	return TRUE;
	// 异常: OCX 属性页应返回 FALSE
}

void CProgramDlg::OnPaint()
{
	// 这句千万不能删！删除之后MessageBox就出不来了！
	CDialog::OnPaint();

	// 本窗口特殊绘制
	CPaintDC dc(m_picResult);
	CRect rc;
	m_picResult->GetClientRect(&rc);

	CDC* dcIcon = NULL;
	switch (m_nProgStatus) {
	case PROG_STATUS_RIGHT:
		dcIcon = m_pDoc->m_pImgMgr->GetImgDC(IDB_RIGHT);
		break;
	case PROG_STATUS_ERROR:
		dcIcon = m_pDoc->m_pImgMgr->GetImgDC(IDB_ERROR);
		break;
	}
	if (dcIcon != NULL) {
		dc.BitBlt(rc.left, rc.top, rc.Width(), rc.Height(), dcIcon, 0, 0, SRCCOPY);
	}
}

void CProgramDlg::OnBnClickedWriteToFile()
{
#ifdef DEBUG
	CLedMgr* pMgr = m_pDoc->m_pLedMgr;

	CFileDialog dlg(FALSE, _T("bin"), _T("Bytecode"));
	if (IDOK == dlg.DoModal()) {
		BYTE pbBuffer[CODE_SIZE_TOTAL];
		memset(pbBuffer, 0xFF, CODE_SIZE_TOTAL);

		int i;
		for (i=0; i<CONTROL_BAR_COUNT; i++) {
			int nBufferIndex = CODE_SIZE_CHANNEL * i;

			CLedBar* pBar = pMgr->GetLedBar(i);
			int offset = pMgr->GetOffset(i);
			CLedModeMgr* pModeMgr = pBar->GetModeMgr();

			int j;
			int nSize = pModeMgr->GetCount();
			for (j=0; j<nSize; j++) {
				CLedMode* pMode = pModeMgr->Get(j);
				int nCodeLen = CEncoder::GetByteCode(pbBuffer + nBufferIndex,
					i, offset,
					pMode->m_nTypeIndex, pMode->m_saParams);
				nBufferIndex += nCodeLen;

				//CString msg;
				//msg.Format(_T("Type:%d; Len:%d"), pMode->m_nTypeIndex, nCodeLen);
				//MessageBox(msg, _T("提示"), MB_OK);
			}
		}

		CFile file(dlg.GetPathName(), CFile::modeCreate | CFile::modeWrite);
		file.Write(pbBuffer, CODE_SIZE_TOTAL);
		file.Close();
	}
#endif // DEBUG
}

void CProgramDlg::OnBnClickedAnalysisFile()
{
#ifdef DEBUG
	CFileDialog dlg(TRUE);
	dlg.m_ofn.lpstrFilter = _T("(*.bin)\0*.bin\0\0");
	if (IDOK == dlg.DoModal()) {
		BYTE pbBuffer[CODE_SIZE_TOTAL];
		CFile file(dlg.GetPathName(), CFile::modeRead);
		if (file.GetLength() != CODE_SIZE_TOTAL) {
			CString sMsg;
			sMsg.Format(_T("文件尺寸不是%d字节！"), CODE_SIZE_TOTAL);
			MessageBox(sMsg, _T("错误"), MB_OK|MB_ICONERROR);
			return;
		}
		file.Read(pbBuffer, CODE_SIZE_TOTAL);
		file.Close();

		CString sReportFileName = dlg.GetPathName() + _T(".report.txt");
		if (CEncoder::Analysis(pbBuffer, sReportFileName)) {
			ShellExecute(NULL, _T("open"), sReportFileName, NULL, NULL, SW_SHOW);
		}
	}
#endif // DEBUG
}
