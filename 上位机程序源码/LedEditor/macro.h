#ifndef __MACRO_H__
#define __MACRO_H__

// 控制器支持的通道数
#define CONTROL_BAR_COUNT         (16)

// 一条灯带灯带的默认灯泡数量
#define LED_BAR_SIZE_DEFAULT      (40)
#define LED_BAR_SIZE_MAX          (200)
#define LED_COLOR_DEFAULT         (0x00CCCCCC)

// 操作区域尺寸
#define CLIENT_W                  (HUB_DRAW_X + HUB_INFO_W + HUB_UNIT_W + LED_DRAW_W * LED_BAR_SIZE_MAX + 60)
#define CLIENT_H                  (HUB_DRAW_Y + HUB_UNIT_H * CONTROL_BAR_COUNT + 60)

// 绘图位置
#define HUB_DRAW_X                (10)
#define HUB_DRAW_Y                (40)
#define HUB_INFO_W                (120)
#define HUB_UNIT_W                (150)
#define HUB_UNIT_H                (40)

#define LED_REAL_W                (25.0)
#define LED_REAL_H                (10.0)
#define LED_DRAW_W                (9)
#define LED_DRAW_H                (HUB_DRAW_H - 2)

#define RULER_LEFT                (HUB_DRAW_X + HUB_INFO_W + HUB_UNIT_W)
#define RULER_TOP                 (HUB_DRAW_Y - RULER_H)
#define RULER_H                   (35)
#define RULER_W                   (LED_DRAW_W * LED_BAR_SIZE_MAX + 5)
#define RULER_UNIT_SCALE_H        (5)
#define RULER_DM_SCALE_H          (10)
#define RULER_HM_SCALE_H          (20)
#define RULER_M_SCALE_H           (30)

// 目前一个通道所支持最大代码尺寸，单位字节
#define CODE_SIZE_CHANNEL         (32)
#define CODE_SIZE_TOTAL           (CODE_SIZE_CHANNEL * CONTROL_BAR_COUNT)
#define SEND_UNIT_SIZE            (16)
#define CODE_CHIP_NUM             (CODE_SIZE_CHANNEL / SEND_UNIT_SIZE)
#define CODE_PROG_NUM             (CODE_CHIP_NUM * CONTROL_BAR_COUNT)

// 模拟灯带显示的定时器ID号
#define ID_SIM_TIMER              (1001)
#define SIM_TIMER_PERIOD          (100)

#endif // __MACRO_H__