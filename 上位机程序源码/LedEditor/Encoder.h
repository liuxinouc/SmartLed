#ifndef __ENCODER_H__
#define __ENCODER_H__

// LED所支持的8种颜色
typedef enum _EnLedColor
{
	CR_BLACK   = 0x00000000,
	CR_RED     = 0x000000FF,
	CR_GREEN   = 0x0000FF00,
	CR_BLUE    = 0x00FF0000,
	CR_YELLOR  = 0x0000FFFF,
	CR_MAGENTA = 0x00FF00FF,
	CR_CYAN    = 0x00FFFF00,
	CR_WHITE   = 0x00FFFFFF
} EnLedColor;

// 全局函数，根据亮度调整LED的颜色值
// gamma取值范围0~255
#define GAMMA(cr, gamma)     (cr & (((BYTE)(gamma)|((WORD)((BYTE)(gamma))<<8))|(((DWORD)(BYTE)(gamma))<<16)))

class CEncoder
{
private:
	static CString GetModeSummary(CStringArray& sa, int nIndex, int nType);
	static int ByteCode(BYTE* buffer, int i, CStringArray& sa, int nIndex, int nType);
	static CString ColorToString(BYTE color);
	static void AnalysisMode0(BYTE* pData, int& j, CFile& file);
	static void AnalysisMode1(BYTE* pData, int& j, CFile& file);
	static void AnalysisMode2(BYTE* pData, int& j, CFile& file);
	static void AnalysisMode3(BYTE* pData, int& j, CFile& file);
	static void AnalysisMode4(BYTE* pData, int& j, CFile& file);
	static void AnalysisMode5(BYTE* pData, int& j, CFile& file);
	static void AnalysisMode6(BYTE* pData, int& j, CFile& file);
	static void AnalysisMode7(BYTE* pData, int& j, CFile& file);

public:
	static DWORD iToColor(int index);
	static int SpeedToDelay(int speed);
	static CString Summary(int nTypeIndex, CStringArray& sa);
	static int GetByteCode(BYTE* buffer, int index, int offset, int nTypeIndex, CStringArray& sa);
	static BOOL Analysis(BYTE* pbBuffer, CString sReportFileName);
};

#endif // __ENCODER_H__
