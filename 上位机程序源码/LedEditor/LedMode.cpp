#include "StdAfx.h"
#include "Encoder.h"
#include "LedMode.h"
#include "LedModeOption.h"

CLedMode::CLedMode(int type)
{
	m_nTypeIndex = type;
}
CLedMode* CLedMode::Copy(CLedMode* pOrig)
{
	CLedMode* pNewMode = new CLedMode(pOrig->m_nTypeIndex);
	int i;
	int nSize = (int)pOrig->m_saParams.GetSize();
	for (i=0; i<nSize; i++) {
		pNewMode->m_saParams.Add(pOrig->m_saParams.GetAt(i));
	}
	return pNewMode;
}
CLedMode::~CLedMode(void)
{
	RemoveAll();
}
void CLedMode::RemoveAll()
{
	m_saParams.RemoveAll();
}
BOOL CLedMode::Save(CString& strOutput)
{
	CString strMode;
	strMode.Format(_T("%d:"), m_nTypeIndex);
	int i;
	int nSize = (int)m_saParams.GetSize();
	for (i=0; i<nSize; i++) {
		CString sParam;
		if (i != (nSize-1)) {
			sParam.Format(_T("%s,"), m_saParams.GetAt(i));
		} else {
			sParam.Format(_T("%s"), m_saParams.GetAt(i));
		}
		strMode += sParam;
	}
	strOutput += strMode;
	return TRUE;
}
BOOL CLedMode::Load(CString strInput)
{
	int i;
	CStringArray saInput;
	int count = Split(saInput, strInput, _T(":"));
	if (count != 2) {
		return FALSE;
	}
	m_nTypeIndex = _ttoi(saInput[0]);

	RemoveAll();
	CStringArray saParams;
	count = Split(saParams, saInput[1], _T(","));
	for (i=0; i<count; i++) {
		m_saParams.Add(saParams[i]);
	}
	return TRUE;
}
CString CLedMode::GetName()
{
	CLedModeOption* pOptions = CLedModeOption::GetOptions();
	return pOptions->m_aryOptions.GetAt(m_nTypeIndex)->sName;
}
CString CLedMode::GetSummary()
{
	return CEncoder::Summary(m_nTypeIndex, m_saParams);
}
int CLedMode::GetCodeSize()
{
	CLedModeOption* pOptions = CLedModeOption::GetOptions();
	return pOptions->m_aryOptions.GetAt(m_nTypeIndex)->nCodeSize;
}

int CLedMode::GetDelayTime()
{
	static int dict[] = {1, 0, 4, 5, 7, 17, 2, 3};
	int index = dict[m_nTypeIndex];
	int nSize = (int)m_saParams.GetSize();
	if (index >= nSize) {
		return 0;
	}
	CString sParam = m_saParams.GetAt(index);
	return _ttoi(sParam);
}

// 与模拟显示有关的函数
void CLedMode::SimReset(int offset)
{
	m_bSimStart = FALSE;
	m_nSimOffset = offset;
	m_nSimDelayCounter = 0;
}

BOOL CLedMode::SimRun(CLedBar* pBar)
{
	switch (m_nTypeIndex) {
		case 0: /* 单色常亮模式     */ SimOneColorMode(pBar);
			break;
		case 1: /* 熄灭等待         */ SimOffMode(pBar);
			break;
		case 2: /* 单色流动模式     */ SimOneColorFlowMode(pBar);
			break;
		case 3: /* 双色流动模式     */ SimTwoColorFlowMode(pBar);
			break;
		case 4: /* 三色流动模式     */ SimTriColorFlowMode(pBar);
			break;
		case 5: /* 八色流动模式     */ SimOctColorFlowMode(pBar);
			break;
		case 6: /* 呼吸模式         */ SimBreathingMode(pBar);
			break;
		case 7: /* 单色拖尾流动模式 */ SimTrailingMode(pBar);
			break;
	}

	// 处理持续时间
	if (GetDelayTime() == 0) {
		m_nSimDelayCounter ++;
		return FALSE;
	}
	if (m_nSimDelayCounter < GetDelayTime()) {
		m_nSimDelayCounter ++;
		return FALSE;
	}
	return TRUE;
}

// 单色常亮模式
void CLedMode::SimOneColorMode(CLedBar* pBar)
{
	int nParam = _ttoi(m_saParams.GetAt(0));
	DWORD color = CEncoder::iToColor(nParam);
	int i;
	int nSize = pBar->GetLedCount();
	for (i=0; i<nSize; i++) {
		pBar->SetLedColor(i, color);
	}
}

// 熄灭等待
void CLedMode::SimOffMode(CLedBar* pBar)
{
	int i;
	int nSize = pBar->GetLedCount();
	for (i=0; i<nSize; i++) {
		pBar->SetLedColor(i, CR_BLACK);
	}
}

// 单色流动模式
void CLedMode::SimOneColorFlowMode(CLedBar* pBar)
{
	int nParam = _ttoi(m_saParams.GetAt(0));
	DWORD color = CEncoder::iToColor(nParam);
	BYTE count = (BYTE)_ttoi(m_saParams.GetAt(1));
	BYTE gap = (BYTE)_ttoi(m_saParams.GetAt(2));
	BYTE speed = (BYTE)_ttoi(m_saParams.GetAt(3));
	int period = count + gap;

	// 调整速度
	int tm = CEncoder::SpeedToDelay(speed);
	if (tm == 0) {
		return;
	}
	if ((m_nSimDelayCounter % tm) == 0) {
		if (m_nSimOffset == 0) {
			m_bSimStart = TRUE;
		}
		if (m_bSimStart) {
			pBar->Shift();
			int nCurrent = (m_nSimOffset + period - 1) % period;
 			if (nCurrent >= gap) {
				pBar->SetLedColor(0, color);
			} else {
				pBar->SetLedColor(0, CR_BLACK);
			}
		}
		m_nSimOffset --;
		if (m_nSimOffset < 0) {
			m_nSimOffset = period - 1;
		}
	}
}

/* 旧的实现方式
{
	int nParam = _ttoi(m_saParams.GetAt(0));
	DWORD color = CEncoder::iToColor(nParam);
	BYTE count = (BYTE)_ttoi(m_saParams.GetAt(1));
	BYTE gap = (BYTE)_ttoi(m_saParams.GetAt(2));
	BYTE speed = (BYTE)_ttoi(m_saParams.GetAt(3));
	int period = count + gap;

	int i;
	int nSize = pBar->GetLedCount();
	for (i=0; i<nSize; i++) {
		if (((i + m_nSimOffset) % period) < count) {
			pBar->SetLedColor(i, color);
		} else {
			pBar->SetLedColor(i, CR_BLACK);
		}
	}

	// 调整速度
	int tm = CEncoder::SpeedToDelay(speed);
	if (tm == 0) {
		return;
	}
	if ((m_nSimDelayCounter % tm) == 0) {
		m_nSimOffset --;
		if (m_nSimOffset < 0) {
			m_nSimOffset = period;
		}
	}
}
*/

// 双色流动模式
void CLedMode::SimTwoColorFlowMode(CLedBar* pBar)
{
	int nParam = _ttoi(m_saParams.GetAt(0));
	DWORD color1 = CEncoder::iToColor(nParam);
	BYTE count1 = (BYTE)_ttoi(m_saParams.GetAt(1));
	nParam = _ttoi(m_saParams.GetAt(2));
	DWORD color2 = CEncoder::iToColor(nParam);
	BYTE count2 = (BYTE)_ttoi(m_saParams.GetAt(3));
	BYTE speed = (BYTE)_ttoi(m_saParams.GetAt(4));
	int period = count1 + count2;

	// 调整速度
	int tm = CEncoder::SpeedToDelay(speed);
	if (tm == 0) {
		return;
	}
	if ((m_nSimDelayCounter % tm) == 0) {
		if (m_nSimOffset == 0) {
			m_bSimStart = TRUE;
		}
		if (m_bSimStart) {
			pBar->Shift();
			int nCurrent = (m_nSimOffset + period - 1) % period;
 			if (nCurrent >= count2) {
				pBar->SetLedColor(0, color1);
			} else {
				pBar->SetLedColor(0, color2);
			}
		}
		m_nSimOffset --;
		if (m_nSimOffset < 0) {
			m_nSimOffset = period-1;
		}
	}
}

/* 旧的实现方式
{
	int nParam = _ttoi(m_saParams.GetAt(0));
	DWORD color1 = CEncoder::iToColor(nParam);
	BYTE count1 = (BYTE)_ttoi(m_saParams.GetAt(1));
	nParam = _ttoi(m_saParams.GetAt(2));
	DWORD color2 = CEncoder::iToColor(nParam);
	BYTE count2 = (BYTE)_ttoi(m_saParams.GetAt(3));
	BYTE speed = (BYTE)_ttoi(m_saParams.GetAt(4));
	int period = count1 + count2;

	int i;
	int nSize = pBar->GetLedCount();
	for (i=0; i<nSize; i++) {
		if (((i + m_nSimOffset) % period) < count1) {
			pBar->SetLedColor(i, color1);
		} else {
			pBar->SetLedColor(i, color2);
		}
	}

	// 调整速度
	int tm = CEncoder::SpeedToDelay(speed);
	if (tm == 0) {
		return;
	}
	if ((m_nSimDelayCounter % tm) == 0) {
		m_nSimOffset --;
		if (m_nSimOffset < 0) {
			m_nSimOffset = period;
		}
	}
}
*/

// 三色流动模式
void CLedMode::SimTriColorFlowMode(CLedBar* pBar)
{
	int nParam = _ttoi(m_saParams.GetAt(0));
	DWORD color1 = CEncoder::iToColor(nParam);
	BYTE count1 = (BYTE)_ttoi(m_saParams.GetAt(1));
	nParam = _ttoi(m_saParams.GetAt(2));
	DWORD color2 = CEncoder::iToColor(nParam);
	BYTE count2 = (BYTE)_ttoi(m_saParams.GetAt(3));
	nParam = _ttoi(m_saParams.GetAt(4));
	DWORD color3 = CEncoder::iToColor(nParam);
	BYTE count3 = (BYTE)_ttoi(m_saParams.GetAt(5));
	BYTE speed = (BYTE)_ttoi(m_saParams.GetAt(6));
	int period = count1 + count2 + count3;

	// 调整速度
	int tm = CEncoder::SpeedToDelay(speed);
	if (tm == 0) {
		return;
	}
	if ((m_nSimDelayCounter % tm) == 0) {
		if (m_nSimOffset == 0) {
			m_bSimStart = TRUE;
		}
		if (m_bSimStart) {
			pBar->Shift();
			int nCurrent = (m_nSimOffset + period - 1) % period;
 			if (nCurrent >= count3 + count2) {
				pBar->SetLedColor(0, color1);
			} else if (nCurrent >= count3) {
				pBar->SetLedColor(0, color2);
			} else {
				pBar->SetLedColor(0, color3);
			}
		}
		m_nSimOffset --;
		if (m_nSimOffset < 0) {
			m_nSimOffset = period-1;
		}
	}
}

/* 旧的实现方式
{
	int nParam = _ttoi(m_saParams.GetAt(0));
	DWORD color1 = CEncoder::iToColor(nParam);
	BYTE count1 = (BYTE)_ttoi(m_saParams.GetAt(1));
	nParam = _ttoi(m_saParams.GetAt(2));
	DWORD color2 = CEncoder::iToColor(nParam);
	BYTE count2 = (BYTE)_ttoi(m_saParams.GetAt(3));
	nParam = _ttoi(m_saParams.GetAt(4));
	DWORD color3 = CEncoder::iToColor(nParam);
	BYTE count3 = (BYTE)_ttoi(m_saParams.GetAt(5));
	BYTE speed = (BYTE)_ttoi(m_saParams.GetAt(6));
	int period = count1 + count2 + count3;

	int i;
	int nSize = pBar->GetLedCount();
	for (i=0; i<nSize; i++) {
		int it = (i + m_nSimOffset) % period;
		if (it < count1) {
			pBar->SetLedColor(i, color1);
		} else if (it < (count1 + count2)) {
			pBar->SetLedColor(i, color2);
		} else {
			pBar->SetLedColor(i, color3);
		}
	}

	// 调整速度
	int tm = CEncoder::SpeedToDelay(speed);
	if (tm == 0) {
		return;
	}
	if ((m_nSimDelayCounter % tm) == 0) {
		m_nSimOffset --;
		if (m_nSimOffset < 0) {
			m_nSimOffset = period;
		}
	}
}
*/

// 八色流动模式
void CLedMode::SimOctColorFlowMode(CLedBar* pBar)
{
	int nParam = _ttoi(m_saParams.GetAt(0));
	DWORD color1 = CEncoder::iToColor(nParam);
	BYTE count1 = (BYTE)_ttoi(m_saParams.GetAt(1));
	nParam = _ttoi(m_saParams.GetAt(2));
	DWORD color2 = CEncoder::iToColor(nParam);
	BYTE count2 = (BYTE)_ttoi(m_saParams.GetAt(3));
	nParam = _ttoi(m_saParams.GetAt(4));
	DWORD color3 = CEncoder::iToColor(nParam);
	BYTE count3 = (BYTE)_ttoi(m_saParams.GetAt(5));
	nParam = _ttoi(m_saParams.GetAt(6));
	DWORD color4 = CEncoder::iToColor(nParam);
	BYTE count4 = (BYTE)_ttoi(m_saParams.GetAt(7));
	nParam = _ttoi(m_saParams.GetAt(8));
	DWORD color5 = CEncoder::iToColor(nParam);
	BYTE count5 = (BYTE)_ttoi(m_saParams.GetAt(9));
	nParam = _ttoi(m_saParams.GetAt(10));
	DWORD color6 = CEncoder::iToColor(nParam);
	BYTE count6 = (BYTE)_ttoi(m_saParams.GetAt(11));
	nParam = _ttoi(m_saParams.GetAt(12));
	DWORD color7 = CEncoder::iToColor(nParam);
	BYTE count7 = (BYTE)_ttoi(m_saParams.GetAt(13));
	nParam = _ttoi(m_saParams.GetAt(14));
	DWORD color8 = CEncoder::iToColor(nParam);
	BYTE count8 = (BYTE)_ttoi(m_saParams.GetAt(15));
	BYTE speed = (BYTE)_ttoi(m_saParams.GetAt(16));
	int period = count1 + count2 + count3 + count4 + count5 + count6 + count7 + count8;

	// 调整速度
	int tm = CEncoder::SpeedToDelay(speed);
	if (tm == 0) {
		return;
	}
	if ((m_nSimDelayCounter % tm) == 0) {
		if (m_nSimOffset == 0) {
			m_bSimStart = TRUE;
		}
		if (m_bSimStart) {
			pBar->Shift();
			int nCurrent = (m_nSimOffset + period - 1) % period;
 			if (nCurrent >= count8 + count7 + count6 + count5 + count4 + count3 + count2) {
				pBar->SetLedColor(0, color1);
			} else if (nCurrent >= count8 + count7 + count6 + count5 + count4 + count3) {
				pBar->SetLedColor(0, color2);
			} else if (nCurrent >= count8 + count7 + count6 + count5 + count4) {
				pBar->SetLedColor(0, color3);
			} else if (nCurrent >= count8 + count7 + count6 + count5) {
				pBar->SetLedColor(0, color4);
			} else if (nCurrent >= count8 + count7 + count6) {
				pBar->SetLedColor(0, color5);
			} else if (nCurrent >= count8 + count7) {
				pBar->SetLedColor(0, color6);
			} else if (nCurrent >= count8) {
				pBar->SetLedColor(0, color7);
			} else {
				pBar->SetLedColor(0, color8);
			}
		}
		m_nSimOffset --;
		if (m_nSimOffset < 0) {
			m_nSimOffset = period-1;
		}
	}
}

/* 旧的实现方式
{
	int nParam = _ttoi(m_saParams.GetAt(0));
	DWORD color1 = CEncoder::iToColor(nParam);
	BYTE count1 = (BYTE)_ttoi(m_saParams.GetAt(1));
	nParam = _ttoi(m_saParams.GetAt(2));
	DWORD color2 = CEncoder::iToColor(nParam);
	BYTE count2 = (BYTE)_ttoi(m_saParams.GetAt(3));
	nParam = _ttoi(m_saParams.GetAt(4));
	DWORD color3 = CEncoder::iToColor(nParam);
	BYTE count3 = (BYTE)_ttoi(m_saParams.GetAt(5));
	nParam = _ttoi(m_saParams.GetAt(6));
	DWORD color4 = CEncoder::iToColor(nParam);
	BYTE count4 = (BYTE)_ttoi(m_saParams.GetAt(7));
	nParam = _ttoi(m_saParams.GetAt(8));
	DWORD color5 = CEncoder::iToColor(nParam);
	BYTE count5 = (BYTE)_ttoi(m_saParams.GetAt(9));
	nParam = _ttoi(m_saParams.GetAt(10));
	DWORD color6 = CEncoder::iToColor(nParam);
	BYTE count6 = (BYTE)_ttoi(m_saParams.GetAt(11));
	nParam = _ttoi(m_saParams.GetAt(12));
	DWORD color7 = CEncoder::iToColor(nParam);
	BYTE count7 = (BYTE)_ttoi(m_saParams.GetAt(13));
	nParam = _ttoi(m_saParams.GetAt(14));
	DWORD color8 = CEncoder::iToColor(nParam);
	BYTE count8 = (BYTE)_ttoi(m_saParams.GetAt(15));
	BYTE speed = (BYTE)_ttoi(m_saParams.GetAt(16));
	int period = count1 + count2 + count3 + count4 + count5 + count6 + count7 + count8;

	int i;
	int nSize = pBar->GetLedCount();
	for (i=0; i<nSize; i++) {
		int it = (i + m_nSimOffset) % period;
		if (it < count1) {
			pBar->SetLedColor(i, color1);
		} else if (it < (count1 + count2)) {
			pBar->SetLedColor(i, color2);
		} else if (it < (count1 + count2 + count3)) {
			pBar->SetLedColor(i, color3);
		} else if (it < (count1 + count2 + count3 + count4)) {
			pBar->SetLedColor(i, color4);
		} else if (it < (count1 + count2 + count3 + count4 + count5)) {
			pBar->SetLedColor(i, color5);
		} else if (it < (count1 + count2 + count3 + count4 + count5 + count6)) {
			pBar->SetLedColor(i, color6);
		} else if (it < (count1 + count2 + count3 + count4 + count5 + count6 + count7)) {
			pBar->SetLedColor(i, color7);
		} else {
			pBar->SetLedColor(i, color8);
		}
	}

	// 调整速度
	int tm = CEncoder::SpeedToDelay(speed);
	if (tm == 0) {
		return;
	}
	if ((m_nSimDelayCounter % tm) == 0) {
		m_nSimOffset --;
		if (m_nSimOffset < 0) {
			m_nSimOffset = period;
		}
	}
}
*/

// 呼吸模式
void CLedMode::SimBreathingMode(CLedBar* pBar)
{
	int nParam = _ttoi(m_saParams.GetAt(0));
	DWORD color = CEncoder::iToColor(nParam);
	BYTE speed = (BYTE)_ttoi(m_saParams.GetAt(1));

	// 调整亮度
	int tm = CEncoder::SpeedToDelay(speed);
	if (tm == 0) {
		return;
	}
	int period = tm * 2;
	int it = m_nSimDelayCounter % period;
	int gamma = 0;
	if (it < tm) {
		gamma = 255 * it / tm;
	} else {
		gamma = 255 * (period - it) / tm;
	}

	int i;
	int nSize = pBar->GetLedCount();
	for (i=0; i<nSize; i++) {
		pBar->SetLedColor(i, GAMMA(color, gamma));
	}
}

// 单色拖尾流动模式
void CLedMode::SimTrailingMode(CLedBar* pBar)
{
	int nParam = _ttoi(m_saParams.GetAt(0));
	DWORD color = CEncoder::iToColor(nParam);
	BYTE period = (BYTE)_ttoi(m_saParams.GetAt(1));
	BYTE speed = (BYTE)_ttoi(m_saParams.GetAt(2));

	// 调整速度
	int tm = CEncoder::SpeedToDelay(speed);
	if (tm == 0) {
		return;
	}
	if ((m_nSimDelayCounter % tm) == 0) {
		if (m_nSimOffset == 0) {
			m_bSimStart = TRUE;
		}
		if (m_bSimStart) {
			pBar->Shift();
			int nCurrent = (m_nSimOffset + period - 1) % period;
 			if (nCurrent == period - 1) {
				pBar->SetLedColor(0, color);
			} else if (nCurrent > period - 8) {
				int gamma = 255 * (nCurrent - period) / 8;
				pBar->SetLedColor(0, GAMMA(color, gamma));
			} else {
				pBar->SetLedColor(0, CR_BLACK);
			}
		}
		m_nSimOffset --;
		if (m_nSimOffset < 0) {
			m_nSimOffset = period - 1;
		}
	}
}

/* 旧的实现方式
{
	int nParam = _ttoi(m_saParams.GetAt(0));
	DWORD color = CEncoder::iToColor(nParam);
	BYTE count = (BYTE)_ttoi(m_saParams.GetAt(1));
	BYTE speed = (BYTE)_ttoi(m_saParams.GetAt(2));

	int i;
	int nSize = pBar->GetLedCount();
	for (i=0; i<nSize; i++) {
		int it = ((i + m_nSimOffset) % count);
		if (it == 0) {
			pBar->SetLedColor(i, color);
		} else {
			if (it > (count - 8)) {
				int gamma = 255 * (it - (count - 8)) / 8;
				pBar->SetLedColor(i, GAMMA(color, gamma));
			} else {
				pBar->SetLedColor(i, CR_BLACK);
			}
		}
	}

	// 调整速度
	int tm = CEncoder::SpeedToDelay(speed);
	if (tm == 0) {
		return;
	}
	if ((m_nSimDelayCounter % tm) == 0) {
		m_nSimOffset --;
		if (m_nSimOffset < 0) {
			m_nSimOffset = count;
		}
	}
}
*/
