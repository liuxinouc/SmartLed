#include "StdAfx.h"
#include "ImgMgr.h"
#include "resource.h"

CImgMgr::CImgMgr(void)
{
}

CImgMgr::~CImgMgr(void)
{
	POSITION pos = m_mapImgDict.GetStartPosition();
	while(pos) {
		CDC* pdc;
		int key;
		m_mapImgDict.GetNextAssoc(pos, key, pdc);
		::ReleaseDC(AfxGetApp()->m_pMainWnd->GetSafeHwnd(), pdc->m_hDC);
		delete pdc;
	}
	m_mapImgDict.RemoveAll();
}

void CImgMgr::Init(CDC* pdc)
{
	AddImg(IDB_PORTUNIT, pdc);
	AddImg(IDB_PORTFOCUS, pdc);
	AddImg(IDB_PORTENABLE, pdc);
	AddImg(IDB_RIGHT, pdc);
	AddImg(IDB_ERROR, pdc);
}

CDC* CImgMgr::GetImgDC(int imgId)
{
	CDC* pMemDC;
	if (m_mapImgDict.Lookup(imgId, pMemDC)) {
		return pMemDC;
	}
	return NULL;
}

void CImgMgr::AddImg(int imgId, CDC* pdc)
{
    CBitmap buff;
	buff.LoadBitmap(imgId);
	CDC* newDC = new CDC;
    newDC->CreateCompatibleDC(pdc);
    newDC->SelectObject(&buff);
	m_mapImgDict.SetAt(imgId, newDC);
}
