#pragma once

class CLedMode
{
public:
	// 模式类型
	int m_nTypeIndex;

	// 参数列表
	CStringArray m_saParams;

public:
	static CLedMode* Copy(CLedMode* pOrig);

public:
	CLedMode(int type);
	~CLedMode(void);

	void NewData();
	BOOL Save(CString& strOutput);
	BOOL Load(CString strInput);

	CString GetName();
	CString GetSummary();
	int GetCodeSize();
	int GetDelayTime();

private:
	// 与模式显示有关的变量
	BOOL m_bSimStart;
	int m_nSimOffset;
	int m_nSimDelayCounter;

public:
	// 与模拟显示有关的函数
	void SimReset(int offset);
	BOOL SimRun(CLedBar* pBar);

private:
	void RemoveAll();
	// 单色常亮模式
	void SimOneColorMode(CLedBar* pBar);
	// 熄灭等待
	void SimOffMode(CLedBar* pBar);
	// 单色流动模式
	void SimOneColorFlowMode(CLedBar* pBar);
	// 双色流动模式
	void SimTwoColorFlowMode(CLedBar* pBar);
	// 三色流动模式
	void SimTriColorFlowMode(CLedBar* pBar);
	// 八色流动模式
	void SimOctColorFlowMode(CLedBar* pBar);
	// 呼吸模式
	void SimBreathingMode(CLedBar* pBar);
	// 单色拖尾流动模式
	void SimTrailingMode(CLedBar* pBar);
};
