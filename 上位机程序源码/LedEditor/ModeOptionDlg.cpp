// ModeOption.cpp : 实现文件
//

#include "stdafx.h"
#include "LedBar.h"
#include "LedMode.h"
#include "LedEditor.h"
#include "LedEditorView.h"
#include "SetupLedBarDlg.h"
#include "ModeOptionDlg.h"
#include "LedModeOption.h"


// CModeOptionDlg 对话框

IMPLEMENT_DYNAMIC(CModeOptionDlg, CDialog)

CModeOptionDlg::CModeOptionDlg(CWnd* pParent, CLedEditorView* pView, CLedBar* pLedBar, CLedMode* pMode, BOOL bEdit)
: CDialog(CModeOptionDlg::IDD, pParent)
, m_btnStart(IDB_START, IDB_START, IDB_START_DISABLED)
, m_btnStop(IDB_STOP, IDB_STOP, IDB_STOP_DISABLED)
{
	m_pSetupDlg = (CSetupLedBarDlg*)pParent;
	m_pView = pView;
	m_pLedBar = pLedBar;
	m_pMode = pMode;
	m_bEditOp = bEdit;
}

CModeOptionDlg::~CModeOptionDlg()
{
}

void CModeOptionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_START2, m_btnStart);
	DDX_Control(pDX, IDC_BTN_STOP2, m_btnStop);
	DDX_Control(pDX, IDC_MODE_SEL, m_cbModeSel);
	DDX_Control(pDX, IDC_MODE_DESC, m_edModeDesc);
	DDX_Control(pDX, IDC_CMB_P1, m_cbP1);
	DDX_Control(pDX, IDC_CMB_P2, m_cbP2);
	DDX_Control(pDX, IDC_CMB_P3, m_cbP3);
	DDX_Control(pDX, IDC_CMB_P4, m_cbP4);
	DDX_Control(pDX, IDC_CMB_P5, m_cbP5);
	DDX_Control(pDX, IDC_CMB_P6, m_cbP6);
	DDX_Control(pDX, IDC_CMB_P7, m_cbP7);
	DDX_Control(pDX, IDC_CMB_P8, m_cbP8);
	DDX_Control(pDX, IDC_CMB_P9, m_cbP9);
	DDX_Control(pDX, IDC_CMB_P10, m_cbP10);
	DDX_Control(pDX, IDC_CMB_P11, m_cbP11);
	DDX_Control(pDX, IDC_CMB_P12, m_cbP12);
	DDX_Control(pDX, IDC_CMB_P13, m_cbP13);
	DDX_Control(pDX, IDC_CMB_P14, m_cbP14);
	DDX_Control(pDX, IDC_CMB_P15, m_cbP15);
	DDX_Control(pDX, IDC_CMB_P16, m_cbP16);
	DDX_Control(pDX, IDC_CMB_P17, m_cbP17);
	DDX_Control(pDX, IDC_CMB_P18, m_cbP18);
	DDX_Control(pDX, IDC_EDT_P1, m_edP1);
	DDX_Control(pDX, IDC_EDT_P2, m_edP2);
	DDX_Control(pDX, IDC_EDT_P3, m_edP3);
	DDX_Control(pDX, IDC_EDT_P4, m_edP4);
	DDX_Control(pDX, IDC_EDT_P5, m_edP5);
	DDX_Control(pDX, IDC_EDT_P6, m_edP6);
	DDX_Control(pDX, IDC_EDT_P7, m_edP7);
	DDX_Control(pDX, IDC_EDT_P8, m_edP8);
	DDX_Control(pDX, IDC_EDT_P9, m_edP9);
	DDX_Control(pDX, IDC_EDT_P10, m_edP10);
	DDX_Control(pDX, IDC_EDT_P11, m_edP11);
	DDX_Control(pDX, IDC_EDT_P12, m_edP12);
	DDX_Control(pDX, IDC_EDT_P13, m_edP13);
	DDX_Control(pDX, IDC_EDT_P14, m_edP14);
	DDX_Control(pDX, IDC_EDT_P15, m_edP15);
	DDX_Control(pDX, IDC_EDT_P16, m_edP16);
	DDX_Control(pDX, IDC_EDT_P17, m_edP17);
	DDX_Control(pDX, IDC_EDT_P18, m_edP18);
	DDX_Control(pDX, IDC_DESC_P1, m_descP1);
	DDX_Control(pDX, IDC_DESC_P2, m_descP2);
	DDX_Control(pDX, IDC_DESC_P3, m_descP3);
	DDX_Control(pDX, IDC_DESC_P4, m_descP4);
	DDX_Control(pDX, IDC_DESC_P5, m_descP5);
	DDX_Control(pDX, IDC_DESC_P6, m_descP6);
	DDX_Control(pDX, IDC_DESC_P7, m_descP7);
	DDX_Control(pDX, IDC_DESC_P8, m_descP8);
	DDX_Control(pDX, IDC_DESC_P9, m_descP9);
	DDX_Control(pDX, IDC_DESC_P10, m_descP10);
	DDX_Control(pDX, IDC_DESC_P11, m_descP11);
	DDX_Control(pDX, IDC_DESC_P12, m_descP12);
	DDX_Control(pDX, IDC_DESC_P13, m_descP13);
	DDX_Control(pDX, IDC_DESC_P14, m_descP14);
	DDX_Control(pDX, IDC_DESC_P15, m_descP15);
	DDX_Control(pDX, IDC_DESC_P16, m_descP16);
	DDX_Control(pDX, IDC_DESC_P17, m_descP17);
	DDX_Control(pDX, IDC_DESC_P18, m_descP18);
}

BEGIN_MESSAGE_MAP(CModeOptionDlg, CDialog)
	ON_BN_CLICKED(IDC_BTN_START2, &CModeOptionDlg::OnBnClickedBtnStart2)
	ON_BN_CLICKED(IDC_BTN_STOP2, &CModeOptionDlg::OnBnClickedBtnStop2)
	ON_BN_CLICKED(IDOK, &CModeOptionDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CModeOptionDlg::OnBnClickedCancel)
	ON_CBN_SELCHANGE(IDC_MODE_SEL, &CModeOptionDlg::OnCbnSelchangeModeSel)
	ON_WM_TIMER()
END_MESSAGE_MAP()

// CModeOptionDlg 消息处理程序

void CModeOptionDlg::OnBnClickedBtnStart2()
{
	// 灯带开始模拟显示
	m_pView->m_bSimRunning = TRUE;
	SetTimer(ID_SIM_TIMER, SIM_TIMER_PERIOD, NULL);
	UpdateParameters(TRUE);
	m_pMode->SimReset(0);

	// 禁止其他按钮
	m_btnStart.EnableWindow(FALSE);
	m_cbModeSel.EnableWindow(FALSE);
	m_cbP1.EnableWindow(FALSE);
	m_cbP2.EnableWindow(FALSE);
	m_cbP3.EnableWindow(FALSE);
	m_cbP4.EnableWindow(FALSE);
	m_cbP5.EnableWindow(FALSE);
	m_cbP6.EnableWindow(FALSE);
	m_cbP7.EnableWindow(FALSE);
	m_cbP8.EnableWindow(FALSE);
	m_cbP9.EnableWindow(FALSE);
	m_cbP10.EnableWindow(FALSE);
	m_cbP11.EnableWindow(FALSE);
	m_cbP12.EnableWindow(FALSE);
	m_cbP13.EnableWindow(FALSE);
	m_cbP14.EnableWindow(FALSE);
	m_cbP15.EnableWindow(FALSE);
	m_cbP16.EnableWindow(FALSE);
	m_cbP17.EnableWindow(FALSE);
	m_cbP18.EnableWindow(FALSE);
	m_edP1.EnableWindow(FALSE);
	m_edP2.EnableWindow(FALSE);
	m_edP3.EnableWindow(FALSE);
	m_edP4.EnableWindow(FALSE);
	m_edP5.EnableWindow(FALSE);
	m_edP6.EnableWindow(FALSE);
	m_edP7.EnableWindow(FALSE);
	m_edP8.EnableWindow(FALSE);
	m_edP9.EnableWindow(FALSE);
	m_edP10.EnableWindow(FALSE);
	m_edP11.EnableWindow(FALSE);
	m_edP12.EnableWindow(FALSE);
	m_edP13.EnableWindow(FALSE);
	m_edP14.EnableWindow(FALSE);
	m_edP15.EnableWindow(FALSE);
	m_edP16.EnableWindow(FALSE);
	m_edP17.EnableWindow(FALSE);
	m_edP18.EnableWindow(FALSE);
}

void CModeOptionDlg::OnBnClickedBtnStop2()
{
	if (!m_pView->m_bSimRunning) {
		return;
	}
	KillTimer(ID_SIM_TIMER);
	m_pView->m_bSimRunning = FALSE;
	m_pLedBar->SetDefaultLedColor();

	// 所有按钮使能
	m_btnStart.EnableWindow(TRUE);
	m_cbModeSel.EnableWindow(TRUE);
	m_cbP1.EnableWindow(TRUE);
	m_cbP2.EnableWindow(TRUE);
	m_cbP3.EnableWindow(TRUE);
	m_cbP4.EnableWindow(TRUE);
	m_cbP5.EnableWindow(TRUE);
	m_cbP6.EnableWindow(TRUE);
	m_cbP7.EnableWindow(TRUE);
	m_cbP8.EnableWindow(TRUE);
	m_cbP9.EnableWindow(TRUE);
	m_cbP10.EnableWindow(TRUE);
	m_cbP11.EnableWindow(TRUE);
	m_cbP12.EnableWindow(TRUE);
	m_cbP13.EnableWindow(TRUE);
	m_cbP14.EnableWindow(TRUE);
	m_cbP15.EnableWindow(TRUE);
	m_cbP16.EnableWindow(TRUE);
	m_cbP17.EnableWindow(TRUE);
	m_cbP18.EnableWindow(TRUE);
	m_edP1.EnableWindow(TRUE);
	m_edP2.EnableWindow(TRUE);
	m_edP3.EnableWindow(TRUE);
	m_edP4.EnableWindow(TRUE);
	m_edP5.EnableWindow(TRUE);
	m_edP6.EnableWindow(TRUE);
	m_edP7.EnableWindow(TRUE);
	m_edP8.EnableWindow(TRUE);
	m_edP9.EnableWindow(TRUE);
	m_edP10.EnableWindow(TRUE);
	m_edP11.EnableWindow(TRUE);
	m_edP12.EnableWindow(TRUE);
	m_edP13.EnableWindow(TRUE);
	m_edP14.EnableWindow(TRUE);
	m_edP15.EnableWindow(TRUE);
	m_edP16.EnableWindow(TRUE);
	m_edP17.EnableWindow(TRUE);
	m_edP18.EnableWindow(TRUE);
}

void CModeOptionDlg::OnBnClickedOk()
{
	UpdateParameters(TRUE);
	if (m_pView->m_bSimRunning) {
		KillTimer(ID_SIM_TIMER);
		m_pView->m_bSimRunning = FALSE;
		m_pLedBar->SetDefaultLedColor();
	}
	OnOK();
}

void CModeOptionDlg::OnBnClickedCancel()
{
	if (m_pView->m_bSimRunning) {
		KillTimer(ID_SIM_TIMER);
		m_pView->m_bSimRunning = FALSE;
		m_pLedBar->SetDefaultLedColor();
	}
	OnCancel();
}

int CModeOptionDlg::ModeTypeToComboIndex(int nTypeIndex)
{
	int nModeIndex;
	int i;
	int nSize = m_cbModeSel.GetCount();
	for (i=0; i<nSize; i++) {
		if (!m_mapIndexDict.Lookup(i, nModeIndex)) {
			return -1;
		}
		if (nModeIndex == m_pMode->m_nTypeIndex) {
			return i;
		}
	}
	return -1;
}

BOOL CModeOptionDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	int i;
	int nCbIndex = 0;
	CLedModeOption* pOptions = CLedModeOption::GetOptions();
	int nRemainedCodeSize = m_pSetupDlg->GetRemainedCodeSize();

	// 如果是编辑模式，那么要把当前编辑条目占用的空间去掉
	if (m_bEditOp) {
		int nCurCodeSize = m_pMode->GetCodeSize();
		nRemainedCodeSize += nCurCodeSize;
	}

	int nSize = (int)pOptions->m_aryOptions.GetSize();
	for (i=0; i<nSize; i++) {
		int nCodeSize = pOptions->m_aryOptions.GetAt(i)->nCodeSize;
		CString sName = pOptions->m_aryOptions.GetAt(i)->sName;
		// 只加入存储空间还能放得下的指令
		if (nCodeSize <= nRemainedCodeSize) {
			m_mapIndexDict.SetAt(nCbIndex, i);
			m_cbModeSel.AddString(sName);
			nCbIndex ++;
		}
	}

	m_cbP1.InitBox();
	m_cbP2.InitBox();
	m_cbP3.InitBox();
	m_cbP4.InitBox();
	m_cbP5.InitBox();
	m_cbP6.InitBox();
	m_cbP7.InitBox();
	m_cbP8.InitBox();
	m_cbP9.InitBox();
	m_cbP10.InitBox();
	m_cbP11.InitBox();
	m_cbP12.InitBox();
	m_cbP13.InitBox();
	m_cbP14.InitBox();
	m_cbP15.InitBox();
	m_cbP16.InitBox();
	m_cbP17.InitBox();
	m_cbP18.InitBox();

	// 如果是编辑模式，那么按照当前模式的参数设置各控件显示数据
	if (m_bEditOp) {
		int nComboIndex = ModeTypeToComboIndex(m_pMode->m_nTypeIndex);
		if (nComboIndex == -1) {
			return FALSE;
		}
		m_cbModeSel.SetCurSel(nComboIndex);
		OnCbnSelchangeModeSel();
		UpdateParameters(FALSE);
	} else {
		m_cbModeSel.SetCurSel(0);
		OnCbnSelchangeModeSel();
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CModeOptionDlg::SetControlStatus(int nType, CColorSelectComboBox& cb, CEdit& ed)
{
	if (nType == INVAL) {
		cb.ShowWindow(SW_HIDE);
		ed.ShowWindow(SW_HIDE);
	} else if (nType == COLOR) {
		cb.ShowWindow(SW_SHOW);
		ed.ShowWindow(SW_HIDE);
		cb.SetCurSel(0);
	} else {
		cb.ShowWindow(SW_HIDE);
		ed.ShowWindow(SW_SHOW);
		switch (nType) {
			case COUNT:
				ed.SetWindowText(_T("6"));
				break;
			case SPEED:
				ed.SetWindowText(_T("8"));
				break;
			case DELAY:
				ed.SetWindowText(_T("0.0"));
				break;
		}
	}
}

void CModeOptionDlg::AddModeParam(CStringArray& saParams, int nType, CColorSelectComboBox& cb, CEdit& ed)
{
	CString sParam;
	int nParam;
	double dbDelay;
	switch (nType) {
	case INVAL:
		break;
	case COLOR:
		nParam = cb.GetCurSel();
		sParam.Format(_T("%d"), nParam);
		saParams.Add(sParam);
		break;
	case COUNT:
		ed.GetWindowText(sParam);
		nParam = _ttoi(sParam);
		if (nParam < 0) {
			nParam = 0;
		}
		if (nParam > 255) {
			nParam = 255;
		}
		sParam.Format(_T("%d"), nParam);
		saParams.Add(sParam);
		break;
	case DELAY:
		ed.GetWindowText(sParam);
		dbDelay = _tstof(sParam);
		nParam = (int)(dbDelay * 10);
		if (nParam < 0) {
			nParam = 0;
		}
		if (nParam > 65535) {
			nParam = 65535;
		}
		sParam.Format(_T("%d"), nParam);
		saParams.Add(sParam);
		break;
	case SPEED:
		ed.GetWindowText(sParam);
		int nParam = _ttoi(sParam);
		if (nParam < 0) {
			nParam = 0;
		}
		if (nParam > 10) {
			nParam = 10;
		}
		sParam.Format(_T("%d"), nParam);
		saParams.Add(sParam);
		break;
	}
}

void CModeOptionDlg::GetModeParam(CStringArray& saParams, int nIndex, int nType, CColorSelectComboBox& cb, CEdit& ed)
{
	int nParamCount = (int)m_pMode->m_saParams.GetCount();
	if (nIndex >= nParamCount) {
		return;
	}
	CString sParam = saParams.GetAt(nIndex);
	int nParam;
	double dbDelay;
	switch (nType) {
	case INVAL:
		break;
	case COLOR:
		nParam = _ttoi(sParam);
		cb.SetCurSel(nParam);
		break;
	case COUNT:
	case SPEED:
		ed.SetWindowText(sParam);
		break;
	case DELAY:
		nParam = _ttoi(sParam);
		dbDelay = nParam;
		dbDelay /= 10;
		sParam.Format(_T("%.1f"), dbDelay);
		ed.SetWindowText(sParam);
		break;
	}
}

void CModeOptionDlg::OnCbnSelchangeModeSel()
{
	int nModeIndex;
	int nCurSel = m_cbModeSel.GetCurSel();
	if (!m_mapIndexDict.Lookup(nCurSel, nModeIndex)) {
		return;
	}
	CLedModeOption* pOptions = CLedModeOption::GetOptions();
	StLedModeOption* pOpt = pOptions->m_aryOptions.GetAt(nModeIndex);

	m_edModeDesc.SetWindowText(pOpt->sDesc);

	SetControlStatus(pOpt->eParam1Type, m_cbP1, m_edP1);
	m_descP1.SetWindowText(pOpt->sParam1Desc);
	SetControlStatus(pOpt->eParam2Type, m_cbP2, m_edP2);
	m_descP2.SetWindowText(pOpt->sParam2Desc);
	SetControlStatus(pOpt->eParam3Type, m_cbP3, m_edP3);
	m_descP3.SetWindowText(pOpt->sParam3Desc);
	SetControlStatus(pOpt->eParam4Type, m_cbP4, m_edP4);
	m_descP4.SetWindowText(pOpt->sParam4Desc);
	SetControlStatus(pOpt->eParam5Type, m_cbP5, m_edP5);
	m_descP5.SetWindowText(pOpt->sParam5Desc);
	SetControlStatus(pOpt->eParam6Type, m_cbP6, m_edP6);
	m_descP6.SetWindowText(pOpt->sParam6Desc);
	SetControlStatus(pOpt->eParam7Type, m_cbP7, m_edP7);
	m_descP7.SetWindowText(pOpt->sParam7Desc);
	SetControlStatus(pOpt->eParam8Type, m_cbP8, m_edP8);
	m_descP8.SetWindowText(pOpt->sParam8Desc);
	SetControlStatus(pOpt->eParam9Type, m_cbP9, m_edP9);
	m_descP9.SetWindowText(pOpt->sParam9Desc);
	SetControlStatus(pOpt->eParam10Type, m_cbP10, m_edP10);
	m_descP10.SetWindowText(pOpt->sParam10Desc);
	SetControlStatus(pOpt->eParam11Type, m_cbP11, m_edP11);
	m_descP11.SetWindowText(pOpt->sParam11Desc);
	SetControlStatus(pOpt->eParam12Type, m_cbP12, m_edP12);
	m_descP12.SetWindowText(pOpt->sParam12Desc);
	SetControlStatus(pOpt->eParam13Type, m_cbP13, m_edP13);
	m_descP13.SetWindowText(pOpt->sParam13Desc);
	SetControlStatus(pOpt->eParam14Type, m_cbP14, m_edP14);
	m_descP14.SetWindowText(pOpt->sParam14Desc);
	SetControlStatus(pOpt->eParam15Type, m_cbP15, m_edP15);
	m_descP15.SetWindowText(pOpt->sParam15Desc);
	SetControlStatus(pOpt->eParam16Type, m_cbP16, m_edP16);
	m_descP16.SetWindowText(pOpt->sParam16Desc);
	SetControlStatus(pOpt->eParam17Type, m_cbP17, m_edP17);
	m_descP17.SetWindowText(pOpt->sParam17Desc);
	SetControlStatus(pOpt->eParam18Type, m_cbP18, m_edP18);
	m_descP18.SetWindowText(pOpt->sParam18Desc);
}

void CModeOptionDlg::UpdateParameters(BOOL bUp)
{
	CLedModeOption* pOptions = CLedModeOption::GetOptions();

	if (bUp) {
		// 将参数值写入Param管理器
		int nModeIndex;
		int nCurSel = m_cbModeSel.GetCurSel();
		if (!m_mapIndexDict.Lookup(nCurSel, nModeIndex)) {
			return;
		}
		StLedModeOption* pOpt = pOptions->m_aryOptions.GetAt(nModeIndex);
		m_pMode->m_nTypeIndex = nModeIndex;
		m_pMode->m_saParams.RemoveAll();
		AddModeParam(m_pMode->m_saParams, pOpt->eParam1Type, m_cbP1, m_edP1);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam2Type, m_cbP2, m_edP2);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam3Type, m_cbP3, m_edP3);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam4Type, m_cbP4, m_edP4);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam5Type, m_cbP5, m_edP5);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam6Type, m_cbP6, m_edP6);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam7Type, m_cbP7, m_edP7);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam8Type, m_cbP8, m_edP8);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam9Type, m_cbP9, m_edP9);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam10Type, m_cbP10, m_edP10);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam11Type, m_cbP11, m_edP11);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam12Type, m_cbP12, m_edP12);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam13Type, m_cbP13, m_edP13);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam14Type, m_cbP14, m_edP14);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam15Type, m_cbP15, m_edP15);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam16Type, m_cbP16, m_edP16);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam17Type, m_cbP17, m_edP17);
		AddModeParam(m_pMode->m_saParams, pOpt->eParam18Type, m_cbP18, m_edP18);
	} else {
		// 从Param管理器读出数值进行显示
		int nComboIndex = ModeTypeToComboIndex(m_pMode->m_nTypeIndex);
		if (nComboIndex == -1) {
			return;
		}
		m_cbModeSel.SetCurSel(nComboIndex);
		StLedModeOption* pOpt = pOptions->m_aryOptions.GetAt(m_pMode->m_nTypeIndex);
		GetModeParam(m_pMode->m_saParams, 0, pOpt->eParam1Type, m_cbP1, m_edP1);
		GetModeParam(m_pMode->m_saParams, 1, pOpt->eParam2Type, m_cbP2, m_edP2);
		GetModeParam(m_pMode->m_saParams, 2, pOpt->eParam3Type, m_cbP3, m_edP3);
		GetModeParam(m_pMode->m_saParams, 3, pOpt->eParam4Type, m_cbP4, m_edP4);
		GetModeParam(m_pMode->m_saParams, 4, pOpt->eParam5Type, m_cbP5, m_edP5);
		GetModeParam(m_pMode->m_saParams, 5, pOpt->eParam6Type, m_cbP6, m_edP6);
		GetModeParam(m_pMode->m_saParams, 6, pOpt->eParam7Type, m_cbP7, m_edP7);
		GetModeParam(m_pMode->m_saParams, 7, pOpt->eParam8Type, m_cbP8, m_edP8);
		GetModeParam(m_pMode->m_saParams, 8, pOpt->eParam9Type, m_cbP9, m_edP9);
		GetModeParam(m_pMode->m_saParams, 9, pOpt->eParam10Type, m_cbP10, m_edP10);
		GetModeParam(m_pMode->m_saParams, 10, pOpt->eParam11Type, m_cbP11, m_edP11);
		GetModeParam(m_pMode->m_saParams, 11, pOpt->eParam12Type, m_cbP12, m_edP12);
		GetModeParam(m_pMode->m_saParams, 12, pOpt->eParam13Type, m_cbP13, m_edP13);
		GetModeParam(m_pMode->m_saParams, 13, pOpt->eParam14Type, m_cbP14, m_edP14);
		GetModeParam(m_pMode->m_saParams, 14, pOpt->eParam15Type, m_cbP15, m_edP15);
		GetModeParam(m_pMode->m_saParams, 15, pOpt->eParam16Type, m_cbP16, m_edP16);
		GetModeParam(m_pMode->m_saParams, 16, pOpt->eParam17Type, m_cbP17, m_edP17);
		GetModeParam(m_pMode->m_saParams, 17, pOpt->eParam18Type, m_cbP18, m_edP18);
	}
}

void CModeOptionDlg::OnTimer(UINT_PTR nIDEvent)
{
	// 设置当前灯带的显示方式
	if (m_pMode->SimRun(m_pLedBar)) {
		OnBnClickedBtnStop2();
	}

	// 刷新灯带显示
	CRect rc;
	m_pLedBar->GetRect(rc, m_pView->GetDrawScale());
	m_pView->RenewRect(&rc, 0);

	CDialog::OnTimer(nIDEvent);
}
