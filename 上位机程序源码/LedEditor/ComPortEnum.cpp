// ComPortEnum.cpp: implementation of the CComPortEnum class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ComPortEnum.h"

#include <setupapi.h>
#pragma comment (lib, "setupapi.lib")

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CComPortEnum::CComPortEnum()
{
	m_nComPortCount = 0;
}

CComPortEnum::~CComPortEnum()
{
}

// 枚举所有的串口，返回总串口数
int CComPortEnum::EnumComPort(void)
{
	int i, listcnt;
	COMPORT_INFO comdesc, *pcomdesc;
	if (m_lstComPort.GetCount() != 0) {
		m_lstComPort.RemoveAll();
	}
	HKEY hKey;
	LPCTSTR data_Set = _T("HARDWARE\\DEVICEMAP\\SERIALCOMM\\");
	long ret0 = RegOpenKeyEx(HKEY_LOCAL_MACHINE, data_Set, 0, KEY_READ, &hKey);
	if (ret0 != ERROR_SUCCESS) {
		AfxMessageBox(_T("错误：无法打开有关的hKEY"));
		m_nComPortCount = 0;
		return 0;
	}
	DWORD dwIndex = 0;
	LONG Status;
	WCHAR Name[25];
	UCHAR szPortName[80];
	DWORD dwName;
	DWORD dwSizeofPortName;
	DWORD Type;
	do {
		dwName = sizeof(Name);
		dwSizeofPortName = sizeof(szPortName);
		Status = RegEnumValue(hKey, dwIndex++, Name, &dwName, NULL, &Type,
								szPortName, &dwSizeofPortName);
		if ((Status == ERROR_SUCCESS)||(Status == ERROR_MORE_DATA)) {
			comdesc.name = (LPCTSTR)szPortName;
			comdesc.num = GetNumber(comdesc.name);
			if (m_lstComPort.GetCount() == 0) {     // 排序插入
				m_lstComPort.AddHead(comdesc);
			} else {
				listcnt = (int)m_lstComPort.GetCount();
				for (i=0; i<listcnt; i++) {
					pcomdesc = &m_lstComPort.GetAt(m_lstComPort.FindIndex(i));
					if(comdesc.num < pcomdesc->num) {
						m_lstComPort.InsertBefore(m_lstComPort.FindIndex(i), comdesc);
						goto _insert_out;
					}
				}
				m_lstComPort.AddTail(comdesc);
			}
_insert_out:
			m_nComPortCount ++;
		}
	} while((Status == ERROR_SUCCESS)||(Status == ERROR_MORE_DATA));
	RegCloseKey(hKey);
	return 0;
}

// 从列表索引号得到串口描述
CString CComPortEnum::GetComPortDesc(int listnum)
{
	CString costDesc;
	COMPORT_INFO *comdesc;
	if (listnum > m_lstComPort.GetCount()) {
	    return costDesc;
	}
	comdesc = &m_lstComPort.GetAt(m_lstComPort.FindIndex(listnum));
	if (comdesc == NULL) {
	    return costDesc;
	}
	return comdesc->desc;
}

// 从列表选择号得到串口名
CString CComPortEnum::GetComPortName(int listnum)
{
	CString costName;
	COMPORT_INFO *comdesc;
	if (listnum > m_lstComPort.GetCount()) {
	    return costName;
	}
	comdesc = &m_lstComPort.GetAt(m_lstComPort.FindIndex(listnum));
	if (comdesc == NULL) {
	    return costName;
	}
	return comdesc->name;
}

// 从列表选择号得到串口设备号
int CComPortEnum::GetPortNum(int listnum)
{
	COMPORT_INFO *comdesc;
	if (listnum > m_lstComPort.GetCount()) {
		return -1;
	}
	comdesc = &m_lstComPort.GetAt(m_lstComPort.FindIndex(listnum));
	if (comdesc == NULL) {
		return -1;
	}
	return comdesc->num;
}

CString CComPortEnum::GetName(CString csstr)
{
	CString sName;
	int nStart = csstr.Find(_T('('));
	int nEnd = csstr.Find(_T(')'), nStart);
	sName = csstr.Right(csstr.GetLength() - nStart -1);
	sName = sName.Left(nEnd - nStart - 1);
	return sName;
}

int CComPortEnum::GetNumber(CString csstr)
{
	int m_Numberprot;
	LPCTSTR str;
	csstr.Delete(0,3);
	str = (LPCTSTR)csstr;
	m_Numberprot = _ttoi(str);
	return m_Numberprot;
}

DEFINE_GUID( GUID_DEVCLASS_PORTS,  0x4D36E978, 0xE325, 0x11CE, 0xBF, 0xC1, 0x08, 0x00, 0x2B, 0xE1, 0x03, 0x18 );
BOOL CComPortEnum::EnumComPort2(void)
{
    HDEVINFO         DeviceInfoSet;
    SP_DEVINFO_DATA  DeviceInfoData;
    DWORD            index;
    BOOL             bReult = FALSE;
	COMPORT_INFO     comdesc;
	COMPORT_INFO*    pcomdesc;

	if (m_lstComPort.GetCount() != 0) {
		m_lstComPort.RemoveAll();
	}

    // Buffer must over MAX_PATH(127 Byte)
    // Get port info set
    DeviceInfoSet = SetupDiGetClassDevs((LPGUID)&GUID_DEVCLASS_PORTS, // All Classes
                                        0,
                                        0,
                                        DIGCF_PRESENT);//DIGCF_ALLCLASSES); // All devices

    if (DeviceInfoSet == INVALID_HANDLE_VALUE) {
        return FALSE;
    }
    DeviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA); 

    // Enum all ports
    for (index=0; SetupDiEnumDeviceInfo(DeviceInfoSet,index,&DeviceInfoData); index++) {
        wchar_t* buffer  = NULL;
        DWORD buffersize = 0;
        // Get friendly name
        while (!SetupDiGetDeviceRegistryProperty(IN  DeviceInfoSet,
                                                 IN  &DeviceInfoData,
                                                 IN  SPDRP_FRIENDLYNAME,
                                                 OUT NULL,
                                                 OUT (PBYTE)buffer,
                                                 IN  buffersize,
                                                 OUT &buffersize))
        {
            if (GetLastError() == ERROR_INVALID_DATA) {
                // May be a Legacy Device with no HardwareID
                break;
            }
			else if (GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
                // We need to change the buffer size
                if (buffer) {
                    LocalFree(buffer);
                }
                buffer = (wchar_t*)LocalAlloc(LPTR, buffersize*sizeof(wchar_t));
            }
            else {
                // Unknown Failure
                goto cleanup_DeviceInfo;
            }
        }

        if (!buffer) {
            // Some device no friendly name, so buffer is null
            // In this situation, we can't compare device ID
            continue;
        }

		comdesc.desc = buffer;
		comdesc.name = GetName(comdesc.desc);
		comdesc.num = GetNumber(comdesc.name);
		if (m_lstComPort.GetCount() == 0) {     // 排序插入
			m_lstComPort.AddHead(comdesc);
		} else {
			int i;
			int listcnt = (int)m_lstComPort.GetCount();
			for (i=0; i<listcnt; i++) {
				pcomdesc = &m_lstComPort.GetAt(m_lstComPort.FindIndex(i));
				if(comdesc.num < pcomdesc->num) {
					m_lstComPort.InsertBefore(m_lstComPort.FindIndex(i), comdesc);
					goto _insert_out;
				}
			}
			m_lstComPort.AddTail(comdesc);
		}

_insert_out:
		m_nComPortCount ++;

        // Compare device ID
		/*
        if(wcsstr(buffer, pName))  //这里的AT_PORT 就是设备管理器里,AT口的设备名称
        {
            // Get Port number
            // DBG_PRINT("Name: %s", buffer, 0);
            DWORD i = (DWORD)wcslen(buffer);
            DWORD dwStart, dwEnd;
            wchar_t* pStart;
            while (buffer[i] != '(') {
                if (buffer[i] == ')') {
                    dwEnd = i;
                }
                i--;
            }

            dwStart = i+1;
            pStart = buffer + dwStart;
            memcpy(pPort, pStart, dwEnd-dwStart);
            pPort[dwEnd-dwStart] = '/0';

            // DBG_PRINT("pPort: %s", pPort, 0);
            bReult = TRUE;
            if (buffer) {
                LocalFree(buffer);
                buffer = NULL;
            }

            goto cleanup_DeviceInfo;
        }
		*/

        if (buffer) {
            LocalFree(buffer);
            buffer = NULL;
        }
    }

cleanup_DeviceInfo:
    SetupDiDestroyDeviceInfoList(DeviceInfoSet);
    return bReult;
}
