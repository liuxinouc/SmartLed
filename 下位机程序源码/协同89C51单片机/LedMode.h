// LedMode.h

#ifndef __LEDMODE_H__
#define __LEDMODE_H__

// 初始化灯带工作模式
void LED_Init();

// 执行灯带工作模式
void LED_Run();

// 执行1毫秒处理任务
void LED_1msProc();

#endif // __LEDMODE_H__

