// ByteCode.h

#ifndef __BYTECODE_H__
#define __BYTECODE_H__

#define     BYTE      unsigned char
#define     WORD      unsigned int
#define     DWORD     unsigned long

#define CODE_UNIT_SIZE            (32)

// 模式1指令参数
struct _ST_PARAM_MODE1 {
	BYTE byColor;
	WORD wDelay;
};

// 模式2指令参数
struct _ST_PARAM_MODE2 {
	WORD wOffset;
	BYTE byColor1;
	BYTE byLen1;
	BYTE byColor2;
	BYTE byLen2;
	BYTE byColor3;
	BYTE byLen3;
	BYTE byColor4;
	BYTE byLen4;
	BYTE byColor5;
	BYTE byLen5;
	BYTE byColor6;
	BYTE byLen6;
	BYTE byColor7;
	BYTE byLen7;
	BYTE byColor8;
	BYTE byLen8;
	BYTE bySpeed;
	WORD wDelay;
};

// 模式3指令参数
struct _ST_PARAM_MODE3 {
	BYTE byColor;
	BYTE byFreq;
	WORD wDelay;
};

// 模式4指令参数
struct _ST_PARAM_MODE4 {
	WORD wOffset;
	BYTE byColor;
	BYTE byGap;
	BYTE bySpeed;
	WORD wDelay;
};

// 指令参数联合体
union _UN_CMD_PARAM {
	struct _ST_PARAM_MODE1 pm1;
	struct _ST_PARAM_MODE2 pm2;
	struct _ST_PARAM_MODE3 pm3;
	struct _ST_PARAM_MODE4 pm4;
};

// 获取字节码
// pData：字节码数据，长度固定为CODE_UNIT_SIZE
// 返回值：指令条数
BYTE GetCodeCount(BYTE* byaData);

// 获得指令类型
BYTE GetByteCode(BYTE* byaData, BYTE index, union _UN_CMD_PARAM* punParam);

// 将速度转化为延时
BYTE SpeedToDelay(BYTE speed);

// 将颜色转变为渐明颜色
BYTE ColorToLight(BYTE color);

// 将颜色转变为渐暗颜色
BYTE ColorToDim(BYTE color);

// 将频率转变为速度（呼吸模式用）
BYTE FreqToSpeed(BYTE freq);

// 将速度转换为流星模式使用的速度参数
WORD GetMode4Speed(BYTE speed);

#endif // __BYTECODE_H__

