/*-----------------------------------------------
 * 主文件
------------------------------------------------*/  

#include <reg52.h>          //头文件的包含
#include <intrins.h>
#include "ByteCode.h"
#include "HL1606.h"
#include "LedMode.h"

char code reserve[3] _at_ 0x3b;  // 为仿真调试保留3个字节

extern xdata BYTE g_byaByteCode1[];
extern xdata BYTE g_byaByteCode2[];
extern xdata BYTE g_byaByteCode3[];
extern xdata BYTE g_byaByteCode4[];

extern xdata union _UN_CMD_PARAM g_unCmdParam1;
extern xdata union _UN_CMD_PARAM g_unCmdParam2;
extern xdata union _UN_CMD_PARAM g_unCmdParam3;
extern xdata union _UN_CMD_PARAM g_unCmdParam4;

static BYTE  m_byCount = 0;
static bit   g_bAddrMode = 1;
static bit   g_bTimerFlag = 0;
static BYTE  g_by100msCounter = 99;

//sbit SDA=P2^1;            //模拟I2C数据传送位
//sbit SCL=P2^0;            //模拟I2C时钟控制位
//BYTE  array[64];//数组
//unsigned char  array1[64];//数组                         

BYTE temp;          //定义临时变量 
//static int count=0;//数组计数
static int begin=0;//当为0时表示对于接收到的数据丢弃，1表示接收的数据放到数组array中

void InitUART(void)
{
	SCON  = 0x90;
	TMOD |= 0x20;               // TMOD: timer 1, mode 2, 8-bit 重装
	SM2   = 1;
	TH1   = 0xFD;               // TH1:  重装值 9600 波特率 晶振 11.0592MHz  
	TR1   = 1;                  // TR1:  timer 1 打开
	EA    = 1;                  // 打开总中断
	// ES = 1;                  // 打开串口中断
}

void Init_Timer0(void)
{
	TMOD |= 0x01;	  // 使用模式1，16位定时器，使用"|"符号可以在使用多个定时器时不受影响
	EA   = 1;         // 总中断打开
	ET0  = 1;         // 定时器中断打开
	TR0  = 1;         // 定时器开关打开
	ET1  = 0;
	TR1  = 0;
}

void UART_SER(void) interrupt 4 //串行中断服务程序
{
	BYTE bySubAddr = P0 & 0x03;
	BYTE it;

	if (TI) {
		TI = 0;
	}

	if (RI) {
		RI = 0;
		it = SBUF;

		if (g_bAddrMode) {
			if (it == bySubAddr) {
				g_bAddrMode = 0;
				SM2 = 0;
			}
			return;
		}

		if (m_byCount < 32) {
			g_byaByteCode1[m_byCount] = it;
		}
		if ((m_byCount >= 32) && (m_byCount < 64)) {
			g_byaByteCode2[m_byCount - 32] = it;
		}
		if ((m_byCount >= 64) && (m_byCount < 96)) {
			g_byaByteCode3[m_byCount - 64] = it;
		}
		if ((m_byCount >= 96) && (m_byCount < 128)) {
			g_byaByteCode4[m_byCount - 96] = it;
		}
		m_byCount ++;
		if (m_byCount == 128) {
			// 全部接收完毕，关串口中断，不再接收串口数据
			ES = 0;
		}
	}
}

/*------------------------------------------------
                 定时器中断子程序
------------------------------------------------*/
void Timer0_isr(void) interrupt 1
{
	TH0 = (65536 - 922) / 256;		  // 重新赋值 12M晶振计算，指令周期1uS，
	TL0 = (65536 - 922) % 256;        // 1mS方波半个周期500uS，即定时500次
	g_bTimerFlag = 1;
}

/*------------------------------------------------
                    主函数
------------------------------------------------*/
void main()
{
	P0 = 0xFF;
	InitUART();
	ES = 1;
	//HL1606_LED1_1(0x81);//每个灯亮的时间为0.2MS，300灯约为0.06S
	//HL1606_LED2_2(0x81); //时间为0.0016S
	//HL1606_LED4_3(200,0x82,100);//速度参数为1时时间为0.05S
	//HL1606_LED4_4(5,0x83,10,10); 

	while (m_byCount < 128) {
		HL1606_LED1_1(0x81); // 蓝
		HL1606_LED2_1(0x85); // 紫
		HL1606_LED3_1(0x90); // 绿
		HL1606_LED4_1(0x94); // 黄
		_nop_();
		; // 等待初始化完成
	}

	HL1606_LED1_1(0x80);
	HL1606_LED2_1(0x80);
	HL1606_LED3_1(0x80);
	HL1606_LED4_1(0x80);
	Init_Timer0();
	LED_Init();
	while (1) {
		if (g_bTimerFlag) {
			g_bTimerFlag = 0;

			g_by100msCounter ++;
			if (g_by100msCounter == 100) {
				LED_Run();
				g_by100msCounter = 0;
			}

			LED_1msProc();
		}
	}

}
