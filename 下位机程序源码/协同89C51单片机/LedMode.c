/*-----------------------------------------------
 * LED灯带工作模式控制函数
------------------------------------------------*/  

#include <reg52.h>          //头文件的包含
#include <intrins.h>
#include "ByteCode.h"
#include "HL1606.h"

xdata BYTE g_byaByteCode1[CODE_UNIT_SIZE] _at_ 0x0100;
xdata BYTE g_byaByteCode2[CODE_UNIT_SIZE] _at_ 0x0120;
xdata BYTE g_byaByteCode3[CODE_UNIT_SIZE] _at_ 0x0140;
xdata BYTE g_byaByteCode4[CODE_UNIT_SIZE] _at_ 0x0160;

xdata union _UN_CMD_PARAM g_unCmdParam1;
xdata union _UN_CMD_PARAM g_unCmdParam2;
xdata union _UN_CMD_PARAM g_unCmdParam3;
xdata union _UN_CMD_PARAM g_unCmdParam4;

static xdata BYTE m_byLED1_ModeCount;
static xdata BYTE m_byLED2_ModeCount;
static xdata BYTE m_byLED3_ModeCount;
static xdata BYTE m_byLED4_ModeCount;

static xdata BYTE m_byLED1_ModeIndex;
static xdata BYTE m_byLED2_ModeIndex;
static xdata BYTE m_byLED3_ModeIndex;
static xdata BYTE m_byLED4_ModeIndex;

static xdata BYTE m_byLED1_ModeType;
static xdata BYTE m_byLED2_ModeType;
static xdata BYTE m_byLED3_ModeType;
static xdata BYTE m_byLED4_ModeType;

static bit  m_bLED1_Start;
static bit  m_bLED2_Start;
static bit  m_bLED3_Start;
static bit  m_bLED4_Start;

static xdata WORD m_wLED1_Offset;
static xdata WORD m_wLED2_Offset;
static xdata WORD m_wLED3_Offset;
static xdata WORD m_wLED4_Offset;

static xdata WORD m_wLED1_DelayCounter;
static xdata WORD m_wLED2_DelayCounter;
static xdata WORD m_wLED3_DelayCounter;
static xdata WORD m_wLED4_DelayCounter;

static bit  m_bLED1_LightOrDim;
static bit  m_bLED2_LightOrDim;
static bit  m_bLED3_LightOrDim;
static bit  m_bLED4_LightOrDim;

#define LED_TURN_LIGHT       1
#define LED_TURN_DIM         0

static xdata BYTE LED1_Mode3_DimDelayCounter = 0;
static xdata BYTE LED2_Mode3_DimDelayCounter = 0;
static xdata BYTE LED3_Mode3_DimDelayCounter = 0;
static xdata BYTE LED4_Mode3_DimDelayCounter = 0;

static void LED1_InitMode()
{
	m_bLED1_Start = 0;
	m_wLED1_DelayCounter = 0;
	m_bLED1_LightOrDim = LED_TURN_LIGHT;
	switch (m_byLED1_ModeType) {
		case 0:
		case 2:
			break;
		case 1:
			m_wLED1_Offset = g_unCmdParam1.pm2.wOffset;
			break;
		case 3:
			m_wLED1_Offset = g_unCmdParam1.pm4.wOffset;
			break;
		default:
			break;
	}
}

static void LED2_InitMode()
{
	m_bLED2_Start = 0;
	m_wLED2_DelayCounter = 0;
	m_bLED2_LightOrDim = LED_TURN_LIGHT;
	switch (m_byLED2_ModeType) {
		case 0:
		case 2:
			break;
		case 1:
			m_wLED2_Offset = g_unCmdParam2.pm2.wOffset;
			break;
		case 3:
			m_wLED2_Offset = g_unCmdParam2.pm4.wOffset;
			break;
		default:
			break;
	}
}

static void LED3_InitMode()
{
	m_bLED3_Start = 0;
	m_wLED3_DelayCounter = 0;
	m_bLED3_LightOrDim = LED_TURN_LIGHT;
	switch (m_byLED3_ModeType) {
		case 0:
		case 2:
			break;
		case 1:
			m_wLED3_Offset = g_unCmdParam3.pm2.wOffset;
			break;
		case 3:
			m_wLED3_Offset = g_unCmdParam3.pm4.wOffset;
			break;
		default:
			break;
	}
}

static void LED4_InitMode()
{
	m_bLED4_Start = 0;
	m_wLED4_DelayCounter = 0;
	m_bLED4_LightOrDim = LED_TURN_LIGHT;
	switch (m_byLED4_ModeType) {
		case 0:
		case 2:
			break;
		case 1:
			m_wLED4_Offset = g_unCmdParam4.pm2.wOffset;
			break;
		case 3:
			m_wLED4_Offset = g_unCmdParam4.pm4.wOffset;
			break;
		default:
			break;
	}
}

static void LED1_Mode1()
{
	if (!m_bLED1_Start) {
		HL1606_LED1_1(g_unCmdParam1.pm1.byColor);
		m_bLED1_Start = 1;
	}
}

static void LED1_Mode2()
{
	BYTE tm;
	WORD period = g_unCmdParam1.pm2.byLen1
				+ g_unCmdParam1.pm2.byLen2
				+ g_unCmdParam1.pm2.byLen3
				+ g_unCmdParam1.pm2.byLen4
				+ g_unCmdParam1.pm2.byLen5
				+ g_unCmdParam1.pm2.byLen6
				+ g_unCmdParam1.pm2.byLen7
				+ g_unCmdParam1.pm2.byLen8;
	if (period < 1) {
		return;
	}

	// 调整速度
	tm = SpeedToDelay(g_unCmdParam1.pm2.bySpeed);
	if (tm == 0) {
		return;
	}
	if ((m_wLED1_DelayCounter % tm) == 0) {
		if (m_wLED1_Offset == 0) {
			m_bLED1_Start = 1;
		}
		if (m_bLED1_Start) {
			WORD wCurrent = (m_wLED1_Offset + period - 1) % period;
 			if (wCurrent >= g_unCmdParam1.pm2.byLen8
						  + g_unCmdParam1.pm2.byLen7
						  + g_unCmdParam1.pm2.byLen6
						  + g_unCmdParam1.pm2.byLen5
						  + g_unCmdParam1.pm2.byLen4
						  + g_unCmdParam1.pm2.byLen3
						  + g_unCmdParam1.pm2.byLen2) {
				HL1606_LED1_2(g_unCmdParam1.pm2.byColor1);
			} else if (wCurrent >= g_unCmdParam1.pm2.byLen8
								 + g_unCmdParam1.pm2.byLen7
								 + g_unCmdParam1.pm2.byLen6
								 + g_unCmdParam1.pm2.byLen5
								 + g_unCmdParam1.pm2.byLen4
								 + g_unCmdParam1.pm2.byLen3) {
				HL1606_LED1_2(g_unCmdParam1.pm2.byColor2);
			} else if (wCurrent >= g_unCmdParam1.pm2.byLen8
								 + g_unCmdParam1.pm2.byLen7
								 + g_unCmdParam1.pm2.byLen6
								 + g_unCmdParam1.pm2.byLen5
								 + g_unCmdParam1.pm2.byLen4) {
				HL1606_LED1_2(g_unCmdParam1.pm2.byColor3);
			} else if (wCurrent >= g_unCmdParam1.pm2.byLen8
								 + g_unCmdParam1.pm2.byLen7
								 + g_unCmdParam1.pm2.byLen6
								 + g_unCmdParam1.pm2.byLen5) {
				HL1606_LED1_2(g_unCmdParam1.pm2.byColor4);
			} else if (wCurrent >= g_unCmdParam1.pm2.byLen8
								 + g_unCmdParam1.pm2.byLen7
								 + g_unCmdParam1.pm2.byLen6) {
				HL1606_LED1_2(g_unCmdParam1.pm2.byColor5);
			} else if (wCurrent >= g_unCmdParam1.pm2.byLen8
								 + g_unCmdParam1.pm2.byLen7) {
				HL1606_LED1_2(g_unCmdParam1.pm2.byColor6);
			} else if (wCurrent >= g_unCmdParam1.pm2.byLen8) {
				HL1606_LED1_2(g_unCmdParam1.pm2.byColor7);
			} else {
				HL1606_LED1_2(g_unCmdParam1.pm2.byColor8);
			}
		}
		m_wLED1_Offset --;
		if (m_wLED1_Offset == 0xFFFF) {
			m_wLED1_Offset = period - 1;
		}
	}
}

static void LED1_Mode3()
{
	BYTE color = g_unCmdParam1.pm3.byColor;
	BYTE freq = g_unCmdParam1.pm3.byFreq;

	if (HL1606_LED1_3_IsEnd()) {
		if (LED1_Mode3_DimDelayCounter > 0) {
			LED1_Mode3_DimDelayCounter --;
		} else {
			if (m_bLED1_LightOrDim == LED_TURN_LIGHT) {
				HL1606_LED1_3_1(0xFF, ColorToLight(color), FreqToSpeed(freq));
				m_bLED1_LightOrDim = LED_TURN_DIM;
			} else {
				HL1606_LED1_3_1(0xFF, ColorToDim(color), FreqToSpeed(freq));
				m_bLED1_LightOrDim = LED_TURN_LIGHT;
				LED1_Mode3_DimDelayCounter = 2;
			}
		}
	}
}

static void LED1_Mode4()
{
	BYTE tm;
	BYTE color = g_unCmdParam1.pm4.byColor;
	BYTE gap = g_unCmdParam1.pm4.byGap;
	BYTE speed = g_unCmdParam1.pm4.bySpeed;

	// 调整速度
	tm = SpeedToDelay(g_unCmdParam1.pm2.bySpeed);
	if (tm == 0) {
		return;
	}
	if ((m_wLED1_DelayCounter % tm) == 0) {
		// 处理偏移量
		if (m_wLED1_Offset == 0) {
			m_bLED1_Start = 1;
		}
		if (m_bLED1_Start) {
			// 启动一颗流星
			if (HL1606_LED1_4_IsEnd()) {
				HL1606_LED1_4_1(gap, ColorToDim(color),
				GetMode4Speed(speed), GetMode4Speed(speed));
			}
		}
		// 处理偏移量
		m_wLED1_Offset --;
		if (m_wLED1_Offset == 0xFFFF) {
			m_wLED1_Offset = gap - 1;
		}
	}
}

static void LED2_Mode1()
{
	if (!m_bLED2_Start) {
		HL1606_LED2_1(0x81);
		m_bLED2_Start = 1;
	}
}

static void LED2_Mode2()
{
	BYTE tm;
	WORD period = g_unCmdParam2.pm2.byLen1
				+ g_unCmdParam2.pm2.byLen2
				+ g_unCmdParam2.pm2.byLen3
				+ g_unCmdParam2.pm2.byLen4
				+ g_unCmdParam2.pm2.byLen5
				+ g_unCmdParam2.pm2.byLen6
				+ g_unCmdParam2.pm2.byLen7
				+ g_unCmdParam2.pm2.byLen8;
	if (period < 1) {
		return;
	}

	// 调整速度
	tm = SpeedToDelay(g_unCmdParam2.pm2.bySpeed);
	if (tm == 0) {
		return;
	}
	if ((m_wLED2_DelayCounter % tm) == 0) {
		if (m_wLED2_Offset == 0) {
			m_bLED2_Start = 1;
		}
		if (m_bLED2_Start) {
			WORD wCurrent = (m_wLED2_Offset + period - 1) % period;
 			if (wCurrent >= g_unCmdParam2.pm2.byLen8
						  + g_unCmdParam2.pm2.byLen7
						  + g_unCmdParam2.pm2.byLen6
						  + g_unCmdParam2.pm2.byLen5
						  + g_unCmdParam2.pm2.byLen4
						  + g_unCmdParam2.pm2.byLen3
						  + g_unCmdParam2.pm2.byLen2) {
				HL1606_LED2_2(g_unCmdParam2.pm2.byColor1);
			} else if (wCurrent >= g_unCmdParam2.pm2.byLen8
								 + g_unCmdParam2.pm2.byLen7
								 + g_unCmdParam2.pm2.byLen6
								 + g_unCmdParam2.pm2.byLen5
								 + g_unCmdParam2.pm2.byLen4
								 + g_unCmdParam2.pm2.byLen3) {
				HL1606_LED2_2(g_unCmdParam2.pm2.byColor2);
			} else if (wCurrent >= g_unCmdParam2.pm2.byLen8
								 + g_unCmdParam2.pm2.byLen7
								 + g_unCmdParam2.pm2.byLen6
								 + g_unCmdParam2.pm2.byLen5
								 + g_unCmdParam2.pm2.byLen4) {
				HL1606_LED2_2(g_unCmdParam2.pm2.byColor3);
			} else if (wCurrent >= g_unCmdParam2.pm2.byLen8
								 + g_unCmdParam2.pm2.byLen7
								 + g_unCmdParam2.pm2.byLen6
								 + g_unCmdParam2.pm2.byLen5) {
				HL1606_LED2_2(g_unCmdParam2.pm2.byColor4);
			} else if (wCurrent >= g_unCmdParam2.pm2.byLen8
								 + g_unCmdParam2.pm2.byLen7
								 + g_unCmdParam2.pm2.byLen6) {
				HL1606_LED2_2(g_unCmdParam2.pm2.byColor5);
			} else if (wCurrent >= g_unCmdParam2.pm2.byLen8
								 + g_unCmdParam2.pm2.byLen7) {
				HL1606_LED2_2(g_unCmdParam2.pm2.byColor6);
			} else if (wCurrent >= g_unCmdParam2.pm2.byLen8) {
				HL1606_LED2_2(g_unCmdParam2.pm2.byColor7);
			} else {
				HL1606_LED2_2(g_unCmdParam2.pm2.byColor8);
			}
		}
		m_wLED2_Offset --;
		if (m_wLED2_Offset == 0xFFFF) {
			m_wLED2_Offset = period - 1;
		}
	}
}

static void LED2_Mode3()
{
	BYTE color = g_unCmdParam2.pm3.byColor;
	BYTE freq = g_unCmdParam2.pm3.byFreq;

	if (HL1606_LED2_3_IsEnd()) {
		if (LED2_Mode3_DimDelayCounter > 0) {
			LED2_Mode3_DimDelayCounter --;
		} else {
			if (m_bLED2_LightOrDim == LED_TURN_LIGHT) {
				HL1606_LED2_3_1(0xFF, ColorToLight(color), FreqToSpeed(freq));
				m_bLED2_LightOrDim = LED_TURN_DIM;
			} else {
				HL1606_LED2_3_1(0xFF, ColorToDim(color), FreqToSpeed(freq));
				m_bLED2_LightOrDim = LED_TURN_LIGHT;
				LED2_Mode3_DimDelayCounter = 2;
			}
		}
	}
}

static void LED2_Mode4()
{
	BYTE tm;
	BYTE color = g_unCmdParam2.pm4.byColor;
	BYTE gap = g_unCmdParam2.pm4.byGap;
	BYTE speed = g_unCmdParam2.pm4.bySpeed;

	// 调整速度
	tm = SpeedToDelay(g_unCmdParam2.pm2.bySpeed);
	if (tm == 0) {
		return;
	}
	if ((m_wLED2_DelayCounter % tm) == 0) {
		// 处理偏移量
		if (m_wLED2_Offset == 0) {
			m_bLED2_Start = 1;
		}
		if (m_bLED2_Start) {
			// 启动一颗流星
			if (HL1606_LED2_4_IsEnd()) {
				HL1606_LED2_4_1(gap, ColorToDim(color),
				GetMode4Speed(speed), GetMode4Speed(speed));
			}
		}
		// 处理偏移量
		m_wLED2_Offset --;
		if (m_wLED2_Offset == 0xFFFF) {
			m_wLED2_Offset = gap - 1;
		}
	}
}

static void LED3_Mode1()
{
	if (!m_bLED3_Start) {
		HL1606_LED3_1(0x81);
		m_bLED3_Start = 1;
	}
}

static void LED3_Mode2()
{
	BYTE tm;
	WORD period = g_unCmdParam3.pm2.byLen1
				+ g_unCmdParam3.pm2.byLen2
				+ g_unCmdParam3.pm2.byLen3
				+ g_unCmdParam3.pm2.byLen4
				+ g_unCmdParam3.pm2.byLen5
				+ g_unCmdParam3.pm2.byLen6
				+ g_unCmdParam3.pm2.byLen7
				+ g_unCmdParam3.pm2.byLen8;
	if (period < 1) {
		return;
	}

	// 调整速度
	tm = SpeedToDelay(g_unCmdParam3.pm2.bySpeed);
	if (tm == 0) {
		return;
	}
	if ((m_wLED3_DelayCounter % tm) == 0) {
		if (m_wLED3_Offset == 0) {
			m_bLED3_Start = 1;
		}
		if (m_bLED3_Start) {
			WORD wCurrent = (m_wLED3_Offset + period - 1) % period;
 			if (wCurrent >= g_unCmdParam3.pm2.byLen8
						  + g_unCmdParam3.pm2.byLen7
						  + g_unCmdParam3.pm2.byLen6
						  + g_unCmdParam3.pm2.byLen5
						  + g_unCmdParam3.pm2.byLen4
						  + g_unCmdParam3.pm2.byLen3
						  + g_unCmdParam3.pm2.byLen2) {
				HL1606_LED3_2(g_unCmdParam3.pm2.byColor1);
			} else if (wCurrent >= g_unCmdParam3.pm2.byLen8
								 + g_unCmdParam3.pm2.byLen7
								 + g_unCmdParam3.pm2.byLen6
								 + g_unCmdParam3.pm2.byLen5
								 + g_unCmdParam3.pm2.byLen4
								 + g_unCmdParam3.pm2.byLen3) {
				HL1606_LED3_2(g_unCmdParam3.pm2.byColor2);
			} else if (wCurrent >= g_unCmdParam3.pm2.byLen8
								 + g_unCmdParam3.pm2.byLen7
								 + g_unCmdParam3.pm2.byLen6
								 + g_unCmdParam3.pm2.byLen5
								 + g_unCmdParam3.pm2.byLen4) {
				HL1606_LED3_2(g_unCmdParam3.pm2.byColor3);
			} else if (wCurrent >= g_unCmdParam3.pm2.byLen8
								 + g_unCmdParam3.pm2.byLen7
								 + g_unCmdParam3.pm2.byLen6
								 + g_unCmdParam3.pm2.byLen5) {
				HL1606_LED3_2(g_unCmdParam3.pm2.byColor4);
			} else if (wCurrent >= g_unCmdParam3.pm2.byLen8
								 + g_unCmdParam3.pm2.byLen7
								 + g_unCmdParam3.pm2.byLen6) {
				HL1606_LED3_2(g_unCmdParam3.pm2.byColor5);
			} else if (wCurrent >= g_unCmdParam3.pm2.byLen8
								 + g_unCmdParam3.pm2.byLen7) {
				HL1606_LED3_2(g_unCmdParam3.pm2.byColor6);
			} else if (wCurrent >= g_unCmdParam3.pm2.byLen8) {
				HL1606_LED3_2(g_unCmdParam3.pm2.byColor7);
			} else {
				HL1606_LED3_2(g_unCmdParam3.pm2.byColor8);
			}
		}
		m_wLED3_Offset --;
		if (m_wLED3_Offset == 0xFFFF) {
			m_wLED3_Offset = period - 1;
		}
	}
}

static void LED3_Mode3()
{
	BYTE color = g_unCmdParam3.pm3.byColor;
	BYTE freq = g_unCmdParam3.pm3.byFreq;

	if (HL1606_LED3_3_IsEnd()) {
		if (LED3_Mode3_DimDelayCounter > 0) {
			LED3_Mode3_DimDelayCounter --;
		} else {
			if (m_bLED3_LightOrDim == LED_TURN_LIGHT) {
				HL1606_LED3_3_1(0xFF, ColorToLight(color), FreqToSpeed(freq));
				m_bLED3_LightOrDim = LED_TURN_DIM;
			} else {
				HL1606_LED3_3_1(0xFF, ColorToDim(color), FreqToSpeed(freq));
				m_bLED3_LightOrDim = LED_TURN_LIGHT;
				LED3_Mode3_DimDelayCounter = 2;
			}
		}
	}
}

static void LED3_Mode4()
{
	BYTE tm;
	BYTE color = g_unCmdParam3.pm4.byColor;
	BYTE gap = g_unCmdParam3.pm4.byGap;
	BYTE speed = g_unCmdParam3.pm4.bySpeed;

	// 调整速度
	tm = SpeedToDelay(g_unCmdParam3.pm2.bySpeed);
	if (tm == 0) {
		return;
	}
	if ((m_wLED3_DelayCounter % tm) == 0) {
		// 处理偏移量
		if (m_wLED3_Offset == 0) {
			m_bLED3_Start = 1;
		}
		if (m_bLED3_Start) {
			// 启动一颗流星
			if (HL1606_LED3_4_IsEnd()) {
				HL1606_LED3_4_1(gap, ColorToDim(color),
				GetMode4Speed(speed), GetMode4Speed(speed));
			}
		}
		// 处理偏移量
		m_wLED3_Offset --;
		if (m_wLED3_Offset == 0xFFFF) {
			m_wLED3_Offset = gap - 1;
		}
	}
}

static void LED4_Mode1()
{
	if (!m_bLED4_Start) {
		HL1606_LED4_1(0x81);
		m_bLED4_Start = 1;
	}
}

static void LED4_Mode2()
{
	BYTE tm;
	WORD period = g_unCmdParam4.pm2.byLen1
				+ g_unCmdParam4.pm2.byLen2
				+ g_unCmdParam4.pm2.byLen3
				+ g_unCmdParam4.pm2.byLen4
				+ g_unCmdParam4.pm2.byLen5
				+ g_unCmdParam4.pm2.byLen6
				+ g_unCmdParam4.pm2.byLen7
				+ g_unCmdParam4.pm2.byLen8;
	if (period < 1) {
		return;
	}

	// 调整速度
	tm = SpeedToDelay(g_unCmdParam4.pm2.bySpeed);
	if (tm == 0) {
		return;
	}
	if ((m_wLED4_DelayCounter % tm) == 0) {
		if (m_wLED4_Offset == 0) {
			m_bLED4_Start = 1;
		}
		if (m_bLED4_Start) {
			WORD wCurrent = (m_wLED4_Offset + period - 1) % period;
 			if (wCurrent >= g_unCmdParam4.pm2.byLen8
						  + g_unCmdParam4.pm2.byLen7
						  + g_unCmdParam4.pm2.byLen6
						  + g_unCmdParam4.pm2.byLen5
						  + g_unCmdParam4.pm2.byLen4
						  + g_unCmdParam4.pm2.byLen3
						  + g_unCmdParam4.pm2.byLen2) {
				HL1606_LED4_2(g_unCmdParam4.pm2.byColor1);
			} else if (wCurrent >= g_unCmdParam4.pm2.byLen8
								 + g_unCmdParam4.pm2.byLen7
								 + g_unCmdParam4.pm2.byLen6
								 + g_unCmdParam4.pm2.byLen5
								 + g_unCmdParam4.pm2.byLen4
								 + g_unCmdParam4.pm2.byLen3) {
				HL1606_LED4_2(g_unCmdParam4.pm2.byColor2);
			} else if (wCurrent >= g_unCmdParam4.pm2.byLen8
								 + g_unCmdParam4.pm2.byLen7
								 + g_unCmdParam4.pm2.byLen6
								 + g_unCmdParam4.pm2.byLen5
								 + g_unCmdParam4.pm2.byLen4) {
				HL1606_LED4_2(g_unCmdParam4.pm2.byColor3);
			} else if (wCurrent >= g_unCmdParam4.pm2.byLen8
								 + g_unCmdParam4.pm2.byLen7
								 + g_unCmdParam4.pm2.byLen6
								 + g_unCmdParam4.pm2.byLen5) {
				HL1606_LED4_2(g_unCmdParam4.pm2.byColor4);
			} else if (wCurrent >= g_unCmdParam4.pm2.byLen8
								 + g_unCmdParam4.pm2.byLen7
								 + g_unCmdParam4.pm2.byLen6) {
				HL1606_LED4_2(g_unCmdParam4.pm2.byColor5);
			} else if (wCurrent >= g_unCmdParam4.pm2.byLen8
								 + g_unCmdParam4.pm2.byLen7) {
				HL1606_LED4_2(g_unCmdParam4.pm2.byColor6);
			} else if (wCurrent >= g_unCmdParam4.pm2.byLen8) {
				HL1606_LED4_2(g_unCmdParam4.pm2.byColor7);
			} else {
				HL1606_LED4_2(g_unCmdParam4.pm2.byColor8);
			}
		}
		m_wLED4_Offset --;
		if (m_wLED4_Offset == 0xFFFF) {
			m_wLED4_Offset = period - 1;
		}
	}
}

static void LED4_Mode3()
{
	BYTE color = g_unCmdParam4.pm3.byColor;
	BYTE freq = g_unCmdParam4.pm3.byFreq;

	if (HL1606_LED4_3_IsEnd()) {
		if (LED4_Mode3_DimDelayCounter > 0) {
			LED4_Mode3_DimDelayCounter --;
		} else {
			if (m_bLED4_LightOrDim == LED_TURN_LIGHT) {
				HL1606_LED4_3_1(0xFF, ColorToLight(color), FreqToSpeed(freq));
				m_bLED4_LightOrDim = LED_TURN_DIM;
			} else {
				HL1606_LED4_3_1(0xFF, ColorToDim(color), FreqToSpeed(freq));
				m_bLED4_LightOrDim = LED_TURN_LIGHT;
				LED4_Mode3_DimDelayCounter = 2;
			}
		}
	}
}

static void LED4_Mode4()
{
	BYTE tm;
	BYTE color = g_unCmdParam4.pm4.byColor;
	BYTE gap = g_unCmdParam4.pm4.byGap;
	BYTE speed = g_unCmdParam4.pm4.bySpeed;

	// 调整速度
	tm = SpeedToDelay(g_unCmdParam4.pm2.bySpeed);
	if (tm == 0) {
		return;
	}
	if ((m_wLED4_DelayCounter % tm) == 0) {
		// 处理偏移量
		if (m_wLED4_Offset == 0) {
			m_bLED4_Start = 1;
		}
		if (m_bLED4_Start) {
			// 启动一颗流星
			if (HL1606_LED4_4_IsEnd()) {
				HL1606_LED4_4_1(gap, ColorToDim(color),
				GetMode4Speed(speed), GetMode4Speed(speed));
			}
		}
		// 处理偏移量
		m_wLED4_Offset --;
		if (m_wLED4_Offset == 0xFFFF) {
			m_wLED4_Offset = gap - 1;
		}
	}
}

static BYTE LED1_Run()
{
	DWORD wDelayTime;
	switch (m_byLED1_ModeType) {
		case 1:
			LED1_Mode1();
			wDelayTime = g_unCmdParam1.pm1.wDelay;
			break;
		case 2:
			LED1_Mode2();
			wDelayTime = g_unCmdParam1.pm2.wDelay;
			break;
		case 3:
			LED1_Mode3();
			wDelayTime = g_unCmdParam1.pm3.wDelay;
			break;
		case 4:
			LED1_Mode4();
			wDelayTime = g_unCmdParam1.pm4.wDelay;
			break;
		default:break;
	}

	// 处理持续时间
	if (wDelayTime == 0) {
		m_wLED1_DelayCounter ++;
		return 0;
	}
	if (m_wLED1_DelayCounter < wDelayTime) {
		m_wLED1_DelayCounter ++;
		return 0;
	}
	return 1;
}

static BYTE LED2_Run()
{
	DWORD wDelayTime;
	switch (m_byLED2_ModeType) {
		case 1:
			LED2_Mode1();
			wDelayTime = g_unCmdParam2.pm1.wDelay;
			break;
		case 2:
			LED2_Mode2();
			wDelayTime = g_unCmdParam2.pm2.wDelay;
			break;
		case 3:
			LED2_Mode3();
			wDelayTime = g_unCmdParam2.pm3.wDelay;
			break;
		case 4:
			LED2_Mode4();
			wDelayTime = g_unCmdParam2.pm4.wDelay;
			break;
		default:break;
	}

	// 处理持续时间
	if (wDelayTime == 0) {
		m_wLED2_DelayCounter ++;
		return 0;
	}
	if (m_wLED2_DelayCounter < wDelayTime) {
		m_wLED2_DelayCounter ++;
		return 0;
	}
	return 1;
}

static BYTE LED3_Run()
{
	DWORD wDelayTime;
	switch (m_byLED3_ModeType) {
		case 1:
			LED3_Mode1();
			wDelayTime = g_unCmdParam3.pm1.wDelay;
			break;
		case 2:
			LED3_Mode2();
			wDelayTime = g_unCmdParam3.pm2.wDelay;
			break;
		case 3:
			LED3_Mode3();
			wDelayTime = g_unCmdParam3.pm3.wDelay;
			break;
		case 4:
			LED3_Mode4();
			wDelayTime = g_unCmdParam3.pm4.wDelay;
			break;
		default:break;
	}

	// 处理持续时间
	if (wDelayTime == 0) {
		m_wLED3_DelayCounter ++;
		return 0;
	}
	if (m_wLED3_DelayCounter < wDelayTime) {
		m_wLED3_DelayCounter ++;
		return 0;
	}
	return 1;
}

static BYTE LED4_Run()
{
	DWORD wDelayTime;
	switch (m_byLED4_ModeType) {
		case 1:
			LED4_Mode1();
			wDelayTime = g_unCmdParam4.pm1.wDelay;
			break;
		case 2:
			LED4_Mode2();
			wDelayTime = g_unCmdParam4.pm2.wDelay;
			break;
		case 3:
			LED4_Mode3();
			wDelayTime = g_unCmdParam4.pm3.wDelay;
			break;
		case 4:
			LED4_Mode4();
			wDelayTime = g_unCmdParam4.pm4.wDelay;
			break;
		default:break;
	}

	// 处理持续时间
	if (wDelayTime == 0) {
		m_wLED4_DelayCounter ++;
		return 0;
	}
	if (m_wLED4_DelayCounter < wDelayTime) {
		m_wLED4_DelayCounter ++;
		return 0;
	}
	return 1;
}

void LED_Init()
{
	m_byLED1_ModeCount = GetCodeCount(g_byaByteCode1);
	m_byLED2_ModeCount = GetCodeCount(g_byaByteCode2);
	m_byLED3_ModeCount = GetCodeCount(g_byaByteCode3);
	m_byLED4_ModeCount = GetCodeCount(g_byaByteCode4);
	m_byLED1_ModeIndex = 0;
	m_byLED2_ModeIndex = 0;
	m_byLED3_ModeIndex = 0;
	m_byLED4_ModeIndex = 0;
	m_byLED1_ModeType = GetByteCode(g_byaByteCode1, m_byLED1_ModeIndex, &g_unCmdParam1);
	m_byLED2_ModeType = GetByteCode(g_byaByteCode2, m_byLED2_ModeIndex, &g_unCmdParam2);
	m_byLED3_ModeType = GetByteCode(g_byaByteCode3, m_byLED3_ModeIndex, &g_unCmdParam3);
	m_byLED4_ModeType = GetByteCode(g_byaByteCode4, m_byLED4_ModeIndex, &g_unCmdParam4);

	LED1_InitMode();
	LED2_InitMode();
	LED3_InitMode();
	LED4_InitMode();
}

void LED_Run()
{
	BYTE byModeEnd;

	byModeEnd = LED1_Run();
	if (byModeEnd) {
		m_byLED1_ModeIndex ++;
		if (m_byLED1_ModeIndex >= m_byLED1_ModeCount) {
			m_byLED1_ModeIndex = 0;
		}
		m_byLED1_ModeType = GetByteCode(g_byaByteCode1, m_byLED1_ModeIndex, &g_unCmdParam1);
		LED1_InitMode();
	}

	byModeEnd = LED2_Run();
	if (byModeEnd) {
		m_byLED2_ModeIndex ++;
		if (m_byLED2_ModeIndex >= m_byLED2_ModeCount) {
			m_byLED2_ModeIndex = 0;
		}
		m_byLED2_ModeType = GetByteCode(g_byaByteCode2, m_byLED2_ModeIndex, &g_unCmdParam2);
		LED2_InitMode();
	}

	byModeEnd = LED3_Run();
	if (byModeEnd) {
		m_byLED3_ModeIndex ++;
		if (m_byLED3_ModeIndex >= m_byLED3_ModeCount) {
			m_byLED3_ModeIndex = 0;
		}
		m_byLED3_ModeType = GetByteCode(g_byaByteCode3, m_byLED3_ModeIndex, &g_unCmdParam3);
		LED3_InitMode();
	}

	byModeEnd = LED4_Run();
	if (byModeEnd) {
		m_byLED4_ModeIndex ++;
		if (m_byLED4_ModeIndex >= m_byLED4_ModeCount) {
			m_byLED4_ModeIndex = 0;
		}
		m_byLED4_ModeType = GetByteCode(g_byaByteCode4, m_byLED4_ModeIndex, &g_unCmdParam4);
		LED4_InitMode();
	}
}

void LED_1msProc()
{
	// 呼吸模式的延时处理
	HL1606_LED1_3_2();
	HL1606_LED2_3_2();
	HL1606_LED3_3_2();
	HL1606_LED4_3_2();

	// 拖尾模式的延时处理
	HL1606_LED1_4_2();
	HL1606_LED1_4_3();
	HL1606_LED1_4_4();
	HL1606_LED1_4_5();
	HL1606_LED1_4_6();
	HL1606_LED2_4_2();
	HL1606_LED2_4_3();
	HL1606_LED2_4_4();
	HL1606_LED2_4_5();
	HL1606_LED2_4_6();
	HL1606_LED3_4_2();
	HL1606_LED3_4_3();
	HL1606_LED3_4_4();
	HL1606_LED3_4_5();
	HL1606_LED3_4_6();
	HL1606_LED4_4_2();
	HL1606_LED4_4_3();
	HL1606_LED4_4_4();
	HL1606_LED4_4_5();
	HL1606_LED4_4_6();
}