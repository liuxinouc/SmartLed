/*-----------------------------------------------
 * 所有与HL1606灯带控制相关的函数
------------------------------------------------*/  

#ifndef __HL1606_H__
#define __HL1606_H__

// 与常亮模式相关的函数
void HL1606_LED1_1(BYTE color);
void HL1606_LED2_1(BYTE color);
void HL1606_LED3_1(BYTE color);
void HL1606_LED4_1(BYTE color);

// 与流水模式相关的函数
void HL1606_LED1_2(BYTE color);
void HL1606_LED2_2(BYTE color);
void HL1606_LED3_2(BYTE color);
void HL1606_LED4_2(BYTE color);

// 与呼吸模式相关的函数
void HL1606_LED1_3_1(BYTE k, BYTE color, BYTE speed);
void HL1606_LED1_3_2();
BYTE HL1606_LED1_3_IsEnd();
void HL1606_LED2_3_1(BYTE k, BYTE color, BYTE speed);
void HL1606_LED2_3_2();
BYTE HL1606_LED2_3_IsEnd();
void HL1606_LED3_3_1(BYTE k, BYTE color, BYTE speed);
void HL1606_LED3_3_2();
BYTE HL1606_LED3_3_IsEnd();
void HL1606_LED4_3_1(BYTE k, BYTE color, BYTE speed);
void HL1606_LED4_3_2();
BYTE HL1606_LED4_3_IsEnd(); 

// 与拖尾模式相关的函数
void HL1606_LED1_4_1(BYTE k, BYTE color, BYTE speed0, BYTE speed);
void HL1606_LED1_4_2();
void HL1606_LED1_4_3();
void HL1606_LED1_4_4();
void HL1606_LED1_4_5();
void HL1606_LED1_4_6();
BYTE HL1606_LED1_4_IsEnd();
void HL1606_LED2_4_1(BYTE k, BYTE color, BYTE speed0, BYTE speed);
void HL1606_LED2_4_2();
void HL1606_LED2_4_3();
void HL1606_LED2_4_4();
void HL1606_LED2_4_5();
void HL1606_LED2_4_6();
BYTE HL1606_LED2_4_IsEnd();
void HL1606_LED3_4_1(BYTE k, BYTE color, BYTE speed0, BYTE speed);
void HL1606_LED3_4_2();
void HL1606_LED3_4_3();
void HL1606_LED3_4_4();
void HL1606_LED3_4_5();
void HL1606_LED3_4_6();
BYTE HL1606_LED3_4_IsEnd();
void HL1606_LED4_4_1(BYTE k, BYTE color, BYTE speed0, BYTE speed);
void HL1606_LED4_4_2();
void HL1606_LED4_4_3();
void HL1606_LED4_4_4();
void HL1606_LED4_4_5();
void HL1606_LED4_4_6();
BYTE HL1606_LED4_4_IsEnd();

#endif //__HL1606_H__