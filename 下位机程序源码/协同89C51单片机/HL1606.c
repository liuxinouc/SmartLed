/*-----------------------------------------------
 * 所有与HL1606灯带控制相关的函数
------------------------------------------------*/  


#include <reg52.h>          //头文件的包含
#include <intrins.h>
#include "ByteCode.h"
#include "HL1606.h"

sbit HL1606_SI1  = P1^0;
sbit HL1606_DIN1 = P1^1;
sbit HL1606_CLK1 = P1^2;
sbit HL1606_LI1  = P1^3;

sbit HL1606_SI2  = P1^4;
sbit HL1606_DIN2 = P1^5;
sbit HL1606_CLK2 = P1^6;
sbit HL1606_LI2  = P1^7;

sbit HL1606_SI3  = P2^0;
sbit HL1606_DIN3 = P2^1;
sbit HL1606_CLK3 = P2^2;
sbit HL1606_LI3  = P2^3;

sbit HL1606_SI4  = P2^4;
sbit HL1606_DIN4 = P2^5;
sbit HL1606_CLK4 = P2^6;
sbit HL1606_LI4  = P2^7;

static void mDelay(WORD Delay)//延时子程序
{
	BYTE i;
	for (; Delay>0; Delay--) {
		for (i=0; i<55; i++) {
			;
		}
	}
}

static void HL1606_send81(BYTE send8bit)
{
	BYTE i, send8;
	send8 = send8bit;
	for (i=1; i<=8; i++) {
		HL1606_DIN1 = (bit)(send8 & 0x80);
		_nop_();
		_nop_();

		HL1606_CLK1 = 0;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		HL1606_CLK1 = 1;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		send8 = send8<<1;
	}
}

static void HL1606_send82(BYTE send8bit)  
{
	BYTE i, send8;
	send8 = send8bit;
	for (i=1; i<=8; i++) {
		HL1606_DIN2 = (bit)(send8 & 0x80);
		_nop_();
		_nop_();

		HL1606_CLK2 = 0;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		HL1606_CLK2 = 1;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		send8 = send8<<1;
	}
}

static void HL1606_send83(BYTE send8bit)  
{   
	BYTE i, send8;
	send8 = send8bit;
	for (i=1; i<=8; i++) {
		HL1606_DIN3 = (bit)(send8 & 0x80);
		_nop_();
		_nop_();

		HL1606_CLK3 = 0;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		HL1606_CLK3 = 1;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		send8 = send8<<1;
	}
}

static void HL1606_send84(BYTE send8bit)  
{
	BYTE i, send8;
	send8 = send8bit;
	for (i=1; i<=8; i++) {
		HL1606_DIN4 = (bit)(send8 & 0x80);
		_nop_();
		_nop_();

		HL1606_CLK4 = 0;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		HL1606_CLK4 = 1;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		send8 = send8<<1;
	}
}

static void HL1606_TBXH1(WORD TBXH_count,WORD TBXH_time) 
{
	WORD i;
	for (i=1; i<=TBXH_count; i++) {
		HL1606_SI1 = 0;
		mDelay(TBXH_time);
		HL1606_SI1 = 1;
		mDelay(TBXH_time);
	}
}

static void HL1606_TBXH2(WORD TBXH_count,WORD TBXH_time) 
{
	WORD i;
	for (i=1; i<=TBXH_count; i++) {
		HL1606_SI2 = 0;
		mDelay(TBXH_time);
		HL1606_SI2 = 1;
		mDelay(TBXH_time);
	}
}

static void HL1606_TBXH3(WORD TBXH_count,WORD TBXH_time) 
{
	WORD i;
	for (i=1; i<=TBXH_count; i++) {
		HL1606_SI3=0;
		mDelay(TBXH_time);
		HL1606_SI3=1;
		mDelay(TBXH_time);
	}
}

static void HL1606_TBXH4(WORD TBXH_count,WORD TBXH_time) 
{
	WORD i;
	for (i=1; i<=TBXH_count; i++) {
		HL1606_SI4 = 0;
		mDelay(TBXH_time);
		HL1606_SI4 = 1;
		mDelay(TBXH_time);
	}
}

//1号灯带常亮
void HL1606_LED1_1(BYTE color)
{
	BYTE i;
	HL1606_LI1 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	for (i=0; i<250; i++) {
		HL1606_send81(color);
	}
	for (i=0; i<250; i++) {
		HL1606_send81(color);
	}
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI1 = 1;
	_nop_();
	_nop_();

	HL1606_TBXH1(5, 5);
}

//2号灯带常亮
void HL1606_LED2_1(BYTE color)
{
	BYTE i;
	HL1606_LI2 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	for (i=0; i<250; i++) {
		HL1606_send82(color);
	}
	for (i=0; i<250; i++){
		HL1606_send82(color);
	}
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI2 = 1;
	_nop_();
	_nop_();

	HL1606_TBXH2(5, 5);
}

//3号灯带常亮
void HL1606_LED3_1(BYTE color)
{
	BYTE i;
	HL1606_LI3 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	for (i=0; i<250; i++) {
		HL1606_send83(color);
	}
	for (i=0; i<250; i++) {
		HL1606_send83(color);
	}
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI3 = 1;
	_nop_();
	_nop_();

	HL1606_TBXH3(5, 5);
}

//4号灯带常亮
void HL1606_LED4_1(BYTE color)
{
	BYTE i;
	HL1606_LI4 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	for (i=0; i<250; i++) {
		HL1606_send84(color);
	}
	for (i=0; i<250; i++) {
		HL1606_send84(color);
	}
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI4 = 1;
	_nop_();
	_nop_();

	HL1606_TBXH4(5, 5);
}

// 1号灯带流动
void HL1606_LED1_2(BYTE color)
{
	HL1606_LI1 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_send81(color);
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI1 = 1;
	_nop_();
	_nop_();

	HL1606_TBXH1(1, 1);
}

// 2号灯带流动
void HL1606_LED2_2(BYTE color)
{
	HL1606_LI2 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_send82(color);
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI2 = 1;
	_nop_();
	_nop_();

	HL1606_TBXH2(1, 1);
}

// 3号灯带流动
void HL1606_LED3_2(BYTE color)
{
	HL1606_LI3 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_send83(color);
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI3 = 1;
	_nop_();
	_nop_();

	HL1606_TBXH3(1, 1);
}

// 4号灯带流动
void HL1606_LED4_2(BYTE color)
{
	HL1606_LI4 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_send84(color);
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI4 = 1;
	_nop_();
	_nop_();

	HL1606_TBXH4(1, 1);
}

// 1号灯带呼吸
static xdata BYTE m_byLED1_Mode3_Counter = 0xFF;
static xdata BYTE m_byLED1_Mode3_SpeedCounter;
static xdata BYTE m_byLED1_Mode3_Speed;
void HL1606_LED1_3_1(BYTE k, BYTE color, BYTE speed)
{
	WORD i;
	HL1606_LI1 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	for (i=0; i<k; i++) {
		HL1606_send81(color);
	}
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI1 = 1;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	m_byLED1_Mode3_Counter = 0;
	m_byLED1_Mode3_SpeedCounter = 0;
	m_byLED1_Mode3_Speed = speed;
}

void HL1606_LED1_3_2()
{
	if (0xFF == m_byLED1_Mode3_Counter) {
		return;
	}
	if (0 == m_byLED1_Mode3_SpeedCounter) {
		if (m_byLED1_Mode3_Counter < 254) {
			if (0 == m_byLED1_Mode3_Counter % 2) {
				HL1606_SI1 = 0;
			} else {
				HL1606_SI1 = 1;
			}
			m_byLED1_Mode3_Counter ++;
		} else {
			m_byLED1_Mode3_Counter = 0xFF;
		}
	}
	m_byLED1_Mode3_SpeedCounter ++;
	if (m_byLED1_Mode3_SpeedCounter == m_byLED1_Mode3_Speed) {
		m_byLED1_Mode3_SpeedCounter = 0;
	}
}

BYTE HL1606_LED1_3_IsEnd()
{
	return (0xFF == m_byLED1_Mode3_Counter);
}

// 2号灯带呼吸 
static xdata BYTE m_byLED2_Mode3_Counter = 0xFF;
static xdata BYTE m_byLED2_Mode3_SpeedCounter;
static xdata BYTE m_byLED2_Mode3_Speed;
void HL1606_LED2_3_1(BYTE k, BYTE color, BYTE speed)
{
	WORD i;
	HL1606_LI2 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	for (i=0; i<k; i++) {
		HL1606_send82(color);
	}
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI2 = 1;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	m_byLED2_Mode3_Counter = 0;
	m_byLED2_Mode3_SpeedCounter = 0;
	m_byLED2_Mode3_Speed = speed;
}

void HL1606_LED2_3_2()
{
	if (0xFF == m_byLED2_Mode3_Counter) {
		return;
	}
	if (0 == m_byLED2_Mode3_SpeedCounter) {
		if (m_byLED2_Mode3_Counter < 254) {
			if (0 == m_byLED2_Mode3_Counter % 2) {
				HL1606_SI2 = 0;
			} else {
				HL1606_SI2 = 1;
			}
			m_byLED2_Mode3_Counter ++;
		} else {
			m_byLED2_Mode3_Counter = 0xFF;
		}
	}
	m_byLED2_Mode3_SpeedCounter ++;
	if (m_byLED2_Mode3_SpeedCounter == m_byLED2_Mode3_Speed) {
		m_byLED2_Mode3_SpeedCounter = 0;
	}
}

BYTE HL1606_LED2_3_IsEnd()
{
	return (0xFF == m_byLED2_Mode3_Counter);
}

// 3号灯带呼吸 
static xdata BYTE m_byLED3_Mode3_Counter = 0xFF;
static xdata BYTE m_byLED3_Mode3_SpeedCounter;
static xdata BYTE m_byLED3_Mode3_Speed;
void HL1606_LED3_3_1(BYTE k, BYTE color, BYTE speed)
{
	WORD i;
	HL1606_LI3 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	for (i=0; i<k; i++) {
		HL1606_send83(color);
	}
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI3 = 1;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	m_byLED3_Mode3_Counter = 0;
	m_byLED3_Mode3_SpeedCounter = 0;
	m_byLED3_Mode3_Speed = speed;
}

void HL1606_LED3_3_2()
{
	if (0xFF == m_byLED3_Mode3_Counter) {
		return;
	}
	if (0 == m_byLED3_Mode3_SpeedCounter) {
		if (m_byLED3_Mode3_Counter < 254) {
			if (0 == m_byLED3_Mode3_Counter % 2) {
				HL1606_SI3 = 0;
			} else {
				HL1606_SI3 = 1;
			}
			m_byLED3_Mode3_Counter ++;
		} else {
			m_byLED3_Mode3_Counter = 0xFF;
		}
	}
	m_byLED3_Mode3_SpeedCounter ++;
	if (m_byLED3_Mode3_SpeedCounter == m_byLED3_Mode3_Speed) {
		m_byLED3_Mode3_SpeedCounter = 0;
	}
}

BYTE HL1606_LED3_3_IsEnd()
{
	return (0xFF == m_byLED3_Mode3_Counter);
}

// 4号灯带呼吸 
static xdata BYTE m_byLED4_Mode3_Counter = 0xFF;
static xdata BYTE m_byLED4_Mode3_SpeedCounter;
static xdata BYTE m_byLED4_Mode3_Speed;
void HL1606_LED4_3_1(BYTE k, BYTE color, BYTE speed)
{
	WORD i;
	HL1606_LI4 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	for (i=0; i<k; i++) {
		HL1606_send84(color);
	}
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI4 = 1;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	m_byLED4_Mode3_Counter = 0;
	m_byLED4_Mode3_SpeedCounter = 0;
	m_byLED4_Mode3_Speed = speed;
}

void HL1606_LED4_3_2()
{
	if (0xFF == m_byLED4_Mode3_Counter) {
		return;
	}
	if (0 == m_byLED4_Mode3_SpeedCounter) {
		if (m_byLED4_Mode3_Counter < 254) {
			if (0 == m_byLED4_Mode3_Counter % 2) {
				HL1606_SI4 = 0;
			} else {
				HL1606_SI4 = 1;
			}
			m_byLED4_Mode3_Counter ++;
		} else {
			m_byLED4_Mode3_Counter = 0xFF;
		}
	}
	m_byLED4_Mode3_SpeedCounter ++;
	if (m_byLED4_Mode3_SpeedCounter == m_byLED4_Mode3_Speed) {
		m_byLED4_Mode3_SpeedCounter = 0;
	}
}

BYTE HL1606_LED4_3_IsEnd()
{
	return (0xFF == m_byLED4_Mode3_Counter);
}

// 1号灯带流星跑动
static idata BYTE m_byLED1_4_LoopFlag = 0; // 0:退出；n:在_n函数中
static idata WORD m_wLED1_4_LoopK;
static idata WORD m_wLED1_4_LoopKi;
static idata WORD m_wLED1_4_LoopS;
static idata WORD m_wLED1_4_LoopSi;
static idata WORD m_wLED1_4_DelayS;
static idata WORD m_wLED1_4_DelaySi;
static idata WORD m_wLED1_4_LoopSpeed0;
static idata WORD m_wLED1_4_LoopSpeed;
void HL1606_LED1_4_1(BYTE k, BYTE color, BYTE speed0, BYTE speed)
{
	m_byLED1_4_LoopFlag = 1;

	HL1606_LI1 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_send81(color);
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI1 = 1;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	m_wLED1_4_LoopK = k;
	m_wLED1_4_LoopSpeed0 = speed0 * 2;
	m_wLED1_4_LoopSpeed = speed;
	m_wLED1_4_LoopSi = 1;

	m_byLED1_4_LoopFlag = 2;
}

void HL1606_LED1_4_2()
{
	if (2 != m_byLED1_4_LoopFlag) {
		return;
	}
	if (m_wLED1_4_LoopSi < m_wLED1_4_LoopSpeed0) {
		m_wLED1_4_LoopSi ++;

		if (0 == m_wLED1_4_LoopSi % 2) {
			HL1606_SI1 = 0;
		} else {
			HL1606_SI1 = 1;
		}

		// 延时循环
		m_wLED1_4_DelayS = m_wLED1_4_LoopSpeed;
		m_wLED1_4_DelaySi = 0;
		m_byLED1_4_LoopFlag = 3;
	} else {
		// 退出
		m_byLED1_4_LoopFlag = 4;
		m_wLED1_4_LoopKi = 0;
	}
}

void HL1606_LED1_4_3()
{
	if (3 != m_byLED1_4_LoopFlag) {
		return;
	}
	if (m_wLED1_4_DelaySi < m_wLED1_4_DelayS) {
		m_wLED1_4_DelaySi ++;
		// 本函数仅用于延时
	} else {
		m_byLED1_4_LoopFlag = 2;
	}
}

void HL1606_LED1_4_4()
{
	if (4 != m_byLED1_4_LoopFlag) {
		return;
	}
	if (m_wLED1_4_LoopKi < m_wLED1_4_LoopK) {
		HL1606_LI1 = 0;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		HL1606_send81(0x00);
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		HL1606_LI1 = 1;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		m_wLED1_4_LoopKi ++;
		m_wLED1_4_LoopSi = 0;
		m_byLED1_4_LoopFlag = 5;
	} else {
		m_byLED1_4_LoopFlag = 0;
	}
}

void HL1606_LED1_4_5()
{
	if (5 != m_byLED1_4_LoopFlag) {
		return;
	}
	if (m_wLED1_4_LoopSi < m_wLED1_4_LoopSpeed0) {
		m_wLED1_4_LoopSi ++;

		if (0 == m_wLED1_4_LoopSi % 2) {
			HL1606_SI1 = 0;
		} else {
			HL1606_SI1 = 1;
		}

		// 延时循环
		m_wLED1_4_DelayS = m_wLED1_4_LoopSpeed;
		m_wLED1_4_DelaySi = 0;
		m_byLED1_4_LoopFlag = 6;
	} else {
		// 退出
		m_byLED1_4_LoopFlag = 4;
	}
}

void HL1606_LED1_4_6()
{
	if (6 != m_byLED1_4_LoopFlag) {
		return;
	}
	if (m_wLED1_4_DelaySi < m_wLED1_4_DelayS) {
		m_wLED1_4_DelaySi ++;
		// 本函数仅用于延时
	} else {
		m_byLED1_4_LoopFlag = 5;
	}
}

BYTE HL1606_LED1_4_IsEnd()
{
	return (0 == m_byLED1_4_LoopFlag);
}

// 2号灯带流星跑动
static idata BYTE m_byLED2_4_LoopFlag = 0; // 0:退出；n:在_n函数中
static idata WORD m_wLED2_4_LoopK;
static idata WORD m_wLED2_4_LoopKi;
static idata WORD m_wLED2_4_LoopS;
static idata WORD m_wLED2_4_LoopSi;
static idata WORD m_wLED2_4_DelayS;
static idata WORD m_wLED2_4_DelaySi;
static idata WORD m_wLED2_4_LoopSpeed0;
static idata WORD m_wLED2_4_LoopSpeed;
void HL1606_LED2_4_1(BYTE k, BYTE color, BYTE speed0, BYTE speed)
{
	m_byLED2_4_LoopFlag = 1;

	HL1606_LI2 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_send82(color);
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI2 = 1;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	m_wLED2_4_LoopK = k;
	m_wLED2_4_LoopSpeed0 = speed0 * 2;
	m_wLED2_4_LoopSpeed = speed;
	m_wLED2_4_LoopSi = 1;

	m_byLED2_4_LoopFlag = 2;
}

void HL1606_LED2_4_2()
{
	if (2 != m_byLED2_4_LoopFlag) {
		return;
	}
	if (m_wLED2_4_LoopSi < m_wLED2_4_LoopSpeed0) {
		m_wLED2_4_LoopSi ++;

		if (0 == m_wLED2_4_LoopSi % 2) {
			HL1606_SI2 = 0;
		} else {
			HL1606_SI2 = 1;
		}

		// 延时循环
		m_wLED2_4_DelayS = m_wLED2_4_LoopSpeed;
		m_wLED2_4_DelaySi = 0;
		m_byLED2_4_LoopFlag = 3;
	} else {
		// 退出
		m_byLED2_4_LoopFlag = 4;
		m_wLED2_4_LoopKi = 0;
	}
}

void HL1606_LED2_4_3()
{
	if (3 != m_byLED2_4_LoopFlag) {
		return;
	}
	if (m_wLED2_4_DelaySi < m_wLED2_4_DelayS) {
		m_wLED2_4_DelaySi ++;
		// 本函数仅用于延时
	} else {
		m_byLED2_4_LoopFlag = 2;
	}
}

void HL1606_LED2_4_4()
{
	if (4 != m_byLED2_4_LoopFlag) {
		return;
	}
	if (m_wLED2_4_LoopKi < m_wLED2_4_LoopK) {
		HL1606_LI2 = 0;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		HL1606_send82(0x00);
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		HL1606_LI2 = 1;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		m_wLED2_4_LoopKi ++;
		m_wLED2_4_LoopSi = 0;
		m_byLED2_4_LoopFlag = 5;
	} else {
		m_byLED2_4_LoopFlag = 0;
	}
}

void HL1606_LED2_4_5()
{
	if (5 != m_byLED2_4_LoopFlag) {
		return;
	}
	if (m_wLED2_4_LoopSi < m_wLED2_4_LoopSpeed0) {
		m_wLED2_4_LoopSi ++;

		if (0 == m_wLED2_4_LoopSi % 2) {
			HL1606_SI2 = 0;
		} else {
			HL1606_SI2 = 1;
		}

		// 延时循环
		m_wLED2_4_DelayS = m_wLED2_4_LoopSpeed;
		m_wLED2_4_DelaySi = 0;
		m_byLED2_4_LoopFlag = 6;
	} else {
		// 退出
		m_byLED2_4_LoopFlag = 4;
	}
}

void HL1606_LED2_4_6()
{
	if (6 != m_byLED2_4_LoopFlag) {
		return;
	}
	if (m_wLED2_4_DelaySi < m_wLED2_4_DelayS) {
		m_wLED2_4_DelaySi ++;
		// 本函数仅用于延时
	} else {
		m_byLED2_4_LoopFlag = 5;
	}
}

BYTE HL1606_LED2_4_IsEnd()
{
	return (0 == m_byLED2_4_LoopFlag);
}

// 3号灯带流星跑动
static idata BYTE m_byLED3_4_LoopFlag = 0; // 0:退出；n:在_n函数中
static idata WORD m_wLED3_4_LoopK;
static idata WORD m_wLED3_4_LoopKi;
static idata WORD m_wLED3_4_LoopS;
static idata WORD m_wLED3_4_LoopSi;
static idata WORD m_wLED3_4_DelayS;
static idata WORD m_wLED3_4_DelaySi;
static idata WORD m_wLED3_4_LoopSpeed0;
static idata WORD m_wLED3_4_LoopSpeed;
void HL1606_LED3_4_1(BYTE k, BYTE color, BYTE speed0, BYTE speed)
{
	m_byLED3_4_LoopFlag = 1;

	HL1606_LI3 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_send83(color);
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI3 = 1;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	m_wLED3_4_LoopK = k;
	m_wLED3_4_LoopSpeed0 = speed0 * 2;
	m_wLED3_4_LoopSpeed = speed;
	m_wLED3_4_LoopSi = 1;

	m_byLED3_4_LoopFlag = 2;
}

void HL1606_LED3_4_2()
{
	if (2 != m_byLED3_4_LoopFlag) {
		return;
	}
	if (m_wLED3_4_LoopSi < m_wLED3_4_LoopSpeed0) {
		m_wLED3_4_LoopSi ++;

		if (0 == m_wLED3_4_LoopSi % 2) {
			HL1606_SI3 = 0;
		} else {
			HL1606_SI3 = 1;
		}

		// 延时循环
		m_wLED3_4_DelayS = m_wLED3_4_LoopSpeed;
		m_wLED3_4_DelaySi = 0;
		m_byLED3_4_LoopFlag = 3;
	} else {
		// 退出
		m_byLED3_4_LoopFlag = 4;
		m_wLED3_4_LoopKi = 0;
	}
}

void HL1606_LED3_4_3()
{
	if (3 != m_byLED3_4_LoopFlag) {
		return;
	}
	if (m_wLED3_4_DelaySi < m_wLED3_4_DelayS) {
		m_wLED3_4_DelaySi ++;
		// 本函数仅用于延时
	} else {
		m_byLED3_4_LoopFlag = 2;
	}
}

void HL1606_LED3_4_4()
{
	if (4 != m_byLED3_4_LoopFlag) {
		return;
	}
	if (m_wLED3_4_LoopKi < m_wLED3_4_LoopK) {
		HL1606_LI3 = 0;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		HL1606_send83(0x00);
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		HL1606_LI3 = 1;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		m_wLED3_4_LoopKi ++;
		m_wLED3_4_LoopSi = 0;
		m_byLED3_4_LoopFlag = 5;
	} else {
		m_byLED3_4_LoopFlag = 0;
	}
}

void HL1606_LED3_4_5()
{
	if (5 != m_byLED3_4_LoopFlag) {
		return;
	}
	if (m_wLED3_4_LoopSi < m_wLED3_4_LoopSpeed0) {
		m_wLED3_4_LoopSi ++;

		if (0 == m_wLED3_4_LoopSi % 2) {
			HL1606_SI3 = 0;
		} else {
			HL1606_SI3 = 1;
		}

		// 延时循环
		m_wLED3_4_DelayS = m_wLED3_4_LoopSpeed;
		m_wLED3_4_DelaySi = 0;
		m_byLED3_4_LoopFlag = 6;
	} else {
		// 退出
		m_byLED3_4_LoopFlag = 4;
	}
}

void HL1606_LED3_4_6()
{
	if (6 != m_byLED3_4_LoopFlag) {
		return;
	}
	if (m_wLED3_4_DelaySi < m_wLED3_4_DelayS) {
		m_wLED3_4_DelaySi ++;
		// 本函数仅用于延时
	} else {
		m_byLED3_4_LoopFlag = 5;
	}
}

BYTE HL1606_LED3_4_IsEnd()
{
	return (0 == m_byLED3_4_LoopFlag);
}

// 4号灯带流星跑动
static idata BYTE m_byLED4_4_LoopFlag = 0; // 0:退出；n:在_n函数中
static idata WORD m_wLED4_4_LoopK;
static idata WORD m_wLED4_4_LoopKi;
static idata WORD m_wLED4_4_LoopS;
static idata WORD m_wLED4_4_LoopSi;
static idata WORD m_wLED4_4_DelayS;
static idata WORD m_wLED4_4_DelaySi;
static idata WORD m_wLED4_4_LoopSpeed0;
static idata WORD m_wLED4_4_LoopSpeed;
void HL1606_LED4_4_1(BYTE k, BYTE color, BYTE speed0, BYTE speed)
{
	m_byLED4_4_LoopFlag = 1;

	HL1606_LI4 = 0;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_send84(color);
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	HL1606_LI4 = 1;
	_nop_();
	_nop_();
	_nop_();
	_nop_();

	m_wLED4_4_LoopK = k;
	m_wLED4_4_LoopSpeed0 = speed0 * 2;
	m_wLED4_4_LoopSpeed = speed;
	m_wLED4_4_LoopSi = 1;

	m_byLED4_4_LoopFlag = 2;
}

void HL1606_LED4_4_2()
{
	if (2 != m_byLED4_4_LoopFlag) {
		return;
	}
	if (m_wLED4_4_LoopSi < m_wLED4_4_LoopSpeed0) {
		m_wLED4_4_LoopSi ++;

		if (0 == m_wLED4_4_LoopSi % 2) {
			HL1606_SI4 = 0;
		} else {
			HL1606_SI4 = 1;
		}

		// 延时循环
		m_wLED4_4_DelayS = m_wLED4_4_LoopSpeed;
		m_wLED4_4_DelaySi = 0;
		m_byLED4_4_LoopFlag = 3;
	} else {
		// 退出
		m_byLED4_4_LoopFlag = 4;
		m_wLED4_4_LoopKi = 0;
	}
}

void HL1606_LED4_4_3()
{
	if (3 != m_byLED4_4_LoopFlag) {
		return;
	}
	if (m_wLED4_4_DelaySi < m_wLED4_4_DelayS) {
		m_wLED4_4_DelaySi ++;
		// 本函数仅用于延时
	} else {
		m_byLED4_4_LoopFlag = 2;
	}
}

void HL1606_LED4_4_4()
{
	if (4 != m_byLED4_4_LoopFlag) {
		return;
	}
	if (m_wLED4_4_LoopKi < m_wLED4_4_LoopK) {
		HL1606_LI4 = 0;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		HL1606_send84(0x00);
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		HL1606_LI4 = 1;
		_nop_();
		_nop_();
		_nop_();
		_nop_();

		m_wLED4_4_LoopKi ++;
		m_wLED4_4_LoopSi = 0;
		m_byLED4_4_LoopFlag = 5;
	} else {
		m_byLED4_4_LoopFlag = 0;
	}
}

void HL1606_LED4_4_5()
{
	if (5 != m_byLED4_4_LoopFlag) {
		return;
	}
	if (m_wLED4_4_LoopSi < m_wLED4_4_LoopSpeed0) {
		m_wLED4_4_LoopSi ++;

		if (0 == m_wLED4_4_LoopSi % 2) {
			HL1606_SI4 = 0;
		} else {
			HL1606_SI4 = 1;
		}

		// 延时循环
		m_wLED4_4_DelayS = m_wLED4_4_LoopSpeed;
		m_wLED4_4_DelaySi = 0;
		m_byLED4_4_LoopFlag = 6;
	} else {
		// 退出
		m_byLED4_4_LoopFlag = 4;
	}
}

void HL1606_LED4_4_6()
{
	if (6 != m_byLED4_4_LoopFlag) {
		return;
	}
	if (m_wLED4_4_DelaySi < m_wLED4_4_DelayS) {
		m_wLED4_4_DelaySi ++;
		// 本函数仅用于延时
	} else {
		m_byLED4_4_LoopFlag = 5;
	}
}

BYTE HL1606_LED4_4_IsEnd()
{
	return (0 == m_byLED4_4_LoopFlag);
}
