// ByteCode.c

#include <reg52.h>
#include <intrins.h>
#include "ByteCode.h"

// 获取字节码
// pData：字节码数据，长度固定为CODE_UNIT_SIZE
// 返回值：指令条数
static code BYTE m_byaCmdSize[] = {
	4,  // 单色常亮模式
	3,  // 熄灭等待
	9,  // 单色流动模式
	10, // 双色流动模式
	12, // 三色流动模式
	22, // 八色流动模式
	5,  // 呼吸模式
	8   // 单色拖尾流动模式
};
// 转换指令类型
static code BYTE m_byaConvert[] = {0, 0, 1, 1, 1, 1, 2, 3};

// 单色常亮模式
static void GetParamOfMode1(BYTE* byaData, union _UN_CMD_PARAM* punParam)
{
	punParam->pm1.byColor = byaData[1];
	punParam->pm1.wDelay = ((WORD)byaData[2]) << 8;
	punParam->pm1.wDelay += byaData[3];
}

// 熄灭等待
static void GetParamOfMode2(BYTE* byaData, union _UN_CMD_PARAM* punParam)
{
	punParam->pm1.byColor = 0x80;
	punParam->pm1.wDelay = ((WORD)byaData[1]) << 8;
	punParam->pm1.wDelay += byaData[2];
}

// 单色流动模式
static void GetParamOfMode3(BYTE* byaData, union _UN_CMD_PARAM* punParam)
{
	punParam->pm2.wOffset = ((WORD)byaData[1]) << 8;
	punParam->pm2.wOffset += byaData[2];
	punParam->pm2.byColor1 = byaData[3];
	punParam->pm2.byLen1   = byaData[4];
	punParam->pm2.byColor2 = 0x80;
	punParam->pm2.byLen2   = byaData[5];
	punParam->pm2.byColor3 = 0x80;
	punParam->pm2.byLen3   = 0;
	punParam->pm2.byColor4 = 0x80;
	punParam->pm2.byLen4   = 0;
	punParam->pm2.byColor5 = 0x80;
	punParam->pm2.byLen5   = 0;
	punParam->pm2.byColor6 = 0x80;
	punParam->pm2.byLen6   = 0;
	punParam->pm2.byColor7 = 0x80;
	punParam->pm2.byLen7   = 0;
	punParam->pm2.byColor8 = 0x80;
	punParam->pm2.byLen8   = 0;
	punParam->pm2.bySpeed  = byaData[6];
	punParam->pm2.wDelay  = ((WORD)byaData[7]) << 8;
	punParam->pm2.wDelay  += byaData[8];
}

// 双色流动模式
static void GetParamOfMode4(BYTE* byaData, union _UN_CMD_PARAM* punParam)
{
	punParam->pm2.wOffset = ((WORD)byaData[1]) << 8;
	punParam->pm2.wOffset += byaData[2];
	punParam->pm2.byColor1 = byaData[3];
	punParam->pm2.byLen1   = byaData[4];
	punParam->pm2.byColor2 = byaData[5];
	punParam->pm2.byLen2   = byaData[6];
	punParam->pm2.byColor3 = 0x80;
	punParam->pm2.byLen3   = 0;
	punParam->pm2.byColor4 = 0x80;
	punParam->pm2.byLen4   = 0;
	punParam->pm2.byColor5 = 0x80;
	punParam->pm2.byLen5   = 0;
	punParam->pm2.byColor6 = 0x80;
	punParam->pm2.byLen6   = 0;
	punParam->pm2.byColor7 = 0x80;
	punParam->pm2.byLen7   = 0;
	punParam->pm2.byColor8 = 0x80;
	punParam->pm2.byLen8   = 0;
	punParam->pm2.bySpeed  = byaData[7];
	punParam->pm2.wDelay  = ((WORD)byaData[8]) << 8;
	punParam->pm2.wDelay  += byaData[9];
}

// 三色流动模式
static void GetParamOfMode5(BYTE* byaData, union _UN_CMD_PARAM* punParam)
{
	punParam->pm2.wOffset = ((WORD)byaData[1]) << 8;
	punParam->pm2.wOffset += byaData[2];
	punParam->pm2.byColor1 = byaData[3];
	punParam->pm2.byLen1   = byaData[4];
	punParam->pm2.byColor2 = byaData[5];
	punParam->pm2.byLen2   = byaData[6];
	punParam->pm2.byColor3 = byaData[7];
	punParam->pm2.byLen3   = byaData[8];
	punParam->pm2.byColor4 = 0x80;
	punParam->pm2.byLen4   = 0;
	punParam->pm2.byColor5 = 0x80;
	punParam->pm2.byLen5   = 0;
	punParam->pm2.byColor6 = 0x80;
	punParam->pm2.byLen6   = 0;
	punParam->pm2.byColor7 = 0x80;
	punParam->pm2.byLen7   = 0;
	punParam->pm2.byColor8 = 0x80;
	punParam->pm2.byLen8   = 0;
	punParam->pm2.bySpeed  = byaData[9];
	punParam->pm2.wDelay  = ((WORD)byaData[10]) << 8;
	punParam->pm2.wDelay  += byaData[11];
}

// 八色流动模式
static void GetParamOfMode6(BYTE* byaData, union _UN_CMD_PARAM* punParam)
{
	punParam->pm2.wOffset = ((WORD)byaData[1]) << 8;
	punParam->pm2.wOffset += byaData[2];
	punParam->pm2.byColor1 = byaData[3];
	punParam->pm2.byLen1   = byaData[4];
	punParam->pm2.byColor2 = byaData[5];
	punParam->pm2.byLen2   = byaData[6];
	punParam->pm2.byColor3 = byaData[7];
	punParam->pm2.byLen3   = byaData[8];
	punParam->pm2.byColor4 = byaData[9];
	punParam->pm2.byLen4   = byaData[10];
	punParam->pm2.byColor5 = byaData[11];
	punParam->pm2.byLen5   = byaData[12];
	punParam->pm2.byColor6 = byaData[13];
	punParam->pm2.byLen6   = byaData[14];
	punParam->pm2.byColor7 = byaData[15];
	punParam->pm2.byLen7   = byaData[16];
	punParam->pm2.byColor8 = byaData[17];
	punParam->pm2.byLen8   = byaData[18];
	punParam->pm2.bySpeed  = byaData[19];
	punParam->pm2.wDelay  = ((WORD)byaData[20]) << 8;
	punParam->pm2.wDelay  += byaData[21];
}

// 呼吸模式
static void GetParamOfMode7(BYTE* byaData, union _UN_CMD_PARAM* punParam)
{
	punParam->pm3.byColor = byaData[1];
	punParam->pm3.byFreq  = byaData[2];
	punParam->pm3.wDelay = ((WORD)byaData[3]) << 8;
	punParam->pm3.wDelay += byaData[4];
}

// 单色拖尾流动模式
static void GetParamOfMode8(BYTE* byaData, union _UN_CMD_PARAM* punParam)
{
	punParam->pm4.wOffset = ((WORD)byaData[1]) << 8;
	punParam->pm4.wOffset += byaData[2];
	punParam->pm4.byColor  = byaData[3];
	punParam->pm4.byGap    = byaData[4];
	punParam->pm4.bySpeed  = byaData[5];
	punParam->pm4.wDelay  = ((WORD)byaData[6]) << 8;
	punParam->pm4.wDelay  += byaData[7];
}

BYTE GetCodeCount(BYTE* byaData)
{
	BYTE byCodeCount = 0;
	BYTE byTypeIndex;
	BYTE byRun = 1;
	WORD i = 0;

	// 分析段标号
	if (byaData[0] == 0xFF) {
		return 0;
	}
	while (byRun) {
		byTypeIndex = byaData[i];
		if (byTypeIndex < 8) {
			i += m_byaCmdSize[byTypeIndex];
			byCodeCount ++;
		} else {
			byRun = 0;
		}
		// 本灯带存储区域已满，解析结束
		if ((CODE_UNIT_SIZE - i) < 4) {
			byRun = 0;
		}
	}
	return byCodeCount;
}

BYTE GetByteCode(BYTE* byaData, BYTE index, union _UN_CMD_PARAM* punParam)
{
	BYTE byCodeCount = 0;
	BYTE byTypeIndex;
	BYTE byCmdType;
	BYTE byRun = 1;
	WORD i = 0;

	// 分析段标号
	if (byaData[0] == 0xFF) {
		return 0xFF;
	}
	while (byRun) {
		byTypeIndex = byaData[i];
		if (byCodeCount == index) {
			byCmdType = m_byaConvert[byTypeIndex];
			switch (byTypeIndex) {
				case 0:	GetParamOfMode1(byaData + i, punParam);	break;
				case 1:	GetParamOfMode2(byaData + i, punParam);	break;
				case 2:	GetParamOfMode3(byaData + i, punParam);	break;
				case 3:	GetParamOfMode4(byaData + i, punParam);	break;
				case 4:	GetParamOfMode5(byaData + i, punParam);	break;
				case 5:	GetParamOfMode6(byaData + i, punParam);	break;
				case 6:	GetParamOfMode7(byaData + i, punParam);	break;
				case 7:	GetParamOfMode8(byaData + i, punParam);	break;
			}
			return byCmdType;
		} else {
			byTypeIndex = byaData[i];
			if (byTypeIndex < 8) {
				i += m_byaCmdSize[byTypeIndex];
				byCodeCount ++;
			} else {
				byRun = 0;
			}
			// 本灯带存储区域已满，解析结束
			if ((CODE_UNIT_SIZE - i) < 4) {
				byRun = 0;
			}
		}
	}
	return 0xFF;
}

// 将速度转换为延时时间
BYTE SpeedToDelay(BYTE speed)
{
	static code BYTE dict[] = {
		0,
		30,
		23,
		17,
		13,
		10,
		7,
		5,
		3,
		2,
		1
	};
	return dict[speed];
}

// 将颜色转变为渐明颜色
BYTE ColorToLight(BYTE color)
{
	switch (color) {
	case 0x81: return 0x82;
	case 0x90: return 0xa0;
	case 0x84: return 0x88;
	case 0x94: return 0xa8;
	case 0x91: return 0xa2;
	case 0x85: return 0x8a;
	case 0x95: return 0xaa;
	default: return 0x80;
	}
}

// 将颜色转变为渐暗颜色
BYTE ColorToDim(BYTE color)
{
	switch (color) {
	case 0x81: return 0x83;
	case 0x90: return 0xb0;
	case 0x84: return 0x8b;
	case 0x94: return 0xcb;
	case 0x91: return 0xb3;
	case 0x85: return 0x8b;
	case 0x95: return 0xbf;
	default: return 0x80;
	}
}

BYTE FreqToSpeed(BYTE freq)
{
	switch (freq) {
	case 10: return 10;
	case 9: return 20;
	case 8: return 30;
	case 7: return 40;
	case 6: return 50;
	case 5: return 60;
	case 4: return 70;
	case 3: return 80;
	case 2: return 90;
	case 1: return 100;
	case 0:
	default:
		return 200;
	}
}

WORD GetMode4Speed(BYTE speed)
{
	static code WORD dict[] = {
		0,
		30,
		23,
		17,
		13,
		10,
		7,
		5,
		3,
		2,
		1
	};
	return dict[speed];
}
