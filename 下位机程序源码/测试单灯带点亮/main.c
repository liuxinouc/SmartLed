/*-----------------------------------------------
 * 主文件
------------------------------------------------*/  

#include <reg52.h>          //头文件的包含
#include <intrins.h>

#define BYTE      unsigned char
#define WORD      unsigned int
#define DWORD     unsigned long

char code reserve[3] _at_ 0x3b;  // 为仿真调试保留3个字节

static xdata BYTE g_byaByteCode1[32] _at_ 0x0100;
static xdata BYTE g_byaByteCode2[32] _at_ 0x0120;
static xdata BYTE g_byaByteCode3[32] _at_ 0x0140;
static xdata BYTE g_byaByteCode4[32] _at_ 0x0160;

static BYTE  m_byCount = 0;
static bit   g_bAddrMode = 1;
static bit   g_bTimerFlag = 0;
static BYTE  g_by100msCounter = 99;

void InitUART(void)
{
	SCON  = 0x90;
	TMOD |= 0x20;     // TMOD: timer 1, mode 2, 8-bit 重装
	SM2   = 1;
	TH1   = 0xFD;     // TH1:  重装值 9600 波特率 晶振 11.0592MHz  
	TR1   = 1;        // TR1:  timer 1 打开
	EA    = 1;        // 打开总中断
	ES    = 1;        // 打开串口中断
}

void Init_Timer0(void)
{
	TMOD |= 0x01;	  // 使用模式1，16位定时器，使用"|"符号可以在使用多个定时器时不受影响
	EA   = 1;         // 总中断打开
	ET0  = 1;         // 定时器中断打开
	TR0  = 1;         // 定时器开关打开
	ET1  = 0;
	TR1  = 0;
}

void UART_SER(void) interrupt 4 //串行中断服务程序
{
	BYTE bySubAddr = P0 & 0x03;
	BYTE it;

	if (TI) {
		TI = 0;
	}

	if (RI) {
		RI = 0;
		it = SBUF;

		if (g_bAddrMode) {
			if (it == bySubAddr) {
				g_bAddrMode = 0;
				SM2 = 0;
			}
			return;
		}

		if (m_byCount < 32) {
			g_byaByteCode1[m_byCount] = it;
		}
		if ((m_byCount >= 32) && (m_byCount < 64)) {
			g_byaByteCode2[m_byCount - 32] = it;
		}
		if ((m_byCount >= 64) && (m_byCount < 96)) {
			g_byaByteCode3[m_byCount - 64] = it;
		}
		if ((m_byCount >= 96) && (m_byCount < 128)) {
			g_byaByteCode4[m_byCount - 96] = it;
		}
		m_byCount ++;
		if (m_byCount == 128) {
			// 全部接收完毕，关串口中断，不再接收串口数据
			ES = 0;
		}
	}
}

/*------------------------------------------------
                 定时器中断子程序
------------------------------------------------*/
void Timer0_isr(void) interrupt 1
{
	TH0 = (65536 - 922) / 256;		  // 重新赋值 12M晶振计算，指令周期1uS，
	TL0 = (65536 - 922) % 256;        // 1mS方波半个周期500uS，即定时500次
	g_bTimerFlag = 1;
}

/*------------------------------------------------
                    主函数
------------------------------------------------*/
void main()
{
	P0 = 0xFF;
	InitUART();

	while (m_byCount < 128) {
		;
		_nop_();
	}

	Init_Timer0();
	while (1) {
		;
		_nop_();
	}

}
