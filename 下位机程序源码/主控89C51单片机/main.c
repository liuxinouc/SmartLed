#include <reg52.h>
#include <intrins.h>

#define     BYTE      unsigned char
#define     WORD      unsigned int
#define     DWORD     unsigned long
#define     _Nop()    _nop_()

code char reserve[3] _at_ 0x3b; // 为仿真器预留的3个字节

// 调试用的显示数字
BYTE m_byaDebugShow[8];

// 24C08操作相关
sbit SDA = P2^1;            // I2C数据传送位
sbit SCL = P2^0;            // I2C时钟控制位

// 是否USB插入
sbit m_bIsUSB = P0^0;

// 常量数组
code BYTE Empyt[16] = {0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};
code BYTE AddrB[16] = {0x00,0x10,0x20,0x30,0x40,0x50,0x60,0x70,0x80,0x90,0xa0,0xb0,0xc0,0xd0,0xe0,0xf0};
code BYTE AddrC[2]  = {0xa8,0xaa};
code BYTE AddrC2[4]  = {0xa8,0xaa,0xac,0xae};

BYTE m_byIndexAB;
BYTE m_byIndexAC;

// 24C08读写缓冲
BYTE m_byaWriteArray[16];
BYTE m_byaReadArray[16];
BYTE m_byCount;         // 数组计数
WORD m_wError;         // 出错计数

bit m_bACK;             // 应答标志位
bit m_bBegin;           // 当为0时表示对于接收到的数据丢弃，1表示接收的数据放到数组array中
char code m_chaReturn[] = " OK  ER ";

/*------------------------------------------------
 uS延时函数，含有输入参数 unsigned char t，无返回值
 unsigned char 是定义无符号字符变量，其值的范围是
 0~255 这里使用晶振12M，精确延时请使用汇编,大致延时
 长度如下 T=tx2+5 uS 
------------------------------------------------*/
void DelayUs2x(BYTE t)
{
	while(--t);
}

/*------------------------------------------------
 mS延时函数，含有输入参数 unsigned char t，无返回值
 unsigned char 是定义无符号字符变量，其值的范围是
 0~255 这里使用晶振12M，精确延时请使用汇编
------------------------------------------------*/
void DelayMs(BYTE t)
{
	while (t--) {
		//大致延时1mS
		DelayUs2x(245);
		DelayUs2x(245);
	}
}

/*------------------------------------------------
                    启动总线
------------------------------------------------*/
void Start_I2c()
{
	SDA = 1;     // 发送起始条件的数据信号
	_Nop();
	SCL = 1;
	_Nop();      // 起始条件建立时间大于4.7us,延时
	_Nop();
	_Nop();
	_Nop();
	_Nop();
	SDA = 0;     // 发送起始信号
	_Nop();      // 起始条件锁定时间大于4μ
	_Nop();
	_Nop();
	_Nop();
	_Nop();
	SCL = 0;     // 钳住I2C总线，准备发送或接收数据
	_Nop();
	_Nop();
}
/*------------------------------------------------
                    结束总线
------------------------------------------------*/
void Stop_I2c()
{
	SDA = 0;     // 发送结束条件的数据信号
	_Nop();      // 发送结束条件的时钟信号
	SCL = 1;     // 结束条件建立时间大于4μ
	_Nop();
	_Nop();
	_Nop();
	_Nop();
	_Nop();
	SDA = 1;     // 发送I2C总线结束信号
	_Nop();
	_Nop();
	_Nop();
	_Nop();
}

/*----------------------------------------------------------------
                 字节数据传送函数               
函数原型: void  SendByte(unsigned char c);
功能:  将数据c发送出去,可以是地址,也可以是数据,发完后等待应答,并对
     此状态位进行操作.(不应答或非应答都使ack=0 假)     
     发送数据正常，ack=1; ack=0表示被控器无应答或损坏。
------------------------------------------------------------------*/
void SendByte(BYTE c)
{
	BYTE BitCnt;
	// 要传送的数据长度为8位
	for (BitCnt=0; BitCnt<8; BitCnt++) {
		// 判断发送位
		if ((c << BitCnt) & 0x80) {
			SDA = 1;
		} else {
			SDA = 0;
		}
		_Nop();
		SCL = 1; // 置时钟线为高，通知被控器开始接收数据位
		_Nop();
		_Nop();  // 保证时钟高电平周期大于4μ
		_Nop();
		_Nop();
		_Nop();
		SCL = 0;
	}

	_Nop();
	_Nop();
	SDA = 1;     //8位发送完后释放数据线，准备接收应答位
	_Nop();
	_Nop();
	SCL = 1;
	_Nop();
	_Nop();
	_Nop();

	// 判断是否接收到应答信号
	if (SDA == 1) {
		m_bACK = 0;
	} else {
		m_bACK = 1;
	}
	SCL = 0;
	_Nop();
	_Nop();
}

/*----------------------------------------------------------------
                 字节数据传送函数               
函数原型: unsigned char  RcvByte();
功能:  用来接收从器件传来的数据,并判断总线错误(不发应答信号)，
     发完后请用应答函数。  
------------------------------------------------------------------*/	
BYTE RcvByte()
{
	BYTE retc;
	BYTE BitCnt;
	retc = 0;
	SDA = 1;     // 置数据线为输入方式
	for (BitCnt=0; BitCnt<8; BitCnt++) {
		_Nop();
		SCL = 0; // 置时钟线为低，准备接收数据位
		_Nop();
		_Nop();  // 时钟低电平周期大于4.7us
		_Nop();
		_Nop();
		_Nop();
		SCL = 1; // 置时钟线为高使数据线上数据有效
		_Nop();
		_Nop();
		retc = retc << 1;
		if (SDA == 1) {
			// 读数据位,接收的数据位放入retc中
			retc++;
		}
		_Nop();
		_Nop();
	}
	SCL = 0;
	_Nop();
	_Nop();
	return (retc);
}

/*----------------------------------------------------------------
                     应答子函数
原型:  void Ack_I2c(void);
 
----------------------------------------------------------------*/
void Ack_I2c(void)
{
	SDA = 0;
	_Nop();
	_Nop();
	_Nop();
	SCL = 1;
	_Nop();
	_Nop();      // 时钟低电平周期大于4μ
	_Nop();
	_Nop();
	_Nop();
	SCL = 0;     // 清时钟线，钳住I2C总线以便继续接收
	_Nop();
	_Nop();
}
/*----------------------------------------------------------------
                     非应答子函数
原型:  void NoAck_I2c(void);
 
----------------------------------------------------------------*/
void NoAck_I2c(void)
{
	SDA = 1;
	_Nop();
	_Nop();
	_Nop();
	SCL = 1;
	_Nop();
	_Nop();      // 时钟低电平周期大于4μ
	_Nop();
	_Nop();
	_Nop();
	SCL = 0;     // 清时钟线，钳住I2C总线以便继续接收
	_Nop();
	_Nop();
}

/*----------------------------------------------------------------
                    向有子地址器件发送多字节数据函数               
函数原型: bit  ISendStr(unsigned char sla,unsigned char suba,ucahr *s,unsigned char no);  
功能:     从启动总线到发送地址，子地址,数据，结束总线的全过程,从器件
          地址sla，子地址suba，发送内容是s指向的内容，发送no个字节。
           如果返回1表示操作成功，否则操作有误。
注意：    使用前必须已结束总线。
----------------------------------------------------------------*/
bit ISendStr(BYTE sla, BYTE suba, BYTE* s, BYTE no)
{
	BYTE i;
	Start_I2c();          // 启动总线
	SendByte(sla);        // 发送器件地址
	if (m_bACK == 0) {
		return (0);
	}

	SendByte(suba);       // 发送器件子地址
	if (m_bACK == 0) {
		return (0);
	}

	for (i=0; i<no; i++) {
		SendByte(*s);     // 发送数据
		if (m_bACK == 0) {
			return(0);
		}
		s++;
	}
	Stop_I2c();           // 结束总线
	return (1);
}

/*----------------------------------------------------------------
                    向有子地址器件读取多字节数据函数               
函数原型: bit  ISendStr(unsigned char sla,unsigned char suba,ucahr *s,unsigned char no);  
功能:     从启动总线到发送地址，子地址,读数据，结束总线的全过程,从器件
          地址sla，子地址suba，读出的内容放入s指向的存储区，读no个字节。
           如果返回1表示操作成功，否则操作有误。
注意：    使用前必须已结束总线。
----------------------------------------------------------------*/
bit IRcvStr(BYTE sla, BYTE suba, BYTE *s, BYTE no)
{
	BYTE i;

	Start_I2c();          // 启动总线
	SendByte(sla);        // 发送器件地址
	if (m_bACK == 0) {
		return(0);
	}
	SendByte(suba);       // 发送器件子地址
	if (m_bACK == 0) {
		return(0);
	}

	Start_I2c();
	SendByte(sla + 1);
	if (m_bACK == 0) {
		return(0);
	}
	for (i=0; i<no-1; i++) {
		*s = RcvByte();   // 发送数据
		Ack_I2c();        // 发送就答位 
		s++;
	}
	*s = RcvByte();
	NoAck_I2c();          // 发送非应位
	Stop_I2c();           // 结束总线
	return (1);
}

// 初始化串口与PC通讯
void InitUART(void)
{
	SCON  = 0x50;
	TMOD |= 0x20;         // TMOD: timer 1, mode 2, 8-bit 重装
	TH1   = 0xFD;         // TH1:  重装值 9600 波特率 晶振 11.0592MHz  
	TR1   = 1;            // TR1:  timer 1 打开                         
	EA    = 1;            // 打开总中断
	// ES    = 1;         // 打开串口中断
}

// 初始化串口为多机通讯模式
void InitUART1(void)
{
	SCON  = 0x90;
	SM2   = 0;
	TMOD |= 0x20;         // TMOD: timer 1, mode 2, 8-bit 重装
	TH1   = 0xFD;         // TH1:  重装值 9600 波特率 晶振 11.0592MHz  
	TR1   = 1;            // TR1:  timer 1 打开                         
	EA    = 1;            // 总中断不能关，关闭总中断导致仿真器失败
	ES    = 0;            // 关闭串口中断
}

// 串口中断处理
void UART_SER(void) interrupt 4
{
	BYTE i;
	BYTE j;
	BYTE it;

	// 未检测到接收有效标志
	if (!RI) {
		return;
	}

	RI = 0;
	it = SBUF;
	if (!m_bBegin) {
		if (0xCC == it) {
			// 清除24C08的数据
			for (j=0; j<4; j++) {
				for (i=0; i<16; i++) {
					ISendStr(AddrC2[j], AddrB[i], Empyt, 16);
					DelayMs(10);
				}
			}
			// 回复PC机OK
			for (i=0; i<4; i++) {
				SBUF = m_chaReturn[i];
				while (!TI) {
					;
				}
				TI = 0;
			}
		}

		// 检测到数据报头
		if (0xAA == it) {
			m_bBegin = 1;
			m_byCount = 0;
			m_wError = 0;
		}
		return;
	}

	// 下面是begin为1的情况，接收数据
	if (m_byCount == 16) {
		// 检测到数据报尾
		if (0xBB != it) {
			m_wError ++;
		}
		m_bBegin = 0;

		if (m_wError == 0) {
			// 回复PC机OK
			for (i=0; i<4; i++) {
				SBUF = m_chaReturn[i];
				while (!TI) {
					;
				}
				TI = 0;
			}
		}
		return;
	}

	m_byaWriteArray[m_byCount] = it;
	m_byCount ++;
	if (m_byCount == 16) {
		// 写入24C08
		ISendStr(AddrC[m_byIndexAC], AddrB[m_byIndexAB], m_byaWriteArray, 16);
		DelayMs(5);
		// 从24C08读出
		IRcvStr(AddrC[m_byIndexAC], AddrB[m_byIndexAB], m_byaReadArray, 16);
		// 校验读写是否一致
		for (i=0; i<16; i++) {
			if(m_byaWriteArray[i] != m_byaReadArray[i]) {
				m_wError ++;
			}
		}
		// 清除数据缓冲区
		for (i=0; i<16; i++) {
			m_byaWriteArray[i] = 0;
			m_byaReadArray[i] = 0;
		}

		m_byIndexAB ++;
		if (m_byIndexAB == 16) {
			m_byIndexAB = 0;
			m_byIndexAC ++;
			if (m_byIndexAC == 2) {
				m_byIndexAC = 0;
			}
		}
	}

	// 检查工作过程中是否出错
	if (m_wError > 0) {
		// 回复PC机ER
		for (i=4; i<8; i++) {
			SBUF = m_chaReturn[i];
			while (!TI) {
				;
			}
			TI = 0;
		}
	}
}

/*------------------------------------------------
                    主函数
------------------------------------------------*/
void main()
{
	BYTE i;
	BYTE j;
	BYTE k;

	m_bIsUSB = 1;
	m_byIndexAC = 0;
	m_byIndexAB = 0;
	m_wError = 0;
	DelayMs(200);

	if (m_bIsUSB == 1) {
		// 初始化为接收数据模式
		m_byCount = 0;
		InitUART();
	} else {
		// 初始化为多机通讯模式
		InitUART1();
	}

	ES = 1;
	if (m_bIsUSB == 1) {
		// 数据接收工作在串口中断函数中完成
	}

	if (m_bIsUSB == 0) {
		// 等待各从机完成初始化
		DelayMs(200);

		// 给4个从机分发显示数据
		for (j=0; j<4; j++) {
			// 串口第九位置为1，呼叫从机地址
			TB8 = 1;
			SBUF = j;
			while(!TI){_nop_();}
			TI = 0;

			// 每次发16字节，分8次发完
			for (i=0; i<8; i++) {
				IRcvStr(AddrC[m_byIndexAC], AddrB[m_byIndexAB], m_byaReadArray, 16);
				m_byIndexAB ++;
				if (m_byIndexAB == 16) {
					m_byIndexAB = 0;
					m_byIndexAC ++;
					if (m_byIndexAC == 2) {
						m_byIndexAC = 0;
					}
				}

				for (k=0; k<16; k++) {
					TB8 = 0;
					SBUF = m_byaReadArray[k];
					while(!TI){_nop_();}
					TI = 0;
				}
			}
		}
	}

	while (1) {
		if (0 == m_wError) {
			P1 = 0xC0;// 显示"O"，表示OK
		} else {
			P1 = 0x06;// 显示"E."，表示ERROR
		}
	}
}

