# SmartLed

#### 介绍
本发明智能灯带控制器是一款多功能、高性能、智能化的 LED 灯带控制器。

本软件所依赖的理论基础非常深奥，为一部发明专利，具体参见“CN201510387617-一种LED灯带显示效果控制方法.pdf”。本系统的实现过程中，首先将此理论抽象为一种虚拟机，然后通过可视化的“灯带编辑软件”构造出供此虚拟机使用的字节码，接下来将字节码“下载”到虚拟机中执行。在灯带控制器设备和灯带编辑器软件中，分别置有两套互相等价的虚拟机，因此模拟器的显示效果与灯带控制器实际驱动的显示效果等同。

#### 安装教程和使用说明

请参考“灯带编辑软件使用说明书.pdf”文件的内容

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)